This folder contains information about communicating with Pasco sensors.

It consists of:
* Communicating with Pasco devices.docx: A document that explains the BLE communications protocol with our wireless sensors.
* datasheets.xml    : XML file that describes all Pasco interfaces and sensors and how to convert their raw data into actual measurement data.
* Windows/*.*       : MFC Windows app that contains extremely basic code to manipulate most of the Pasco Wireless Sensors over BLE.
* JS/*.*            : Javascript demonstration code that provides simple examples of how to manipulate some of the Pasco Wireless Sensors over BLE and USB.

09/01/2023
* Add JavaScript demonstration code (has only been tested on Chrome browser).  The Windows demonstration code will no longer being updated.

