
The JavaScript demonstration code provides simple examples of how to manipulate some of the Pasco Wireless Sensors over BLE and USB.
   * This is not a fully functional library.  It is a simple program that demonstrates how to interact with Pasco wireless sensors.
   * This code has only been tested on the Chrome browser.
   * When connecting over USB, all Pasco wireless sensors will show up as 'Pasco USB Bridge' in the WebUSB interface.

The code is organized as follows:
* Datasheets.js               - Describes all sensors and interfaces that are supported by this demonstration app.  It is derived from datasheets.xml.
* PascoBLEDriver.js           - Contains low-level functions that handle packet-level requests and events to the Pasco Wireless sensors.  Works with BLE and USB protocols.
* PascoWirelessInterface.js   - Handles higher-level functions for the Pasco Wireless Sensors.
* PascoHardware.js            - Manages all connected interfaces.
* PascoInterface.js           - Contains one or more sensors.
* PascoSensor.js              - Contains one or more measurements.
* PascoMeasurement.js         - Contains the measurements provided by a sensor and performs the necessary calculations to convert raw sensor data to measurement data.
* Graph.js                    - A simple XY graph that graphs two arrays of numbers against each other.
* PascoBLETest.js             - Top level code that handles HTML events
* index.html                  - HTML file that launches the program

Most requests to the sensor use async/await to simplify the code and reduce callback confusion.
 
Important functions are:
Datasheets:
   mInterfaces        - Describes all interfaces supported by this demonstration utility.
   mSensors           - Describes all sensors that can be connected to the interfaces
   
PascoBLEDriver:
   BleNotification    - Handles any BLE or USB packet from the sensor.
   SendGenericCommand - Sends request to sensor and optionally waits for response or timeout.

PascoWirelessInterface:
   Connect            - Finalizes BLE or USB connection and does initial setup of the sensor/interface
   ReadOneSample      - Request a single raw sample data buffer from the sensor and calculate the measurement data from it
   StartSampling      - Tell the interface's sensors to continuously sample at their configured sample rate
   PollForData        - Periodically read back the continuously sampled raw data and calculate the measurement data from it
   
PascoSensor:
   ProcessData        - Converts raw stream of byte data from the sensor into actual sensor measurement data.
   
PascoMeasurement:
   CalcData           - Performs the calculation to convert raw data to sensor data as described in Datasheets.js
   p_*                - handlers for measurement calculation functions as described in Datasheets.js

Supported Wireless Sensors in Datasheets.js:
   AirLink",
   Temperature",
   Pressure",
   Smart Cart",
   Motion",
   Smart Gate",
   Code Node",
   Control Node",
   Force Platform",
   Force Platform 2D",

Supported Pasport sensors that connect to the AirLink in Datasheets.js
   Force
   