
'use strict'

function WebUSBManager( )
{
	this.mNextDeviceID = 1;
	this.mScanCallback = null;
	this.mDevices = [];

	this.mLogData = true;

	//**********************************************************************************
	//
	//**********************************************************************************
	this.StartScan = async function( )
	{
        try
        {
            qDebug( "Requesting USB device..." );
            let device = await navigator.bluetooth.requestDevice( bleFilterPasco );
            qDebug( "Found device " + device.name + ": id=" + device.id );

            // Act as if the selected device was just another advertised devise
            let event = { device : device, name : device.name, rssi : 0 };
            this.OnAdvert( event );
        }
        catch( err )
        {
            qDebug( "BLE Request Failed: " + err );
        }
	}

	//**********************************************************************************
	//
	//**********************************************************************************
	this.StopScan = function( )
	{
		if( this.mScanner )
			this.mScanner.stop( );
	}

	//**********************************************************************************
	// Called when a device is detected via an advertisemet packet or a WinBluetooth selection
	//**********************************************************************************
	this.OnAdvert = function( event )
	{
		// See if the device is already in my device list
		let device = null;
		for( let dev of this.mDevices )
		{
			if( dev.mDevice === event.device )
			{
				device = dev;
				break;
			}
		}

		// If the device is not already in my device list, add it and notify any listener
		if( !device )
		{
			device = new WebBLEDriver( event.device )
			this.mDevices.push( device );
		}

		device.mName = event.device.name;
		device.mRSSI = event.device.rssi;
		ExtractPascoInfoFromBLEName( device );

		// Notify any listeners of a new device being available
		if( this.mScanCallback )
			this.mScanCallback( device );
	}
}

var gUSBManager = new WebUSBManager( );
