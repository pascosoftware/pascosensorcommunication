
'use strict'

//**********************************************************
//
//**********************************************************
function PascoHardware( )
{
	this.mInterfaces = [];
	
	this.mOnSensorChanged = null;

	//******************************************************************
	this.AddDevice = async function( device )
	{
		let ifaceID = 0;

		// Get the interface ID from the device driver
		if( device.constructor.name == "USBDevice" )
		{
			// Get the faux product ID
			let req = { requestType : "vendor", recipient : "device", request : 0xed, value : 0, index : 0 };
			let rsp = await device.controlTransferIn( req, 2 );
			ifaceID = rsp.data.getUint16( 0, true );

			qDebug( "USB: vid=0x" + ToHex16( device.vendorId ) + ", pid=" + ToHex16( device.productId ) + ", fpid=" + ToHex16( ifaceID ) );
			if( ifaceID == 0 )
				return false;
		}
		else if( device.constructor.name == "WebBLEDriver" )
		{
			qDebug( "BLE: id=0x" + ToHex16( device.mIfaceID ) );
			ifaceID = device.mIfaceID;
		}
		else
		{
			qDebug( "Invalid driver type: " + device.constructor.name );
			return false;
		}

		// If this is a wireless sensor
		if( ifaceID >= 0x400 && ifaceID < 0x440 )
		{
			let iface = new PascoWirelessInterface( ifaceID );
			if( !await iface.Connect( device ) )
			{
				qDebug( "Failed to connect to device ID " + ifaceID );
				return false;
			}

			this.mInterfaces.push( iface );
		
			// Handle any events from the interfaces
			iface.mOnSensorChanged = this.OnSensorChanged;
		}
		// If this is a Polar heartrate sensor
		else if( ifaceID == 24 )
		{
			let iface = new StandardBLEInterface( ifaceID );
			if( !await iface.Connect( device ) )
			{
				qDebug( "Failed to connect to device ID " + ifaceID );
				return false;
			}

			this.mInterfaces.push( iface );
		}
		else
		{
			qDebug( "Unsupported iface ID: " + ToHex16( ifaceID ) );
			return false;
		}
	}

	//******************************************************************
	this.StartSampling = async function( )
	{
		for( let iface of this.mInterfaces )
			await iface.PreStartSampling( );
		for( let iface of this.mInterfaces )
			await iface.StartSampling( );
	}

	//******************************************************************
	this.StopSampling = async function( )
	{
		for( let iface of this.mInterfaces )
			iface.StopSampling( );
	}

	//******************************************************************
	this.ZeroSensor = async function( )
	{
		for( let iface of this.mInterfaces )
			iface.ZeroSensor( );
	}

	//******************************************************************
	this.RequestBatteryInfo = async function( )
	{
		for( let iface of this.mInterfaces )
			iface.RequestBatteryInfo( );
	}

	//******************************************************************
	this.OnSensorChanged = async function( )
	{
		if( this.mOnSensorChanged )
			this.mOnSensorChanged( );
	}.bind( this );

	//******************************************************************
	this.Synchronize = async function( )
	{
		for( let iface of this.mInterfaces )
			iface.Synchronize( );
	}

	return true;
}

var gPascoHardware = new PascoHardware( );