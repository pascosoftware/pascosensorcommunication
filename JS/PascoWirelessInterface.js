
'use strict'

//**********************************************************
//
//**********************************************************
function PascoWirelessInterface( ifaceID )
{
	PascoInterface.call( this, gDatasheets.FindInterface( ifaceID ) );

	this.mPascoDriver = null;
	this.mSampleTimer = 0;
	this.mShowBatteryEvents = false;
	this.mRangeIndex = -1;

	this.mOnSensorChanged = null;
	
	//*******************************************************
	this.Connect = async function( device )
	{
		this.mPascoDriver = new PascoBleDriver( );

		let success = await this.mPascoDriver.SetBleOrUsbDevice( device, this.mDS.Channels.length );
		if( !success )
		{
			qDebug( "Could not associate hardware driver with Pasco BLE driver" );
			return false;
		}

		// Show sensor firmware version
		await this.mPascoDriver.GetFirmwareVersion( );
		let version = await this.mPascoDriver.GetFirmwareVersion( );
		qDebug( "Firmware version = " + version.mMajor + "." + version.mMinor );

		// Load factory calibration data
		for( let i = 0; i < this.mSensors.length; i++ )
			await this.LoadFactoryCalibration( i );
		
		// Enable Smart Gate P1 and P2
		if( this.mDS.ID == iids.SmartGate )
			await this.mPascoDriver.SmartGateSetMode( true, true, false );
		
		this.mPascoDriver.mOnSensorID = this.OnSensorID;
		this.mPascoDriver.mOnBatteryStatus = this.OnBatteryInfo;
		this.mPascoDriver.mOnSync = this.OnSync;
		
		return true;
	}

	//*******************************************************
	this.PreStartSampling = async function(  )
	{
		for( let i = 0; i < this.mSensors.length; i++ )
		{
			let sensor = this.mSensors[i];

			// Tell each virtual sensor its sample period (or zero if no virtual sensor is configured)
			if( sensor )
			{
				await this.mPascoDriver.SetSampleConfig( i, Math.floor(sensor.mSamplePeriod * 1000000), sensor.mDS.SampleSize, false );
				sensor.StartSampling( );
			}
			else
				await this.mPascoDriver.SetSampleConfig( i, 0, 0, false );
			
			// Set current range, if configured
			if( sensor.mRangeIndex >= 0 && sensor.mRangeIndex < sensor.mDS.Range.Ranges.length )
			{
				this.mRangeIndex = sensor.mRangeIndex;
				let hwRangeID = sensor.mDS.Range.Ranges[sensor.mRangeIndex].HWIndex;
				await this.mPascoDriver.SetRange( i, hwRangeID );
			}
		}
		return true;
	}

	//*******************************************************
	this.StartSampling = async function(  )
	{
		if( !this.mSampleTimer )
			this.mSampleTimer = setInterval( this.PollForData, 200 );

		return await this.mPascoDriver.StartSampling( );
	}

	//*******************************************************
	this.StopSampling = async function(  )
	{
		if( this.mSampleTimer )
		{
			clearInterval( this.mSampleTimer );
			this.mSampleTimer = 0;
		}
		
		return await this.mPascoDriver.StopSampling( );
	}

	//*******************************************************
	this.ZeroSensor = async function(  )
	{
		for( let sensor of this.mSensors )
		{
			if( !this.mSampleTimer )
				await this.ReadOneSample( sensor );
			sensor.ZeroSensor( );
		}
	}

	//*******************************************************
	this.RequestBatteryInfo = async function(  )
	{
		this.mShowBatteryEvents = true;
		await this.mPascoDriver.RequestBatteryInfo( );
	}

	//**********************************************************
	this.PollForData = function( )
	{
		for( let i = 0; i < this.mSensors.length; i++ )
		{
			let sensor = this.mSensors[i];
			if( !sensor )
				continue;

			let data = this.mPascoDriver.ReadSampleData( i, 64, sensor.mDS.SampleSize );
			if( data.length > 0 )
			{
				sensor.ProcessData( data );
	//			qDebug( data.length + " bytes of sample data found on channel " + i );
	//			qHexDump( data );
			}
		}
	}.bind( this );

	//*******************************************************
	//
	//*******************************************************
	this.ReadOneSample = async function( sensor )
	{
		for( let i = 0; i < this.mSensors.length; i++ )
		{
			if( sensor == this.mSensors[i] )
			{
				// If we switched ranges, tell the sensor and wait 100ms to give the sensor time to adjust
				if( sensor.mRangeIndex >= 0 && sensor.mRangeIndex < sensor.mDS.Range.Ranges.length && this.mRangeIndex != sensor.mRangeIndex )
				{
					this.mRangeIndex = sensor.mRangeIndex;
					let hwRangeID = sensor.mDS.Range.Ranges[sensor.mRangeIndex].HWIndex;
					await this.mPascoDriver.SetRange( i, hwRangeID );
					await sleep( 100 );
				}
				
				let data = await this.mPascoDriver.ReadOneSample( i, sensor.mDS.SampleSize );
				
				if( data.length )
					sensor.ProcessData( data );
			}
		}
	}
	
	//*******************************************************
	//
	//*******************************************************
	this.Synchronize = async function( )
	{
		let period = 31000; // 31ms
		let count = 100;
		await this.mPascoDriver.StartSync( period, count );
	}
	
	//**********************************************************
	// Reads factory calibration from the sensor and stores it locally
	//**********************************************************
	this.LoadFactoryCalibration = async function( sensorIndex )
	{
		let sensor = this.mSensors[sensorIndex];
		if( !sensor || !sensor.mDS.hasOwnProperty( "FactoryCals" ) )
			return false;

		qDebug( "Loading factory calibration for sensor: " + sensor.mDS.NameTag );

		// AirLinks need to pull their calibration from their attached Pasport sensors
		if( this.mDS.ID == iids.AirLink3 )
		{
			// Send the request for the raw cal data
			let calBytes = sensor.mDS.FactoryCals.length * 16;
			const PasportRWFactoryCalSubCmd = 1;
			const PasportReadFactoryCalCmd = (0xc3 | 8);
			let cmd = [PasportReadFactoryCalCmd, PasportRWFactoryCalSubCmd, calBytes, 0];
			let result = await this.mPascoDriver.XferPasportSpiData( sensorIndex, cmd, calBytes );
			if( !result.mSuccess )
			{
				qDebug( "Failed to read factory calibration data from the Pasport SPI sensor" );
				return false;
			}

			// Convert cal data from 16.16 fixed point to floating point and apply it to the sensor
			let idata = new Int32Array( result.mData.buffer );
			for( let i = 0; i < idata.length; i += 4 )
			{
				qDebug( "Update Cal " + (i / 4) + ": (" + (idata[i + 0] / 65536.0) + ", " + (idata[i + 1] / 65536.0) +") -> (" + (idata[i + 2] / 65536.0) + ", " + (idata[i + 3] / 65536.0) + ")" );
//				sensor.UpdateFactoryCalibration( i / 4, idata[i] / 65536.0, idata[i + 1] / 65536.0, idata[i + 2] / 65536.0, idata[i + 3] / 65536.0 );
				sensor.mFactoryCalibrations[i/4].SetCalData( idata[i] / 65536.0, idata[i + 1] / 65536.0, idata[i + 2] / 65536.0, idata[i + 3] / 65536.0 );
			}	
		}
		else // All other wireless sensors store their own calibration data
		{
			// Figure out how much calibration data there is and tell the virtual sensor to transfer from its flash to its burst area storage
			let calBytes = sensor.mDS.FactoryCals.length * 16;
			let calCount = await this.mPascoDriver.XferBurstRam( true, pbc.BurstXferLocNvr, pbc.NVStorageFactoryCalTag1 + sensorIndex, calBytes );
			if( calCount == 0 )
			{
				qDebug( "Could not find calibration data for sensor: " + sensor.mDS.NameTag );
				return false;
			}

			// Read the cal data from the virtual sensor and apply it to the local sensor calibration calculator
			qDebug( "Loading " + (calBytes / 16) + " calibrations for sensor: " + sensor.mDS.NameTag );
	//		qHexDump( calData );
			let calData = await this.mPascoDriver.DoBurstReadTransfer( calBytes );
			let fdata = new Float32Array( calData.buffer );
			for( let i = 0; i < fdata.length; i += 4 )
			{
				qDebug( "Update Cal " + (i / 4) + ": (" + fdata[i + 0] + ", " + fdata[i + 1] +") -> (" + fdata[i + 2] + ", " + fdata[i + 3] + ")" );
//				sensor.UpdateFactoryCalibration( i / 4, fdata[i], fdata[i + 1], fdata[i + 2], fdata[i + 3] );
				sensor.mFactoryCalibrations[i/4].SetCalData( fdata[i], fdata[i + 1], fdata[i + 2], fdata[i + 3] );
			}	
		}
		
		return true;
	}

	//*******************************************************
	// Called when a sensor is added or removed from an AirLink
	this.OnSensorID = function( channel, id )
	{
		qDebug( "Sensor " + id + " Detected on channel " + channel );
		this.AddSensor( channel, id );
		this.LoadFactoryCalibration( channel );
		this.mOnSensorChanged( channel, id );
		
	}.bind( this );
	
	//*******************************************************
	// Called when sensor sends a battery status event
	this.OnBatteryInfo = function( voltage, percent, low, charging )
	{
		if( this.mShowBatteryEvents )
		{
			this.mShowBatteryEvents = false;
			qDebug( "Battery: Voltage=" + voltage + "mv (" + percent + "%), " + (low ? "Low" : "Not low") + ", " + (charging ? "Charging" : "Not charging") );		
		}
	}.bind( this );
	
	//*******************************************************
	// Called when sensor sends a synchronization event
	this.OnSync = function( timestamp )
	{
		qDebug( "Sync: timestamp=" + timestamp );		
	}.bind( this );
}
