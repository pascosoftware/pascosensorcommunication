
'use strict'

var gCalcFuncs = ["uint8", "int8", "uint16", "int16", "uint32", "int32", "atan2", "sqrt", "log", "log10", "fcal", "usound", "deriv", "sum", "limit", "wsgtime", "state",
				  "tig", "tbg", "table", "timestamp", "ekgbeat", "bloodpressure", "bppeak", "bpmap", "systolic", "diastolic", "bppulse", "windchill",
				  "dewpoint", "heatindex", "rangesel", "integral", "max", "perbreathvolume", "pendulum", "blockcount"];
var gCalcData;
var gUID = 1;

//*******************************************************************
//
//*******************************************************************
function PascoMeasurement( ds, sensor )
{
    //**************************************************************
    this.Init = function( ds, sensor )
    {
		this.mUID = gUID++;
        this.mSensor = sensor;
        this.mDS = ds;
        this.mX = 0;
        this.mY = 0;
		this.mLastGoodY = 0;
		this.mUserCalInputY = 0;
        this.mSum = 0;
        this.mPrevY = 0;
		this.mZeroOffset = 0.0;
		this.mGotData = false;
		this.mMax = 0;
		this.mIntegral = 0;
		this.mUserCal = null;
		this.mFirstPoint = true;
		
		this.mLimitHigh = null;
		this.mLimitLow = null;
		this.mTimeHigh = 0.0;
		this.mLastBlockTime = 0;
		this.mBlockCount = 0;
		this.mPrevBeatTime = 0;
		this.mFirstBeatTime = true;
		this.mBeatHistory = [];
		this.mBPCalc = sensor.mBPCalc;
		this.mPeakIndex = 0;
		
		this.mPrevFlow = 0;			// Flow rate for the previous sample
		this.mTransitionFlow = 0;
		this.mTransitionCount = 0;		// samples since the last transition
		this.mPositiveTransition = false;	// True if last transition was positive
		this.mVolumeHistory = [];

		this.mXStorage = null;
        this.mYStorage = null;
        this.mCalcProc = null;

		// Set up limits
		if( ds.hasOwnProperty( "Limit" ) )
		{
			this.mLimitLow = ds.Limit[0];
			this.mLimitHigh = ds.Limit[1];
		}

		// Set up user calibration
		if( ds.hasOwnProperty( "UserCalIndex" ) )
		{
			this.mUserCal = sensor.mUserCalibrations[ds.UserCalIndex];
			this.mUserCal.mMeasurement = this;
		}
		
		// Set initial value of user modifyable value
		if( ds.hasOwnProperty( "Value" ) )
			this.mY = ds.Value;

		// Process equation string to make it more JS friendly so we can eval it
		if( ds.hasOwnProperty( "Equation" ) )
		{
			// Create member references to potential inputs from other measurements
			// e.g. Create a property 'this.RawTemperature' for a measurement with an ID of 'RawTemperature'
			for( let m of this.mSensor.mMeasurements )
				this[m.mDS.ID] = m;
				
			// Copy the equation so we can munge it to be more JavaScript friendly
			this.mEquation = ds.Equation;

			// Convert variable references in the calculation equation to be compliant with JS calculations
			// e.g., Convert a reference to 'Temperature' to 'this["Temperature"].mY
			for( let m of this.mSensor.mMeasurements )
			{
				let regEx = new RegExp( "\\b" + m.mDS.ID + "\\b", "g" );
				this.mEquation = this.mEquation.replaceAll( regEx, "this[\"" + m.mDS.ID + "\"].mY" );
			}
			
			// Convert variable independent/time references in the calculation equation to be compliant with JS calculations
			// e.g., Convert a reference to 'Temperature.x' to 'this["Temperature"].mX'
			let regEx = new RegExp( "\\bmY.x\\b", "g" );
			this.mEquation = this.mEquation.replaceAll( regEx, "mX" );

			// Convert function references in the calculation equation to be compliant with JS calculations
			for( let func of gCalcFuncs )
			{
				let regex = new RegExp( "\\b" + func + "\\b", "g" );
				this.mEquation = this.mEquation.replaceAll( regex, "this.p_" + func );
			}

			// Create a JS function that will perform the calculation specified in the datasheet
			this.mEquation = "return " + this.mEquation;
			qDebug( "New equation: " + this.mDS.ID + " = " + this.mEquation );
			eval( "this.mCalcProc = function( ) { " + this.mEquation + "}" );
		}
    }

    //**************************************************************
    this.StartSampling = function( )
    {
        this.mX = 0;
		if( !this.mDS.hasOwnProperty( "Value" ) ) // Don't clear mY if it is holding a user modifyable constant
        	this.mY = 0;
        this.mSum = 0;
		this.mMax = -1e+200;
		this.mIntegral = 0;
		this.mLastGoodY = 0;
		this.mGotData = false;
		this.mFirstPoint = true;
		this.mLastBlockTime = 0;
		this.mBlockCount = 0;
		this.mPrevBeatTime = 0;
		this.mFirstBeatTime = true;
		this.mBeatHistory = [];
		this.mPeakIndex = 0;
		this.mPrevFlow = 0;			// Flow rate for the previous sample
		this.mTransitionFlow = 0;
		this.mTransitionCount = 0;		// samples since the last transition
		this.mPositiveTransition = false;	// True if last transition was positive
		this.mVolumeHistory = [];
		this.mTimeHigh = 0;
        if( this.mYStorage )
        {
            this.mXStorage.length = 0;
            this.mYStorage.length = 0;
        }
    }
	
    //**************************************************************
	this.ZeroMeasurement = function( )
	{
        if( this.mDS.hasOwnProperty( "OffsetZero" ) && this.mDS.OffsetZero != 0 )
		{
			qDebug( "Zero offset measurement '" + this.mDS.NameTag + " to " + this.mY.toFixed( 3 ) );
			this.mZeroOffset = this.mY + this.mZeroOffset;
		}
	}

    //**************************************************************
    this.DoCalc = function( )
    {
		// If this is a user modifyable variable, just return its value
		if( !this.mCalcProc )
		{
			this.mLastGoodY = this.mY;
			return this.mY;
		}

		// Calculate the new value for this measurement
		this.mX = this.mSensor.mSampleCount * this.mSensor.mSamplePeriod;
        this.mY = this.mCalcProc( );
		this.mUserCalInputY = this.mY; // user cal input before limits, offsets, cals, etc.. are applied
		
		// Apply offsets, cals, and limits
		if( this.mUserCal != null )
			this.mY = this.mY * this.mUserCal.mScale + this.mUserCal.mOffset;
		if( this.mLimitLow != null )
		{
			if( this.mY < this.mLimitLow )
				this.mY = this.mLimitLow;
			else if( this.mY > this.mLimitHigh )
				this.mY = this.mLimitHigh;
		}
		this.mY -= this.mZeroOffset;

		// If we still have good data, store it
		if( !isNaN( this.mY ) )
		{
			this.mLastGoodY = this.mY;
			
			// Store data if needed
			if( this.mYStorage )
			{
//				qDebug( "Store " + this.mDS.NameTag + ": X=" + this.mX + ", Y=" + this.mY );
				this.mXStorage.push( this.mX );
				this.mYStorage.push( this.mY );
			}
		}
    }

    //**************************************************************
    this.p_uint8 = function( offset )
    {
        return gCalcData[offset];
    }
    
    //**************************************************************
    this.p_int8 = function( offset )
    {
        let data = gCalcData[offset];
        if( data >= 0x80 )
            return -((~data & 0xff) + 1);
        else
            return data;
    }
    
    //**************************************************************
    this.p_uint16 = function( offset )
    {
        return gCalcData[offset] | (gCalcData[offset + 1] << 8);
    }
    
    //**************************************************************
    this.p_int16 = function( offset )
    {
        let data = gCalcData[offset] | (gCalcData[offset + 1] << 8);
        if( data >= 0x8000 )
            return -((~data & 0xffff) + 1);
        else
            return data;
    }
    
    //**************************************************************
    this.p_uint32 = function( offset )
    {
        return gCalcData[offset] | (gCalcData[offset + 1] << 8) | (gCalcData[offset + 2] << 16) | (gCalcData[offset + 3] << 24);
    }
    
    //**************************************************************
    this.p_int32 = function( offset )
    {
        let data = p_uint32( offset );
        if( data >= 0x80000000 )
            return -((~data & 0xffffffff) + 1);
        else
            return data;
    }
    
    //**************************************************************
    this.p_sqrt = function( x )
    {
		if( isNaN( x ) )
			return NaN;
		
        return Math.sqrt( x );
    }
    
    //**************************************************************
    this.p_log = function( x )
    {
		if( isNaN( x ) )
			return NaN;
		
        return Math.log( x );
    }
    
    //**************************************************************
    this.p_log10 = function( x )
    {
		if( isNaN( x ) )
			return NaN;
		
        return Math.log10( x );
    }
    
    //**************************************************************
    this.p_atan2 = function( y, x )
    {
		if( isNaN( x ) )
			return NaN;
		
        return Math.atan2( y, x );
    }
    
    //**************************************************************
    this.p_fcal = function( x, calIndex )
    {
		if( isNaN( x ) )
			return NaN;
		
        return this.mSensor.mFactoryCalibrations[calIndex].mScale * x + this.mSensor.mFactoryCalibrations[calIndex].mOffset ;
    }
      
    //**************************************************************
    this.p_ucal = function( x, calIndex )
    {
		if( isNaN( x ) )
			return NaN;
		
        return this.mSensor.mUserCalibrations[calIndex].mScale * x + this.mSensor.mUserCalibrations[calIndex].mOffset ;
    }
      
    //**************************************************************
    this.p_deriv = function( y, x )
    {
		if( isNaN( y ) )
			return NaN;
		
		// Since derivative requires a prior point, don't return data for the first point
		if( this.mFirstPoint )
		{
			this.mFirstPoint = false;
			this.mPrevX = x;
			this.mPrevY = y;
			return NaN;
		}

	    let data = (y - this.mPrevY) / (x - this.mPrevX);
		this.mX = (x + this.mPrevX) / 2;
//		qDebug( "Deriv: x=" + x + ", y=" + y + ", d=" + data );

		this.mPrevX = x;
        this.mPrevY = y;
        return data;
    }

    //**************************************************************
    this.p_sum = function( x )
    {
		if( isNaN( x ) )
			return NaN;
		
        this.mSum += x;
        return this.mSum;
    }
    
    //**************************************************************
    this.p_limit = function( x, low, high )
    {
		if( isNaN( x ) )
			return NaN;
		
        if( x < low )
			return low;
		else if( x > high )
			return high;
		else
        	return x;
    }
    
    //**************************************************************
	this.p_wsgtime = function( x )
	{
		// If this is a timer wrap event, update the high part of the timer
		if( gCalcData[0] & 0x10 )
		{
			this.mTimeHigh += 65536.0; // upper 16 bits of timestamp
			return NaN;
		}
		
		// If no change in state (i.e. no block or unblock event), we're done
		if( !((gCalcData[0] ^ this.mPrevY) & (1 << x)) )
			return NaN;
		
		this.mPrevY = gCalcData[0];
		
		// Each time count is 3us.  Block timestamps are positive and unblock timestamps are negative.
		let block = gCalcData[0] & (1 << x);
		let timeLow = gCalcData[1] | (gCalcData[2] << 8);
		let tstamp = (this.mTimeHigh + timeLow) * (block ? 3 : -3) / 1000000.0;

//		qDebug( "Channel " + x + ": " + tstamp + "us" );
		
		this.mX = tstamp;
		return tstamp;
	}

    //**************************************************************
    this.p_state = function( x )
    {
		if( isNaN( x ) )
			return NaN;
		
		this.mX = Math.abs( x );
        return x >= 0 ? 1.0 : 0.0;
    }
	
    //**************************************************************
    this.p_tig = function( x )
    {
		if( isNaN( x ) )
			return NaN;

		// Remember last block time, but don't return any data
		if( x >= 0 )
		{
			this.mLastBlockTime = x;
			return NaN;
		}
		else // When unblocking, return the block-unblock time
		{
			this.mX = -x;
			return -x - this.mLastBlockTime;
		}
    }
	
    //**************************************************************
    this.p_tbg = function( x1, x2 )
    {
		if( isNaN( x1 ) && isNaN( x2 ) )
			return NaN;
		
		// If we got a block on P1, remember the block time
		if( !isNaN( x1 ) && x1 >= 0 )
		{
			this.mLastBlockTime = x1;
			return NaN;
		}

		// If we got a block on P2, return the time between P1 and P2 blockage
		if( !isNaN( x2 ) && x2 >= 0 && this.mLastBlockTime > 0 )
		{
			let result = x2 - this.mLastBlockTime;
			this.mLastBlockTime = 0;
			this.mX = x2;
			return result;
		}
		
        return NaN;
    }
	
    //**************************************************************
	// Re4turns number of block events
    //**************************************************************
    this.p_blockcount = function( x )
    {
		if( isNaN( x ) || x < 0 )
			return NaN;

		// Return accumulated number of block events with a timestamp of the current block event
		this.mX = x;
		return ++this.mSum;
    }

    //**************************************************************
	// Calculate pendulum period 
    //**************************************************************
    this.p_pendulum = function( x )
    {
		if( isNaN( x ) )
			return NaN;

		// Return data every other block time after the first two blocks
		if( x >= 0 )
		{
			this.mBlockCount++;

			// Remember every other block time
			if( this.mBlockCount & 1 )
			{
				let period = x - this.mLastBlockTime;
				this.mLastBlockTime = x;
				if( this.mBlockCount > 1 )
				{
					this.mX = x;
					return period;
				}
			}
		}

		return NaN;
    }
	
    //**************************************************************
	// Handle timestamp data from the EKG sensor
    //**************************************************************
    this.p_timestamp = function( tstamp, interval )
    {		
		// Extract upper word of the timestamp
		if( tstamp & 0x8000 )
		{
			this.mTimeHigh = tstamp & 0x7fff;
			return NaN;
		}

		// Return the timestamp of the last beat event
		// Note that we are repurposing the photogate timing variables
		this.mX = (tstamp + (this.mTimeHigh << 15)) * interval;
		return this.mX;
	}
	
    //**************************************************************
	// Convert timestamp data from the EKG sensor into heart rate data
    //**************************************************************
    this.p_ekgbeat = function( beatTime, averageWindow )
    {
		if( isNaN( beatTime ) )
			return NaN;
		
		// Get the raw timestamp and average window in seconds
		let bpm = 60.0 / (beatTime - this.mPrevBeatTime);
		this.mPrevBeatTime = beatTime;


		// Only return reasonable heart rates.  We also need two beat times to calculate the rate properly.
		if( bpm < 30 || bpm > 240 || this.mFirstBeatTime )
		{
			this.mFirstBeatTime = false;
			return NaN;
		}

		// Do averaging
		if( averageWindow != 0.0 )
		{
			// Add new beat to the history
			this.mBeatHistory.push( bpm );
			this.mBeatHistory.push( beatTime );

			// Remove any beats that don't fall within the averaging window
			while( this.mBeatHistory.length )
			{
				if( beatTime - mBeatHistory[1] > averageWindow )
				{
					mBeatHistory.shift( );
					mBeatHistory.shift( );
				}
				else
					break;
			}

			// Average all beats within the averaging time window
			let avgCount = 0;
			for( let i = 0; i < this.mBeatHistory.length; i += 2 )
				avgCount += this.mBeatHistory[i];
			bpm = avgCount / (this.mBeatHistory.length / 2);
		}

		// Return beats per minute and the time the last beat occurred
		this.mX = beatTime;
		return bpm;
	}

    //**************************************************************
    this.p_bloodpressure = function( pressure )
    {
		if( !this.mBPCalc )
			return NaN;
		
		this.mBPCalc.AddDataPoint( pressure );

		if( !this.mBPCalc.mHPSData.length )
			return NaN;

		return this.mBPCalc.mHPSData[this.mBPCalc.mHPSData.length - 1];
	}
	
    //**************************************************************
    this.p_bppeak = function( )
    {
		if( !this.mBPCalc || this.mPeakIndex >= this.mBPCalc.mPeakData.length / 2 )
			return NaN;

		// Only report peaks and their times
		this.mX = this.mBPCalc.mPeakData[this.mPeakIndex * 2];
		return this.mBPCalc.mPeakData[this.mPeakIndex++ * 2 + 1];
	}
	
    //**************************************************************
    this.p_bpmap = function( )
    {
		if( !this.mBPCalc || this.mGotData || this.mBPCalc.mMAP == 0 )
			return NaN;

		this.mGotData = true;
		this.mX = this.mBPCalc.mMAPTime;
		return this.mBPCalc.mMAP;
	}
	
    //**************************************************************
    this.p_systolic = function( )
    {
		if( !this.mBPCalc || this.mGotData || this.mBPCalc.mSystolic == 0 )
			return NaN;

		this.mGotData = true;
		this.mX = this.mBPCalc.mSystolicTime;
		return this.mBPCalc.mSystolic;
	}
	
    //**************************************************************
    this.p_diastolic = function( )
    {
		if( !this.mBPCalc || this.mGotData || this.mBPCalc.mDiastolic == 0 )
			return NaN;

		this.mGotData = true;
		this.mX = this.mBPCalc.mDiastolicTime;
		return this.mBPCalc.mDiastolic;
	}
	
    //**************************************************************
    this.p_bppulse = function( )
    {
		if( !this.mBPCalc || this.mBPCalc.mPulseRate == 0 )
			return NaN;

		let result = [ this.mBPCalc.mLastTime, this.mBPCalc.mPulseRate ];
		this.mBPCalc.mPulseRate = 0;
		return result;
	}
	
    //**************************************************************
    this.p_dewpoint = function( temperature, relHumidity )
    {
		let vpSat = 6.11 * Math.pow( 10, (7.5 * temperature) / (237.7 + temperature) );	// Saturation vapor pressure
		let vpAct = (relHumidity * vpSat) / 100;							// Actual vapor pressure

		return (-443.22 + 237.7 * Math.log( vpAct )) / (-Math.log( vpAct ) + 19.08);
	}
	
    //**************************************************************
    this.p_heatindex = function( temperature, relHumidity )
    {
		let vpSat = 6.11 * Math.pow( 10, (7.5 * temperature) / (237.7 + temperature) );	// Saturation vapor pressure (mbar)
		let vpAct = (relHumidity * vpSat) / 100;							// Actual vapor pressure (mbar)

		return temperature + 0.55555 * (vpAct - 10.0);					// Heat index (DegC)
	}
	
    //**************************************************************
    this.p_windchill = function( temperature, windSpeed )
    {
		let tempF = (9 * temperature / 5) + 32;  // Get temperature in fahrenheit
		let windMph = windSpeed * 2.237; // Get wind speed in mph (from m/s)

		let windChillF = 0;
		if( windMph < 3.0 || tempF > 50.0 )  //within the def of NWS algorythm
			windChillF = tempF;
		else
			windChillF = 35.74 + 0.6215 * tempF - 35.75 * Math.pow( windMph, 0.16 ) + 0.4275 * tempF * Math.pow( windMph, 0.16 );

		return 5 * (windChillF - 32) / 9; // Convert wind chill to DegC and return it
	}
	
    //**************************************************************
    this.p_rangesel = function( x1, x2, x3, x4, x5, x6 )
    {
		// Return value based on the current range selection
		if( this.mSensor.mRangeIndex == 0 ) return x1;
		else if( this.mSensor.mRangeIndex == 1 ) return x2;
		else if( this.mSensor.mRangeIndex == 2 ) return x3;
		else if( this.mSensor.mRangeIndex == 3 ) return x4;
		else if( this.mSensor.mRangeIndex == 4 ) return x5;
		else if( this.mSensor.mRangeIndex == 5 ) return x6;
		else return NaN;
	}
	
   //**************************************************************
    this.p_table = function( adc, table )
    {
		// Table must have at least four entries (two xy pairs) and must have an even number of entries
		if( table.length < 4 || (table.length & 1) != 0 )
			return NaN;
		
		// Find the pair of table entries that describe the linear conversion
		let index = 0;
		for( index = 0; index < table.length - 4; index += 2 )
		{
			let in1 = table[index + 2]
			if( adc <= in1 )
				break;
		}
		
		// Calculate the return data by applying a scale/offset based on the input data
		let in0 = table[index];
		let out0 = table[index + 1];
		let in1 = table[index + 2];
		let out1 = table[index + 3];
		
		let scale = (out1 - out0) / (in1 - in0);
		let offset = out0 - scale * in0;
		return adc * scale + offset;
	}
	
    //**************************************************************
    this.p_integral = function( data )
    {
		this.mIntegral += data * this.mSensor.mSamplePeriod;
		return this.mIntegral;
	}
	
    //**************************************************************
    this.p_max = function( data )
    {
		if( data > this.mMax )
			this.mMax = data;
		return this.mMax;
	}
	
    //**************************************************************
    this.p_perbreathvolume = function( flow, volume, windowSize )
    {
		let result = NaN;
		
		// Look for a transition from inhale to exhale
		if( (flow >= 0 && this.mPrevFlow < 0) || (flow < 0 && this.mPrevFlow >= 0) )
		{
			this.mTransitionFlow = flow;
			this.mPositiveTransition = flow >= 0;
			this.mTransitionCount = 1;
		}
		else if( this.mTransitionCount )
			this.mTransitionCount++;

		// If the transition is sufficiently large enough (i.e. it is not just noise), save the total flow rate,
		// calculate the average breath volume, and prep for a new transition detection
		let MIN_BREATH_DETECT_FLOW_TRANSITION = 0.2;
		if( this.mTransitionCount && Math.abs( this.mTransitionFlow - flow ) > MIN_BREATH_DETECT_FLOW_TRANSITION )
		{
			// Save the total volume at the detected transition
			if( this.mVolumeHistory.length >= windowSize * 2 )
				this.mVolumeHistory.pop( );
			this.mVolumeHistory.unshift( volume );

			// if we got a negative transition AND we have enough sample history,
			// Calculate the average volume of the last <window> breaths.
			if( !this.mPositiveTransition && this.mVolumeHistory.length >= 2 )
			{
				let sum = 0;
				for( let i = 0; i < Math.floor( this.mVolumeHistory.length / 2 ); i++ )
					sum += this.mVolumeHistory[i * 2] - this.mVolumeHistory[i * 2 + 1];
				result = sum / Math.floor(this.mVolumeHistory.length / 2);
			}

			this.mTransitionCount = 0;
		}

		this.mPrevFlow = flow;

		return result;
	}
	
    this.Init( ds, sensor );
}
