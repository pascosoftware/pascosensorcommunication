
'use strict'

function BPCalc( lpSmooth, hpSmooth )
{
	//**********************************************************************************
	//
	//**********************************************************************************
	this.Init = function( lpSmooth, hpSmooth )
	{
		this.mSamplePeriod = 0.01;

		this.mHPSmoothCount = hpSmooth;
		this.mLPSmoothCount = lpSmooth;
		this.mHPStartTime = this.mSamplePeriod * (hpSmooth + lpSmooth) / 2;

		this.mLPSmoothHistory = [];
		this.mLPSmoothHistory.length = this.mLPSmoothCount;
		this.mLPSmoothHistory.fill( 0.0 );
		this.mHPSmoothHistory = [];
		this.mHPSmoothHistory.length = this.mLPSmoothCount;
		this.mHPSmoothHistory.fill( 0.0 );

		this.mRawData = [];
		this.mLPData = [];
		this.mHPData = [];
		this.mHPSData = [];
		this.mPeakData = [];

		this.mPeakStarted = false;
		this.mMaxPeakValue = 0;
		this.mNegSlopeCount = 0;
		this.mMapPeakIndex = 0;
		this.mReleasing = false;
		this.mReleaseRate = 0;

		this.mMAP = 0;
		this.mSystolic = 0;
		this.mDiastolic = 0;
		this.mPulseRate = 0;
	}
	
	//**********************************************************************************
	//
	//**********************************************************************************
	this.Recalculate = function( lpSmooth, hpSmooth )
	{
		// Recalculate last run with new smoothing params
		let temp = this.mRawData.slice( );
		this.Init( lpSmooth, hpSmooth );
		for( let d of temp )
			AddDataPoint( d );
	}

	//**********************************************************************************
	//
	//**********************************************************************************
	this.AddDataPoint = function( data )
	{
		// Save the raw sample data
		let index = this.mRawData.length;
		this.mRawData.push( data );
		this.mLastTime = index * this.mSamplePeriod;

		// Put the data on the low pass filtering circular buffer
		this.mLPSmoothHistory[index % this.mLPSmoothCount] = data;

		// If we don't have enough data to smooth, we are done
		if( index < this.mLPSmoothCount - 1 )
			return;

		// Smooth the pressure data to get low pass data
		let avgPressure = 0;
		for( let p of this.mLPSmoothHistory )
			avgPressure += p;
		avgPressure /= this.mLPSmoothCount;

		// Save the low-pass filtered data
		this.mLPData.push( avgPressure );

		// Subtract the low-pass data from the raw data to get the high-pass data
		index = this.mHPData.length;
		this.mHPData.push( this.mLPSmoothHistory[Math.floor(index + this.mLPSmoothCount / 2) % this.mLPSmoothCount] - avgPressure );

		// Put the data on the high pass smoothing circular buffer
		this.mHPSmoothHistory[index % this.mHPSmoothCount] = this.mHPData[index];

		// If we don't have enough data to smooth, we are done
		if( index < this.mHPSmoothCount - 1 )
			return;

		// Smooth the high-pass pressure data
		avgPressure = 0;
		for( let p of this.mHPSmoothHistory )
			avgPressure += p;
		avgPressure /= this.mHPSmoothCount;

		// Store the smoothed pressure
		this.mHPSData.push( avgPressure );

		// Check the slope of the low-pass pressure to detect whether the cuff has started to release pressure for a few seconds.
		if( index >= 50 )
		{
			// Get the slope of the last half-second of low-pass data
			let slope = this.mLPData[index] - this.mLPData[index - 50];

			if( slope >= 0 )
			{
				this.mNegSlopeCount = 0;
	//			mReleasing = false;
			}
			else if( ++this.mNegSlopeCount >= 200 )
			{
				this.mReleasing = true;
				this.mReleaseRate = slope;
			}
		}

		// If we are still pumping up the cuff, don't bother looking for peaks
		if( !this.mReleasing )
			return;

		index = this.mHPSData.length - 1;
		if( index < 2 )
			return;

		// Wait until we cross the inflation threshold pressure
		let MAX_VALID_PEAK_PRESSURE = 1.0;
		let MIN_VALID_PEAK_PRESSURE = 0.1;
		let hpPressure = this.mHPSData[index];
		let prevHpPressure = this.mHPSData[index - 1];

		// Pressure goes from negative to positive starts a peak test
		if( !this.mPeakStarted && hpPressure > 0 && prevHpPressure <= 0 )
		{
			this.mPeakStarted = true;
			this.mMaxPeakValue = hpPressure;
			this.mMaxPeakTime = index * this.mSamplePeriod + this.mHPStartTime;
		}
		// We are looking for a positive peak
		else if( this.mPeakStarted )
		{
			// Pressure goes from positive to negative ends a peak test
			if( hpPressure < 0 && prevHpPressure >= 0 )
			{
				// If the peak is within an appropriate range AND it happened more than 0.2s after the previous peak, add it to the peak list
				let prevPeakTime = this.mPeakData.length >= 2 ? this.mPeakData[this.mPeakData.length - 2] : 0;
				this.mPeakStarted = false;
				if( this.mMaxPeakValue < MAX_VALID_PEAK_PRESSURE &&
					this.mMaxPeakValue > MIN_VALID_PEAK_PRESSURE &&
					index * this.mSamplePeriod > prevPeakTime + 0.2 )
				{
					this.mPeakData.push( this.mMaxPeakTime );
					this.mPeakData.push( this.mMaxPeakValue );

					// Find the pulse rate by measuring the time from the previous peak
					if( this.mPeakData.length >= 4 )
					{
						this.mPulseRate = 60.0 / (this.mPeakData[this.mPeakData.length - 2] - this.mPeakData[this.mPeakData.length - 4]);
					}
				}
			}
			// If looking for a peak, note the max pressure
			else if( hpPressure > this.mMaxPeakValue )
			{
				this.mMaxPeakValue = hpPressure;
				this.mMaxPeakTime = index * this.mSamplePeriod + this.mHPStartTime;
			}
		}

		// If we dropped to 70 mmHg, start looking for systolic and diastolic pressure every second
		if( (this.mSystolic == 0 || this.mDiastolic == 0) && this.mLPData[this.mLPData.length - 1] <= 70 && (index % 100 == 0))
		{
			this.mMAP = 0;
			this.mSystolic = 0;
			this.mDiastolic = 0;
			this.mPulseRate = 0;
			this.mMapPeakIndex = 0;

			// Find the MAP peak
			let maxPeak = 0;
			for( let i = 0; i < this.mPeakData.length; i += 2 )
			{
				if( this.mPeakData[i + 1] > maxPeak )
				{
					maxPeak = this.mPeakData[i + 1];
					this.mMapPeakIndex = i;
				}
			}

			// If we found the MAP peak
			if( this.mMapPeakIndex > 1 && this.mMapPeakIndex < this.mPeakData.length - 2 )
			{
				let x = this.mPeakData[this.mMapPeakIndex];
				let y = this.mPeakData[this.mMapPeakIndex + 1];
				let SYSTOLIC_RATIO = 0.65;
				let DIASTOLIC_RATIO = 0.6;

				// Get the pressure at the MAP peak
				let index = Math.floor( (x - this.mHPStartTime) / this.mSamplePeriod );
				if( index >= 0 && index < this.mLPData.length )
				{
					this.mMAPTime = x;
					this.mMAP = this.mLPData[index];
				}

				// Walk backwards until we find a peak that is sufficiently less than the MAP peak.  This is the systolic pressure
				for( let j = this.mMapPeakIndex - 2; j >= 0; j -= 2 )
				{
					let tx = this.mPeakData[j];
					let ty = this.mPeakData[j + 1];

					if( ty < y * SYSTOLIC_RATIO )
					{
						let index = (tx - this.mHPStartTime) / this.mSamplePeriod;
						if( index >= 0 && index < this.mLPData.length )
						{
							this.mSystolicTime = tx;
							this.mSystolic = this.mLPData[index];
						}
						break;
					}
				}

				// Walk forward until we find a peak that is sufficiently less than the max peak.  This is the diastolic pressure
				for( let j = this.mMapPeakIndex + 2; j < this.mPeakData.length; j += 2 )
				{
					let tx = this.mPeakData[j];
					let ty = this.mPeakData[j + 1];

					if( ty < y * DIASTOLIC_RATIO )
					{
						let index = (tx - this.mHPStartTime) / this.mSamplePeriod;
						if( index >= 0 && index < this.mLPData.length )
						{
							this.mDiastolicTime = tx;
							this.mDiastolic = this.mLPData[index];
						}
						break;
					}
				}
			}

			// We're only done when both systolic and diastolic are done
			if( this.mSystolic == 0 || this.mDiastolic == 0 )
			{
				this.mSystolic = 0;
				this.mDiastolic = 0;
			}
		}
	}

	// Call constructor
	this.Init( lpSmooth, hpSmooth );
}
