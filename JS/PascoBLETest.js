
'use strict'

var gShowRawMeasurements = false;
var gKillMe = false;
var gSampleTimer = 0;
var gDoingCalibration = false;
var gSaveSmartGateModeIndex = 0;
var gGraph = new Graph( "graph1" );
gGraph.DrawGrid( );

//**********************************************************
//
//**********************************************************
function CreateMeasurementDisplay( )
{	
	let table = document.getElementById("MeasurementDisplay");
	let select = document.getElementById("MeasSel");
	let firstMeasurement = "";

	// Empty the table
	table.innerHTML = "";
	while( select.length )
		select.remove( 0 );
		
	// Create the data table header
	let row = document.createElement("tr");
	let hdrText = ["Sensor", "Measurement", "Value", "Units", "Sample Rate", "Range"];
	for( let t of hdrText )
	{
		let cell = document.createElement("th");
		let cellText = document.createTextNode( t );
		cell.appendChild(cellText);
		row.appendChild(cell);
	}
	table.appendChild( row );
	
	// For each device/interface we are connected to
	for( let iface of gPascoHardware.mInterfaces )
	{
		// For each virtual sensor in this wireless device
		for( let sensor of iface.mSensors )
		{
			if( ! sensor )
				continue;

			let row = document.createElement("tr");
			let showSensorLabel = true;
			
			// For each measurement
			for( let meas of sensor.mMeasurements )
			{
				if( !gShowRawMeasurements && meas.mDS.hasOwnProperty( "Internal" ) )
					continue;

				let measName = meas.mDS.NameTag;
				let unitName = meas.mDS.UnitType;
				if( firstMeasurement == "" && !meas.mDS.hasOwnProperty( "Value" ) )
					firstMeasurement = measName;

				let row = document.createElement("tr");

				// Sensor column gets sensor name when first measurement of the sensor is shown
				let cell = document.createElement("td");
				if( showSensorLabel )
					cell.appendChild( document.createTextNode( sensor.mDS.NameTag ) );
				row.appendChild( cell );

				// Measurement column gets measurement name
				cell = document.createElement("td");
				let text = document.createTextNode( measName );
				cell.meas = meas;
				cell.addEventListener( "mousedown", OnEditValue );
				cell.appendChild( text );
				row.appendChild( cell );

				// Measurement data column is empty and gets an id
				cell = document.createElement("td");
				cell.id = meas.mUID.toString( );
				row.appendChild( cell );

				// Units column gets unit name
				cell = document.createElement("td");
				cell.appendChild( document.createTextNode( unitName ) );
				row.appendChild( cell );

				// Add sample rate selector for each Sensor
				cell = document.createElement("td");
				if( showSensorLabel )
				{
					// Set up sample rates
					let rateSelect = document.createElement( "select" );
					rateSelect.addEventListener( "change", OnSampleRateSelect );
					rateSelect.sensor = sensor;

					let rates = ["0Hz", "60s", "30s", "15s", "10s", "5s", "2s", "1Hz", "2Hz", "5Hz", "10Hz", "20Hz", "50Hz", "100Hz", "200Hz", "500Hz", "1000Hz"];
					for( let rate of rates )
					{
						let option = document.createElement("option");
						option.text = rate;
						rateSelect.add(option);

						if( rate == sensor.mDS.DefaultRate )
							rateSelect.selectedIndex = rateSelect.length - 1;

						if( rate == sensor.mDS.MaxRate )
							break;
					}
					cell.appendChild( rateSelect );
				}
				row.appendChild( cell );
				
				// Add range selector if required
				cell = document.createElement("td");
				if( showSensorLabel && sensor.mDS.hasOwnProperty( "Range" ) )
				{
					let rangeSelect = document.createElement( "select" );
					rangeSelect.addEventListener( "change", OnRangeSelect );
					rangeSelect.sensor = sensor;
					for( let range of sensor.mDS.Range.Ranges )
					{
						let option = document.createElement("option");
						option.text = range.NameTag;
						rangeSelect.add(option);
					}
					
					sensor.mRangeIndex = sensor.mDS.Range.DefaultIndex;
					rangeSelect.selectedIndex = sensor.mDS.Range.DefaultIndex;
					cell.appendChild( rangeSelect );
				}
				// Add Smart Gate mode selector if required
				else if( showSensorLabel && iface.mDS.ID == iids.SmartGate )
				{
					let modeSelect = document.createElement( "select" );
					modeSelect.addEventListener( "change", OnSmartGateModeSelect );
					modeSelect.iface = iface;

					// Add list of photogate modes to select from
					for( let id of gDatasheets.SmartGateModes )
					{
						let ds = gDatasheets.FindSensor( id );
						if( !ds )
							continue;

						let option = document.createElement("option");
						option.text = ds.NameTag;
						option.sensorID = id;
						modeSelect.add(option);
					}
					
					modeSelect.selectedIndex = gSaveSmartGateModeIndex;
					cell.appendChild( modeSelect );
				}
				row.appendChild( cell );
				
				table.appendChild( row );

				// Add measurement name to graph selection dropdown (except for parameter values)
				if( !meas.mDS.hasOwnProperty( "Value" ) )
				{
					let option = document.createElement("option");
					option.text = measName;
					select.add(option);
				}
				
				showSensorLabel = false;
			}
		}
		
		if( iface.mDS.ID == iids.GMCounter )
			document.getElementById("GMCounter").style.display = "block";
		if( iface.mDS.ID == iids.AirQuality )
			document.getElementById("O3ZeroCal").style.display = "block";
	}

	// Set the graph to point to the first measurement
	OnMeasSelect( { currentTarget : {value : firstMeasurement}} );
}

//**********************************************************
//
//**********************************************************
function UpdateMeasurementDisplay( )
{
	// For each device/interface we ar connected to
	for( let iface of gPascoHardware.mInterfaces )
	{
		// For each virtual sensor in this wireless device, do a one-shot read
		for( let i = 0; i < iface.mSensors.length; i++ )
		{
			let sensor = iface.mSensors[i];
			if( !sensor )
				continue;

			for( let m of sensor.mMeasurements )
			{			
				let element = document.getElementById( m.mUID.toString( ) );
				if( element )
					element.innerText = m.mLastGoodY.toFixed( 3 );
			}
		}
	}

	gGraph.AutoScale( false );
	gGraph.DrawData( true );

	if( gDoingCalibration )
		UpdateCalibrationDisplay( );

}

//**********************************************************
//
//**********************************************************
function CreateCalibrationDisplay( )
{
	let table = document.getElementById("CalibrationDisplay");

	// Empty the table
	table.innerHTML = "";
	
	if( !gDoingCalibration )
		return;

	// Create the calibration table header
	let row = document.createElement("tr");
	let hdrText = ["enable", "in1", "out1", "in2", "out2", "newIn", "scale", "offset"];
	for( let t of hdrText )
	{
		let cell = document.createElement("th");
		let cellText = document.createTextNode( t );
		cell.appendChild(cellText);
		row.appendChild(cell);
	}
	table.appendChild( row );

	// For each device/interface we are connected to
	for( let iface of gPascoHardware.mInterfaces )
	{
		// For each virtual sensor in this wireless device
		for( let sensor of iface.mSensors )
		{
			// If the sensor doesn't have any user calibrations, skip it
			if( !sensor || sensor.mUserCalibrations.length == 0 )
				continue;

			// Add a row for each user calibration
			for( let cal of sensor.mUserCalibrations )
			{
				row = document.createElement("tr");
				row.cal = cal;
				
				// Add selector checkbox
				let cell = document.createElement("th");
				let checkbox = document.createElement("input");
				checkbox.type = "checkbox";
				checkbox.checked = true;
				cell.appendChild( checkbox );
				let text = document.createTextNode( " " + cal.mMeasurement.mDS.NameTag );
				cell.appendChild( text );
				row.appendChild(cell);
				
				// Add cal data fields
				let values = [cal.mIn1, cal.mOut1, cal.mIn2, cal.mOut2, 0, cal.mScale, cal.mOffset];
				let ids = ["in1", "out1", "in2", "out2", "newIn", "scale", "offset"];
				for( let i = 0; i < ids.length; i++ )
				{
					// Add calibration fields to the row
					cell = document.createElement("th");
					cell.id = cal.mMeasurement.mUID.toString( ) + ids[i];
					let text = document.createTextNode( values[i].toFixed( 3 ) );
					cell.appendChild( text );
					row.appendChild( cell );
				}
				
				table.appendChild( row );
			}
		}
	}

	// Create the calibration buttons
	row = document.createElement("tr");
	let btnInfo = [null, ["pt1", OnCalPt1], null, ["pt2", OnCalPt2], null, ["Reset", OnCalReset], ["Cancel", OnCalCancel], ["Save", OnCalSave]];
	for( let i of btnInfo )
	{
		let cell = document.createElement("th");
		if( i != null )
		{
			let btn = document.createElement( "button" );
			btn.addEventListener( "click", i[1] );
			btn.innerText = i[0];
			cell.appendChild(btn);
		}
		row.appendChild(cell);
	}
	table.appendChild( row );
}

//**********************************************************
//
//**********************************************************
function UpdateCalibrationDisplay( )
{
	// For each device/interface we ar connected to
	for( let iface of gPascoHardware.mInterfaces )
	{
		// For each virtual sensor in this wireless device, do a one-shot read
		for( let i = 0; i < iface.mSensors.length; i++ )
		{
			let sensor = iface.mSensors[i];
			if( !sensor )
				continue;

			for( let cal of sensor.mUserCalibrations )
			{
				let values = [cal.mIn1, cal.mIn2, cal.mMeasurement.mUserCalInputY, cal.mScale, cal.mOffset];
				let ids = ["in1", "in2", "newIn", "scale", "offset"];
				for( let i = 0; i < ids.length; i++ )
				{
					let element = document.getElementById( cal.mMeasurement.mUID.toString( ) + ids[i] );
					if( element )
						element.innerText = values[i].toFixed( 3 );
				}
			}
		}
	}
}

//**********************************************************
//
//**********************************************************
function OnConnectToPascoDeviceBle( event )
{
	gBLEManager.StartScan( );
}

let gSaveDevice = null;

//**********************************************************
//
//**********************************************************
async function OnDisconnectPascoDeviceBle( event )
{
	await gSaveDevice.mDevice.forget( );
}

//**********************************************************
//
//**********************************************************
async function OnConnectToPascoDeviceUsb( event )
{
	const filters = [ { vendorId: 0x0945 } ];
	let usb = await navigator.usb.requestDevice({ filters });
	await usb.open( );
	await usb.claimInterface( 0 );
	await usb.selectConfiguration( 1 );

	await gPascoHardware.AddDevice( usb );
	CreateMeasurementDisplay( );
	CreateCalibrationDisplay( );	
}

//**********************************************************
//
//**********************************************************
async function OnNewBLEDevice( ble )
{
	qDebug( "Connecting to device" );
	
	if( !await ble.Connect( ) )
	{
		qDebug( "Failed to connect" );
		return;
	}
	
	gSaveDevice = ble;
	
	await gPascoHardware.AddDevice( ble );
	CreateMeasurementDisplay( );	
	CreateCalibrationDisplay( );	
}

//**********************************************************
// Update the display if any sensors were added or removed
//**********************************************************
async function OnSensorChanged( )
{
	CreateMeasurementDisplay( );
	CreateCalibrationDisplay( );	
}

//**********************************************************
//
//**********************************************************
async function OnCodeNodeSoundTest( )
{
	let driver = gPascoHardware.mInterfaces[0].mPascoDriver;
	
	for( let i = 0; i < 50; i++ )
		await driver.SetCodeNodeSound( Math.floor( Math.random( ) * 2000 ) + 100 );
	await driver.SetCodeNodeSound( 0 );
}

//**********************************************************
//
//**********************************************************
async function OnCodeNodeLedTest( )
{
	let driver = gPascoHardware.mInterfaces[0].mPascoDriver;

	for( let i = 0; i < 28; i++ )
		await driver.SetCodeNodeLED( i, 0xff );
		
	for( let i = 0; i < 28; i++ )
		await driver.SetCodeNodeLED( i, 0 );
	
	let pattern = [0x0000000, 0x0001000, 0x00729c0, 0x1f8c63f ];
	for( let pass = 0; pass < 10; pass++ )
		for( let bits of pattern )
			await driver.SetCodeNodeMatrixLEDs( bits, 0xff );
	await driver.SetCodeNodeMatrixLEDs( 0, 0 );

	for( let i = 0; i < 255; i += 8 )
		await driver.SetCodeNodeRGBLEDs( i, 0, 0 );
	await driver.SetCodeNodeRGBLEDs( 0, 0, 0 );
	for( let i = 0; i < 255; i += 8 )
		await driver.SetCodeNodeRGBLEDs( 0, i, 0 );
	await driver.SetCodeNodeRGBLEDs( 0, 0, 0 );
	for( let i = 0; i < 255; i += 8 )
		await driver.SetCodeNodeRGBLEDs( 0, 0, i );
	await driver.SetCodeNodeRGBLEDs( 0, 0, 0 );
}

//**********************************************************
//
//**********************************************************
async function OnControlNodeTest( )
{
	gKillMe = false;

	qDebug( "Start Stepper Test" );

	let driver = gPascoHardware.mInterfaces[0].mPascoDriver;

	await driver.ControlNodeSetStepper( true, 19200, 200, 0, false, 0, 0, 0 );
	await sleep( 15000 );
	await driver.ControlNodeStopAccessories( );

	qDebug( "Stepper Test Done" );
}

//**********************************************************
//
//**********************************************************
async function OnControlNodeTest2( )
{
	gKillMe = false;

	qDebug( "Start Servo Test" );

	let driver = gPascoHardware.mInterfaces[0].mPascoDriver;

	while( !gKillMe )
	{
		for( let i = -90; i <= 90 && !gKillMe; i += 1 )
		{
			await driver.ControlNodeSetServo( true, i, false, 0);
			await sleep( 5 );
		}

		for( let i = 90; i >= -90 && !gKillMe; i -= 1 )
		{
			await driver.ControlNodeSetServo( true, i, false, 0);
			await sleep( 5 );
		}
	}

	await driver.ControlNodeStopAccessories( );

	qDebug( "Servo Test Done" );
}

//**********************************************************
//
//**********************************************************
function OnCancelTest( )
{
	gKillMe = true;
}

//**********************************************************
//
//**********************************************************
async function OnReadOneSample( )
{
	// For each virtual sensor in this wireless device, do a one-shot read
	for( let iface of gPascoHardware.mInterfaces )
	{
		for( let sensor of iface.mSensors )
			await iface.ReadOneSample( sensor );
	}

	UpdateMeasurementDisplay( );
}

//**********************************************************
//
//**********************************************************
async function OnStartSampling( )
{
	await gPascoHardware.StartSampling( );
	gSampleTimer = setInterval( UpdateMeasurementDisplay.bind( this ), 200 );
}

//**********************************************************
//
//**********************************************************
async function OnStopSampling( )
{
	await gPascoHardware.StopSampling( );
	clearInterval( gSampleTimer );
	gSampleTimer = null;
}

//**********************************************************
//
//**********************************************************
async function OnZeroSensor( )
{
	await gPascoHardware.ZeroSensor( );
}

//**********************************************************
//
//**********************************************************
function GetSelectedCalibrations( )
{
	let table = document.getElementById("CalibrationDisplay");
	let cals = [];
	
	for( let i = 1; i < table.children.length - 1; i++ )
	{
		let row = table.children[i];
		let checkbox = row.children[0].children[0];
		if( checkbox.checked )
			cals.push( row.cal );
	}

	return cals;
}

//**********************************************************
//
//**********************************************************
async function OnCalibrateSensor( )
{
	if( !gDoingCalibration )
	{
		gDoingCalibration = true;
		
		CreateCalibrationDisplay( );

		// Stash current calibration values in case we want to cancel
		let table = document.getElementById("CalibrationDisplay");
		for( let i = 1; i < table.children.length - 1; i++ )
			table.children[i].cal.Stash( );
	}
}

//**********************************************************
//
//**********************************************************
async function OnCalPt1( )
{
	let cals = GetSelectedCalibrations( );
	for( let cal of cals )
	{
		cal.mIn1 = cal.mMeasurement.mUserCalInputY;
		cal.CalcScaleAndOffset( );
	}
}

//**********************************************************
//
//**********************************************************
async function OnCalPt2( )
{
	let cals = GetSelectedCalibrations( );
	for( let cal of cals )
	{
		cal.mIn2 = cal.mMeasurement.mUserCalInputY;
		cal.CalcScaleAndOffset( );
	}
}

//**********************************************************
//
//**********************************************************
async function OnCalReset( )
{
	let cals = GetSelectedCalibrations( );
	for( let cal of cals )
		cal.Reset( );
}

//**********************************************************
//
//**********************************************************
async function OnCalCancel( )
{
	let cals = GetSelectedCalibrations( );
	for( let cal of cals )
		cal.Restore( );
	gDoingCalibration = false;

	// Destroy the calibration table
	let table = document.getElementById("CalibrationDisplay");
	table.innerHTML = "";
}

//**********************************************************
//
//**********************************************************
async function OnCalSave( )
{
	gDoingCalibration = false;

	// Destroy the calibration table
	let table = document.getElementById("CalibrationDisplay");
	table.innerHTML = "";
}

//**********************************************************
//
//**********************************************************
async function OnGetBatteryInfo( )
{
	await gPascoHardware.RequestBatteryInfo( );
}

//**********************************************************
//
//**********************************************************
function OnGraphResize( event )
{
	gGraph.AutoScale( true );
}

//**********************************************************
//
//**********************************************************
function OnMeasSelect( event )
{
	// For each device/interface we are connected to
	for( let iface of gPascoHardware.mInterfaces )
	{
		// For each virtual sensor in this wireless device, do a one-shot read
		for( let sensor of iface.mSensors )
		{
			if( ! sensor )
				continue;

			// For each measurement
			for( let measurement of sensor.mMeasurements )
			{
				// If we hit the selected measurement, add storage arrays that the graph can use
				if( measurement.mDS.NameTag == event.currentTarget.value )
				{
					measurement.mXStorage = [];
					measurement.mYStorage = [];
					gGraph.mXData = measurement.mXStorage;
					gGraph.mYData = measurement.mYStorage;
				}
				else // Disable storage for unselected measurements
				{
					measurement.mXStorage = null;
					measurement.mYStorage = null;
				}
			}
		}
	}
}

//**********************************************************
//
//**********************************************************
function OnEditValue( event )
{
	let value = prompt( "Please enter new value", event.target.meas.mY );
	if( value != null )
	{
		value = parseFloat( value );
		if( value != NaN )
			event.target.meas.mY = value;
	}
}

//**********************************************************
//
//**********************************************************
function OnRangeSelect( event )
{
	let sensor = event.currentTarget.sensor;
	if( sensor )
		sensor.mRangeIndex = event.currentTarget.selectedIndex;
}

//**********************************************************
//
//**********************************************************
function OnSmartGateModeSelect( event )
{
	let option = event.currentTarget.selectedOptions[0];
	gSaveSmartGateModeIndex = event.currentTarget.selectedIndex;
	event.currentTarget.iface.OnSensorID( 0, option.sensorID );
}

//**********************************************************
//
//**********************************************************
function OnSampleRateSelect( event )
{
	let sensor = event.currentTarget.sensor;
	if( sensor )
		sensor.SetSamplePeriod( event.currentTarget.value );
}

//**********************************************************
//
//**********************************************************
function OnShowPackets( event )
{
	gPascoHardware.mInterfaces[0].mPascoDriver.mLogData = event.currentTarget.checked;
	qDebug( "Packet Logging: " + (event.currentTarget.checked ? "On" : "Off") );
}

//**********************************************************
//
//**********************************************************
function OnShowRawMeasurements( event )
{
	gShowRawMeasurements = event.currentTarget.checked;
	qDebug( "Show Raw Measurements: " + (gShowRawMeasurements ? "On" : "Off") );
	CreateMeasurementDisplay( );
}

//**********************************************************
//
//**********************************************************
function OnSynchronize( event )
{
	gPascoHardware.Synchronize( );
}

//**********************************************************
//
//**********************************************************
async function OnGMSetVoltage( event )
{
	let value = Number( prompt( "Please enter voltage between 180V and 700V", "500" ) );
	if( value < 180 || value > 700 )
		qDebug( "Value " + value + " is out of range.  Please try again" );
	else
	{
		for( let iface of gPascoHardware.mInterfaces )
			if( iface.mDS.ID == iids.GMCounter && iface.mPascoDriver )
				await iface.mPascoDriver.GMSetVoltage( value, value );
	}
}

//**********************************************************
//
//**********************************************************
async function OnGMEnableBeep( event )
{
	qDebug( "GM Beep " + (event.currentTarget.checked ? "en" : "dis") + "abled" );

	for( let iface of gPascoHardware.mInterfaces )
		if( iface.mDS.ID == iids.GMCounter && iface.mPascoDriver )
			await iface.mPascoDriver.GMEnableBeeper( event.currentTarget.checked );
}

//**********************************************************
//
//**********************************************************
async function OnGMGetStatus( event )
{
	for( let iface of gPascoHardware.mInterfaces )
		if( iface.mDS.ID == iids.GMCounter && iface.mPascoDriver )
		{
			let result = await iface.mPascoDriver.GMGetStatus( );
			if( result.mSuccess )
				qDebug( "Status: Voltage = " + result.mVoltage + ", Beeper = " + result.mBeeperEnabled );
		}
}

//**********************************************************
// Click will zero calibrate the O3 sensor.
// Shift-click will remove any prior calibration (for debugging purposes only).
//**********************************************************
async function OnCalZeroO3( event )
{
	for( let iface of gPascoHardware.mInterfaces )
		if( iface.mDS.ID == iids.AirQuality && iface.mPascoDriver )
		{
			let success = await iface.mPascoDriver.O3ZeroCal( event.shiftKey );
			qDebug( (event.shiftKey ? "Remove " : "") + "Zero Cal Ozone: success = " + success );
		}
}

gBLEManager.mScanCallback = OnNewBLEDevice;
gPascoHardware.mOnSensorChanged = OnSensorChanged;
