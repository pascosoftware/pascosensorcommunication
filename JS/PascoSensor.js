
'use strict'

//*******************************************************************
//
//*******************************************************************
function PascoSensor( ds )
{
    //**************************************************************
    this.Init = function( ds )
    {
        this.mDS = ds;
        this.mMeasurements = [];
        this.mFactoryCalibrations = [];
        this.mUserCalibrations = [];
        this.mSampleCount = 0;
		this.mRangeIndex = -1;
		this.mBPCalc = (ds.ID == sids.BloodPressure ) ? new BPCalc( 50, 20 ) : null;
        this.mSGSpacing = 0.015; // Nominal spacing between Smart Gate P1 and P2
		
        // Create integer default sample period from default sample rate string
		this.SetSamplePeriod( ds.DefaultRate );

        // Create calibration data for each factory calibration and populate it with nominal values from the datasheet
        if( ds.hasOwnProperty( "FactoryCals" ) )
        {
            for( let i = 0; i  < ds.FactoryCals.length; i++ )
            {
                let cal = ds.FactoryCals[i];
                this.mFactoryCalibrations.push( new LinearCalibration( cal[0], cal[1], cal[2], cal[3] ) );
            }
        }

        // Create calibration data for each user calibration and populate it with nominal values from the datasheet
        if( ds.hasOwnProperty( "UserCals" ) )
        {
            for( let i = 0; i  < ds.UserCals.length; i++ )
            {
                let cal = ds.UserCals[i];
                this.mUserCalibrations.push( new LinearCalibration( cal[0], cal[1], cal[2], cal[3] ) );
            }
        }

        // Create measurement info for each measurement
        for( let i = 0; i < ds.Measurements.length; i++ )
        {
            let measDS = ds.Measurements[i];
            let meas = new PascoMeasurement( measDS, this );
            this.mMeasurements.push( meas );
        }
    }

    //**************************************************************
	this.SetSamplePeriod = function( periodStr )
	{
        if( periodStr.slice( -1 ) == 'z' ) // Hz
            this.mSamplePeriod = 1.00 / parseInt( periodStr.slice( 0, periodStr.length - 2 ) );
        else // seconds
            this.mSamplePeriod = parseInt( periodStr.slice( 0, periodStr.length - 1 ) );
	}
	
    //**************************************************************
    this.StartSampling = function( )
    {
        this.mSampleCount = 0;
		if( this.mBPCalc )
			this.mBPCalc.Init( 50, 20 );
        for( let meas of this.mMeasurements )
            meas.StartSampling( );
    }

    //**************************************************************
    this.ZeroSensor = function( )
    {
        for( let meas of this.mMeasurements )
			meas.ZeroMeasurement( );
    }

    //**************************************************************
    // Process raw data from the sensor and turn it into measurement data
    this.ProcessData = function( data )
    {
		for( let i = 0; i < data.length; i += this.mDS.SampleSize )
		{
			gCalcData = data.slice( i, i + this.mDS.SampleSize );

			// Do all measurement calculations.  They must be defined in dependency order in the datasheet.
			for( let meas of this.mMeasurements )
				meas.DoCalc( );

            this.mSampleCount++;
		}
    }

    this.Init( ds );
}

//*******************************************************************
//
//*******************************************************************
function LinearCalibration( in1, out1, in2, out2 )
{
	// Remember the original values to reset to
	this.mInitIn1 = in1;
	this.mInitOut1 = out1;
	this.mInitIn2 = in2;
	this.mInitOut2 = out2;
	
	this.CalcScaleAndOffset = function( )
	{
        this.mScale = (this.mOut2 - this.mOut1) / (this.mIn2 - this.mIn1);
        this.mOffset = this.mOut1 - this.mScale * this.mIn1;
 	}
	
	this.SetCalData = function( in1, out1, in2, out2 )
	{
		this.mIn1 = in1;
		this.mOut1 = out1;
		this.mIn2 = in2;
		this.mOut2 = out2;
		
		this.CalcScaleAndOffset( );
 	}

	// Return to original factory values from the datasheet
	this.Reset = function( )
	{
		this.SetCalData( this.mInitIn1, this.mInitOut1, this.mInitIn2, this.mInitOut2 );
	}
	
	// Temporarily save values in case we want to undo calibration changes
	this.Stash = function( )
	{
		this.mSaveIn1 = this.mIn1;
		this.mSaveOut1 = this.mOut1;
		this.mSaveIn2 = this.mIn2;
		this.mSaveOut2 = this.mOut2;
	}
	
	// Restore stashed values
	this.Restore = function( )
	{
		this.SetCalData( this.mSaveIn1, this.mSaveOut1, this.mSaveIn2, this.mSaveOut2 );
	}
	
	this.SetCalData( in1, out1, in2, out2 );
}
