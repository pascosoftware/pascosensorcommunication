
'use strict'

// Pasco BLE Commands
var pbc =
{
	// Characteristic identifiers
	charCmd					: 2,	// Commands sent to the sensor
	charRsp					: 3,	// Responses and async events from the sensor
	charData				: 4,	// Sample data from the sensor
	charAck					: 5,	// Sample data ack from the sensor

	// BLE Events
	EvtSync					: 0x81,
	EvtSensorID				: 0x82,
	EvtBatteryStatus		: 0x85,

	// BLE commands
	SetSampleConfig 		: 0x01,
	ReadOneSample 			: 0x05,
	StartSampling 			: 0x06,
	StopSampling 			: 0x07,
	RequestSensorID			: 0x08, // Tell interface to send events to indicate what sensors/accessories are plugged in
	StartBurstXfer			: 0x09,	// Prepare to read/write data from/to temporary RAM storage
	WriteBurstData			: 0x0a,	// Send the read/write data, 1st byte is a sequence count
	BurstDataDone			: 0x0b,	// Ask for or indicate completion of a burst
	XferBurstRam			: 0x0e,
	XferPasportSpiData		: 0x0f,	// Copy 1-16 bytes of data to/from SPI sensor
	GetFirmwareVersion 		: 0x15,
	RequestBatteryInfo 		: 0x18,
	SetRange		 		: 0x28,
	StartSync		 		: 0x30,
	CustomCmd				: 0x37,
	GenericResult 			: 0xc0,

	// Flags for transferring data to/from burst RAM
	BurstXferReadFlag		: 0x80,
	BurstXferLocFlash		: 0x01,
	BurstXferLocSpiSensor	: 0x02,
	BurstXferLocNvr			: 0x03,
	BurstXferLocLog			: 0x04,
	BurstXferLocFlash2		: 0x05,

	// Sensor non-volatile storage tags
	NVStorageNameTag		: 1,
	NVStorageFactoryCalTag1	: 2, // Sensor[0] factory cal storage
	NVStorageFactoryCalTag2	: 3, // Sensor[1] factory cal storage
	NVStorageFactoryCalTag3	: 4, // Sensor[2] factory cal storage
	NVStorageFactoryCalTag4	: 5, // Sensor[3] factory cal storage

	// Code Node commands
	CodeNodeSetLed			: 2,
	CodeNodeSetLeds			: 3,
	CodeNodeSetSoundFreq	: 4,

	// Control Node commands
	CtrlNodeSetServo		: 3, // Enables PWM to servo
	CtrlNodeSetStepper		: 4, // Enables stepper motor
	CtrlNodeSetSignals		: 5, // Sets motor control signals directly
	CtrlNodeGetStepperInfo	: 8, // Get stepper remaining distance and remaining angular velocity to accelerate to
	CtrlNodeStopAccessories	: 9, // Turn off all steppers, servos, and accessories
	CtrlNodeSetBeeper		: 10, // Set beeper frequency
	
	// Smart Gate Commands
	SmartGateGetStatus		: 1, // Get Smart Gate block/unblock status and aux plug detect
	SmartGateSetMode		: 2, // Sets whether P1, P2, and/or Laser channels are enabled
	
	// GM Counter commands
	GMSetVoltage			: 1, // Set the geiger tube voltage between 180V and 700V
	GMEnableBeeper			: 2, // Enable/disable the beeper
	GMGetStatus				: 4, // Retrieve current voltage and beeper settings
	
	// Air Quality commands
	AQZeroCalO3				: 1, // Zero the O3 measurement and save it in the sensor
}

function PascoBleDriver( )
{
	this.mLogData = false;
	this.mNumSensors = 1;
	this.mPendingGenericRequest = null; // Holds the generic request that is currently being handled
	this.mSensorInfo = [];
	this.mLogSampleData = false;
	this.mLogRequests = false;
	this.mUsbDevice = null;
	this.mBleDevice = null;
	this.mBurstPktDataSize = 16; // Max number of data bytes per burst transfer data packet

	this.mOnSensorID = null;
	this.mOnBatteryStatus = null;
	this.mOnSync = null;

	//*************************************************************************
	//
	//*************************************************************************
	this.GetFirmwareVersion = async function( )
	{
		let result = await this.SendGenericCommand( 0, pbc.GetFirmwareVersion, null, 1000 );
		if( !result.mSuccess )
		{
			qDebug( "GetFirmwareVersion failed" );
			return {mSuccess : false};
		}

		var version = {};
		version.mSuccess = true;
		version.mMajor = result.mData[3];
		version.mMinor = result.mData[4];
		version.mDate = "";
		for( var i = 4; i < result.mData.length; i++ )
			version.mDate += String.fromCharCode( result.mData[i] );

		qDebug( "Got firmware version" );

		return version;
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.RequestSensorID = async function( )
	{
		await this.SendGenericCommand( 0, pbc.RequestSensorID, [], 0 );
	}

	//*************************************************************************
	// Tells sensor to send us a battery status event
	//*************************************************************************
	this.RequestBatteryInfo = async function( )
	{
		await this.SendGenericCommand( 0, pbc.RequestBatteryInfo, [], 0 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.SetSampleConfig = async function( channel, periodUS, sampleSize, resetOnStart )
	{
		let cmd = new Uint8Array( 6 );
		cmd.set( [(periodUS & 0xff),
				   (periodUS >> 8) & 0xff,
				   (periodUS >> 16) & 0xff,
				   (periodUS >> 24) & 0xff,
					sampleSize ? sampleSize & 0xff : 0,
					resetOnStart ? 1 : 0] );

		let result = await this.SendGenericCommand( channel + 1, pbc.SetSampleConfig, cmd, 0 );
		return result.mSuccess;
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.SetRange = async function( channel, range )
	{		
		let cmd = new Uint8Array( 3 );
		cmd.set( [0, 0, range] );

		let result = await this.SendGenericCommand( channel + 1, pbc.SetRange, cmd, 1000 );
		return result.mSuccess;
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.StartSampling = async function( startFrames )
	{
		// Clear sensor state to start fresh
		this.mSensorInfo = [];

		let data = null;
		if( Array.isArray( startFrames ) )
		{
			let cmd = [];
			for( var i = 0; i < startFrames.length; i++ )
			{
				cmd.push( startFrames[i] & 0xff );
				cmd.push( startFrames[i] >> 8 );
			}

			data = new Uint8Array( cmd.length );
			data.set( cmd );
		}

		let result = await this.SendGenericCommand( 0, pbc.StartSampling, data, 0 );
		return result.mSuccess;
	}
	
	//*************************************************************************
	//
	//*************************************************************************
	this.StopSampling = async function( )
	{
		let result = await this.SendGenericCommand( 0, pbc.StopSampling, null, 0 );
		return result.mSuccess;
	}
	
	//*************************************************************************
	//
	//*************************************************************************
	this.ReadOneSample = async function( channel, numBytes )
	{
		let cmd = [numBytes];
		let data = await this.SendGenericCommand( channel + 1, pbc.ReadOneSample, cmd, 1000 );
		if( data.mSuccess && data.mData.length >= 3 )
			return data.mData.slice( 3, data.mData.length );

		return [];
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.StartSync = async function( period, count )
	{
		let cmd = [count, period & 0xff, (period >> 8) & 0xff, (period >> 16) & 0xff];
		await this.SendGenericCommand( 0, pbc.StartSync, cmd, 1000 );
	}
	//*******************************************************************
	//
	//*******************************************************************
	this.XferBurstRam = async function( read, location, addr, count )
	{
		let cmd = [(read ? pbc.BurstXferReadFlag : 0) | location, addr & 0xff, (addr >> 8) & 0xff, 
					(addr >> 16) & 0xff, (addr >> 24) & 0xff, count & 0xff, (count >> 8) & 0xff];
		let response = await this.SendGenericCommand( 0, pbc.XferBurstRam, cmd, 1000 );

		// Return no bytes transferred if there was an error
		if( !response.mSuccess || response.mData.length < 5 )
			return 0;

		// If success, return the number of bytes transferred
		return response.mData[3] + (response.mData[4] << 8);
	}

	//*******************************************************************
	//
	//*******************************************************************
	this.StartBurstXfer = async function( read, total, packetSize )
	{
		let cmd = [read, total & 0xff, (total >> 8) & 0xff, packetSize];
		let response = await this.SendGenericCommand( 0, pbc.StartBurstXfer, cmd, 0 );
		return response.mSuccess;
	}

	//*******************************************************************
	//
	//*******************************************************************
	this.WriteBurstData = async function( sequence, dataIn )
	{
		let data = [sequence].concat( dataIn );
		let response = await this.SendGenericCommand( 0, pbc.WriteBurstData, data, 0 );
		return response.mSuccess;
	}

	//*******************************************************************
	// Tell the device we're done sending burst data packets to it
	// OR that the device needs to re-send some missing packets
	//*******************************************************************
	this.BurstXferDone = async function( missingPktMask )
	{
		let cmd = [missingPktMask & 0xff, (missingPktMask >> 8) & 0xff, (missingPktMask >> 16) & 0xff, (missingPktMask >> 24) & 0xff ];
		let response = await this.SendGenericCommand( 0, pbc.BurstDataDone, cmd, 0 );
		return response.mSuccess;
	}

	//*******************************************************************
	// Wait for a burst transfer completion or timeout
	//*******************************************************************
	this.WaitForBurstDone = async function( timeout )
	{
		return new Promise( async function( resolve ) {
			let timer = 0;
			if( timeout )
			{
				timer = setTimeout( function( ) { 
					if( this.mPendingGenericRequest )
					{
						qDebug( "Burst done timeout: req=" + this.mPendingGenericRequest.mCmd );
						this.mPendingGenericRequest.mResolve( false );
					}
					this.mPendingGenericRequest = null;
				}.bind( this ), timeout );
			}
			this.mPendingGenericRequest = { mCmd : 0, mTimer : timer, mResolve : resolve };
		}.bind( this ) );
	}

	//*******************************************************************
	// Read a block of data from sensor RAM.  Data comes in as a set of
	// 16-byte packets, each with a sequence number so that we can detect missing packets
	//
	// Packet sequence:
	//
	// -> StartBurstXfer(read, totalCount)
	// Repeat the following sequence until no missing packets are reported
	// <- WriteBurstData(seq)
	// <-    ...
	// <- WriteBurstData(seq)
	// <- BurstXferDone(checksum)
	// -> BurstXferDone(missingPktMask)
	//
	// Success when no missing packets and checksum is correct
	//*******************************************************************
	this.DoBurstReadTransfer = async function( count )
	{
		const FailTimeoutCount = 3;
		const MaxPasses = 5;

		let numPackets = Math.floor( (count + this.mBurstPktDataSize - 1) / this.mBurstPktDataSize );
		let lastPacketSize = (count % this.mBurstPktDataSize) ? (count % this.mBurstPktDataSize) : this.mBurstPktDataSize;
	
		// Place data where the event handler can get to them
		this.mBurstPacketMask = (1 << numPackets) - 1; // Set a bit for each packet to be recvd
		this.mBurstRecvData = new Array( numPackets ); // Create array to hold the burst data packets
		this.mBurstRecvCount = count;
	
		if( !await this.StartBurstXfer( true, count, this.mBurstPktDataSize ) )
		{
			qDebug( "Failed to start burst read transfer" );
			return [];
		}

		// Try up to five times in case all of the packets didn't make it
		for( let pass = 0; pass < MaxPasses; pass++ )
		{
			let failCount = 0;

			// If we didn't get a completion response from the device, something bad has happened
			if( !await this.WaitForBurstDone( 3000 ) )
			{
				qDebug( "Failed read burst transfer due to DONE request not being received" );
				return [];
			}

			// If we got all of our packets, we're done.
			if( this.mBurstPacketMask == 0 )
				break;
			else // Otherwise, request a resend of the missing packets
			{
				qDebug( "Not all burst packets made it from the device.  Retrying: " + ToHex16( this.mBurstPacketMask) + "/" + ToHex16( (1 << numPackets) - 1) );

				if( !await this.BurstXferDone( this.mBurstPacketMask ) )
				{
					qDebug( "Failed to request resending of missing packets for burst read transfer" );
					return [];
				}
			}
		}

		// Combine the burst data packets and return as one array
		let buffer = new ArrayBuffer( count );
		let data = new Uint8Array( buffer )
		for( let i = 0; i < this.mBurstRecvData.length; i++ )
			data.set( this.mBurstRecvData[i], i * this.mBurstPktDataSize );
		return data;
	}

	//*******************************************************************
	// Transfer 1-16 bytes of SPI data to/from a Pasport sensor via an AirLink3
	//*******************************************************************
	this.XferSinglePasportSpiData = async function( channel, wrData, rdLen )
	{
		const MAX_SPI_XFER_PKT_SIZE = 16;

		// 0-16 bytes can be transferred at a time
		if( wrData.length > MAX_SPI_XFER_PKT_SIZE || rdLen > MAX_SPI_XFER_PKT_SIZE  )
		{
			qDebug( "XferSinglePasportSpiData: Rd/Wr count " + rdLen + "/" + wrData.length + " is incorrect" );
			return false;
		}

		// Insert read length to the front of the command buffer and send the data
		wrData.unshift( rdLen );
		let result = await this.SendGenericCommand( channel, pbc.XferPasportSpiData, wrData, 1000 );
		if( !result.mSuccess )
			return false;
		
		// Return the SPI response data
		return {mSuccess : true, mData : result.mData.slice( 3 ) };
	}

	//*******************************************************************
	// Transfer SPI data to/from a Pasport sensor via an AirLink3
	//*******************************************************************
	this.XferPasportSpiData = async function( channel, wrData, rdLen )
	{
		const MAX_SPI_XFER_PKT_SIZE = 16;

		// If writing and reading, the write packet must be < 16 bytes
		if( wrData.length > MAX_SPI_XFER_PKT_SIZE && rdLen )
		{
			qDebug( "XferPasportSpiData: Write count %d is too long", wrLen );
			return {mSuccess:false};
		}
		let wrLen = wrData.length;
		let wrSoFar = 0;
		let rdSoFar = 0;
		let rdData = [];
		
		// Get the write/read count for the first request
		let wrBlkCount = wrLen > MAX_SPI_XFER_PKT_SIZE ? MAX_SPI_XFER_PKT_SIZE : wrLen;
		let rdBlkCount = rdLen > MAX_SPI_XFER_PKT_SIZE ? MAX_SPI_XFER_PKT_SIZE : rdLen;

		// Do the first transfer
		let result = await this.XferSinglePasportSpiData( channel, wrData, rdBlkCount );
		if( !result.mSuccess )
		{
			qDebug( "XferPasportSpiData: Failed to send first SPI write packet" );
			return {mSuccess:false};
		}

		wrSoFar += wrBlkCount;
		rdSoFar += rdBlkCount;
		rdData = result.mData;
		
		// If writing data, write the rest of it here
		while( wrLen < wrSoFar )
		{
			wrBlkCount = wrLen - wrSoFar;
			if( wrBlkCount > MAX_SPI_XFER_PKT_SIZE )
				wrBlkCount = MAX_SPI_XFER_PKT_SIZE;
			let result = await this.XferSinglePasportSpiData( channel, wrData.slice( wrSoFar, wrSoFar + wrBlkCount ), 0 );
			if( !result.mSuccess )
			{
				qDebug( "XferPasportSpiData: Failed to send next SPI write packet" );
				return {mSuccess:false};
			}

			wrSoFar += wrBlkCount;
		}

		// If reading data, read the rest of it here.
		while( rdLen < rdSoFar )
		{
			rdBlkCount = rdLen - rdSoFar;
			if( rdBlkCount > MAX_SPI_XFER_PKT_SIZE )
				rdBlkCount = MAX_SPI_XFER_PKT_SIZE;
			let result = await this.XferSinglePasportSpiData( channel, [], rdBlkCount );
			if( !result.mSuccess )
			{
				qDebug( "XferPasportSpiData: Failed to recv next SPI read packet" );
				return {mSuccess:false};
			}

			rdSoFar += rdBlkCount;
			rdData = rdData.concat( result.mData );
		}

		return {mSuccess:true, mData:rdData};
	}
	
	//*************************************************************************
	//
	//*************************************************************************
	this.SetCodeNodeLED = async function( index, intensity )
	{
		let cmd = [pbc.CodeNodeSetLed, index, intensity];
		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.SetCodeNodeRGBLEDs = async function( r, g, b, intensity )
	{
		let cmd = [pbc.CodeNodeSetLeds, r, g, b, 0x80, intensity];
		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.SetCodeNodeSound = async function( frequency )
	{
		let cmd = [pbc.CodeNodeSetSoundFreq, frequency & 0xff, (frequency >> 8) & 0xff];
		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.SetCodeNodeMatrixLEDs = async function( bits, intensity )
	{
		let cmd = [pbc.CodeNodeSetLeds, bits & 0xff, (bits >> 8) & 0xff, (bits >> 16) & 0xff, (bits >> 24) & 0xff, intensity];
		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.ControlNodeSetServo = async function( channel1, degrees1, channel2, degrees2 )
	{
		let timeOn1 = 150 + degrees1; // 1.5ms +/- 10us/degree
		let timeOn2 = 150 + degrees2;
		let period = 2000; // 20ms/50Hz PWM period to the servo

		let cmd = [pbc.CtrlNodeSetServo, (channel1 ? 1 : 0) | (channel2 ? 2 : 0),
			timeOn1 & 0xff, (timeOn1 >> 8) & 0xff, period & 0xff, (period >> 8) & 0xff,
			timeOn2 & 0xff, (timeOn2 >> 8) & 0xff, period & 0xff, (period >> 8) & 0xff];

		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.ControlNodeSetStepper = async function( channel1, speed1, accel1, distance1, channel2, speed2, accel2, distance2 )
	{
		let cmd = [pbc.CtrlNodeSetStepper, (channel1 ? 1 : 0) | (channel2 ? 2 : 0),
			speed1 & 0xff, (speed1 >> 8) & 0xff, accel1 & 0xff, (accel1 >> 8) & 0xff, distance1 & 0xff, (distance1 >> 8) & 0xff,
			speed2 & 0xff, (speed2 >> 8) & 0xff, accel2 & 0xff, (accel2 >> 8) & 0xff, distance2 & 0xff, (distance2 >> 8) & 0xff,
			(distance1 >> 16) & 0xff, (distance2 >> 16) & 0xff];

		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.ControlNodeStopAccessories = async function( )
	{
		let cmd = [pbc.CtrlNodeStopAccessories];

		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.SmartGateSetMode = async function( enP1, enP2, enLaser )
	{
		let cmd = [pbc.SmartGateSetMode, (enP1 ? 1 : 0) + (enP2 ? 2 : 0) + (enLaser ? 4 : 0)];
		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*************************************************************************
	//
	//*************************************************************************
	this.SmartGateGetStatus = async function( )
	{
		let cmd = [pbc.SmartGateGetStatus];
		let result = await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
		return result.mSuccess ? -1 : result.mData[3];
	}

	//*******************************************************************
	// if startVoltage == 0 && endVoltage == 0, set to nominal voltage as defined in firmware
	// if startVoltage != 0 && endVoltage != 0, inc/dec voltages before each sample by ~8.2V (the voltage granularity of the hardware)
	// if startVoltage == endVoltage, just set the voltage
	//*******************************************************************
	this.GMSetVoltage = async function( startVoltage, endVoltage )
	{
		let cmd = [ pbc.GMSetVoltage, startVoltage & 0xff, startVoltage >> 8, endVoltage & 0xff, endVoltage >> 8 ];
		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*******************************************************************
	// 
	//*******************************************************************
	this.GMEnableBeeper = async function( enable )
	{
		let cmd = [ pbc.GMEnableBeeper, enable ];
		return await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
	}

	//*******************************************************************
	// 
	//*******************************************************************
	this.GMGetStatus = async function( )
	{
		let cmd = [ pbc.GMGetStatus ];
		let result = await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
		if( result && result.mSuccess && result.mData.length >= 6 )
		{
			result.mBeeperEnabled = result.mData[3];
			result.mVoltage = result.mData[4] + (result.mData[5] << 8);
		}
		return result;
	}
	
	//*******************************************************************
	// Calibrate O3 sensor at zero O3.
	// removeCal:
	//	false = do calibration
	//	true = remove any prior calibration from the sensor.
	//*******************************************************************
	this.O3ZeroCal = async function( removeCal )
	{
		let cmd = [ pbc.AQZeroCalO3, removeCal ? 1 : 0 ];
		let result = await this.SendGenericCommand( 0, pbc.CustomCmd, cmd, 1000 );
		return result.mSuccess;
	}
	
	//*************************************************************************
	//
	//*************************************************************************
	this.SendGenericCommand = async function( serviceIndex, cmd, data, timeout )
	{
		// Cannot start a new request before the previous one completed
		if( this.mPendingGenericRequest != null )
		{
			qDebug( "Unexpected generic request: req=" + cmd + ", pending=" + this.mPendingGenericRequest.mCmd );
			return {mSuccess : false};
		}

		return new Promise( async function( resolve ) {
			// Remember the request completion functions and set up a timeout in case we don't get an expected response
			let timer = 0;
			if( timeout )
			{
				timer = setTimeout( function( ) { 
					if( this.mPendingGenericRequest )
					{
						qDebug( "Generic command timeout: req=" + this.mPendingGenericRequest.mCmd );
						this.mPendingGenericRequest.mResolve( {mSuccess : false} );
					}
					this.mPendingGenericRequest = null;
					}.bind( this ), timeout );
			}
			this.mPendingGenericRequest = { mCmd : cmd, mTimer : timer, mResolve : resolve };
		
			// Add the command byte to the beginning of the packet
			let newData = null
			if( data )
			{
				newData = new Uint8Array( data.length + 1 );
				newData.set( data, 1 );
			}
			else
				newData = new Uint8Array( 1 );
			newData[0] = cmd;

			// Send the command to the requested channel of the wireless sensor
			let success = await this.BleWrite( PascoCharID( serviceIndex, 2 ), newData );
			if( success )
			{
				// If not waiting for a response, we are done when the write is done
				if( !timeout )
				{
					this.mPendingGenericRequest.mResolve( {mSuccess : true} );
					this.mPendingGenericRequest = null;
				}
			}
			else
			{
				qDebug( "Generic command write failed" );
				this.mPendingGenericRequest.mResolve( {mSuccess : false});
				this.mPendingGenericRequest = null;
			}
		}.bind( this ) );
	}

	let retryCacheSize = 0x20;
	let retryCacheSizeMask = 0x1f;
	let RETRY_FLAG = 0x80;

	//*******************************************************************
	//
	//*******************************************************************
	this.ReadSampleData = function( channel, count, alignCount )
	{
		// A little sanity check
	    if( !this.mSensorInfo[channel] )
	        return [];
	    
		var info = this.mSensorInfo[channel];

		// If incomplete burst mode transfer, we're done
		if( info.mBurstMode && !info.mBurstDone )
			return [];

		// Only return as much as our buffer can hold
	    var dataCount = info.mData.length;
	    if( dataCount > count )
	        dataCount = count;
	    
//		qDebug( "Read Sample Data: cnt=" + dataCount + "/" + info.mData.length + ", align=" + alignCount );

	    // Make sure data is an even multiple of bytes per sample
	    if( alignCount && dataCount >= alignCount )
	    {
			dataCount -= dataCount % alignCount;
	        return info.mData.splice( 0, dataCount );
	    }

	    return [];
	}

	//*************************************************************************
	// Handles sample data packets recvd from the BLE connection
	//*************************************************************************
	this.OnSampleData = async function( sensor, data )
	{
		var info;

//		qDebug( "OnSampleData: " + data.length + " bytes" );
//		qHexDump( data );

		// Create a sensor entry if one is not already created
		if( !this.mSensorInfo[sensor] )
		{
			this.mSensorInfo[sensor] = 
			{
				mBurstMode : false,
				mBurstDone : false,
				mBurstTriggerTime : -1,
        		mRecvPktCountForAck : 0,
        		mLastProcessedSequence : retryCacheSizeMask,
        		mLastRecvdSequence : retryCacheSizeMask,
		        mRetryCache : [],
 		        mData : [],
			}
			for( var i = 0; i < retryCacheSize; i++ )
				this.mSensorInfo[sensor].mRetryCache[i] = [];	
		}

		info = this.mSensorInfo[sensor];

		var sequence = data[0];
		var timeToAck = false;

    	// If we are just being asked to return the ack status
    	if( data.length == 1 )
    	{
        	timeToAck = true;
		}

    	else // We got a sample data packet
		{
			var isRetry = (sequence & RETRY_FLAG) != 0;
        	sequence &= retryCacheSizeMask;

			// Determine if this packet is an expected missing packet
        	var isMissing;
        	if( info.mLastRecvdSequence >= info.mLastProcessedSequence )
            	isMissing = sequence > info.mLastProcessedSequence && sequence < info.mLastRecvdSequence;
        	else
            	isMissing = sequence > info.mLastProcessedSequence || sequence < info.mLastRecvdSequence;

	        // If this packet falls between the last processed and last received packets, add it to the cache.
	        if( isMissing )
			{
		        if( this.mLogSampleData )
					qDebug( "Got " + (isRetry ? "retried" : "missing") + " packet 0x", (sequence & info.mRetryCacheSizeMask).toString( 16 ) );

	            info.mRetryCache[sequence] = data;

				// Check the cache to see if the re-sent packet and any following packets can now be processed
				var begin = (info.mLastProcessedSequence + 1) & retryCacheSizeMask;
				var end = (info.mLastRecvdSequence + 1) & retryCacheSizeMask;
				var i;
				for( i = begin; i != end; i = (i + 1) & retryCacheSizeMask )
				{
		            var cache = info.mRetryCache[i];
					if( cache.length )
					{
						cache.forEach( function( d, i ) { if( i > 0) info.mData.push( d ); } );
//						info.mData = info.mData.concat( cache.slice( 1 ) );
						info.mLastProcessedSequence = cache[0] & retryCacheSizeMask;
						cache = [];
	                
						if( this.mLogSampleData )
		                    qDebug( "Processing cached packet 0x" + info.mLastProcessedSequence.toString( 16 ) );
					}
					else
		                break;
				}
			}
	        
	        // This is not a missing packet in the sequence AND it's not a rogue retry packet
	        else if( !isRetry )
			{
				var caching = info.mLastRecvdSequence != info.mLastProcessedSequence;

				// How far off is the new sequence from the last recvd sequence
				var seqOffset = sequence - info.mLastRecvdSequence;
				if( seqOffset < 0 )
					seqOffset += retryCacheSize;

				// If we are not caching data waiting for a missing packet AND this is the next expected packet, process it immediately
				if( !caching && seqOffset == 1 )
				{
					// Every n recvd packets will result in an ack packet back to the sensor
					if( ++info.mRecvPktCountForAck == (retryCacheSize >> 2) )
						timeToAck = true;
	            
					data.forEach( function( d, i ) { if( i > 0) info.mData.push( d ); } );
//					info.mData = info.mData.concat( data.slice( 1 ) );
					info.mLastProcessedSequence = sequence;
				}

				// If we are already caching data waiting for a missing packet OR we detected a missed packet,
				// tell the device to resend the missing packet(s) and cache this packet until the missing packets are received
				else if( caching || (seqOffset > 1 && seqOffset < (retryCacheSize - 3)) )
				{
					if( this.mLogSampleData )
						qDebug( "Queue packet 0x" + sequence.toString( 16 ) + " until retries are complete" );
					info.mRetryCache[sequence & retryCacheSizeMask] = data;
					timeToAck = true;
				}
	        
				// Any packet that is not an expected missing packet AND has a sequence that preceeds the last
				// received sequence is a duplicate packet and should be ignored
				else
					qDebug( "Ignoring duplicate packet 0x" + sequence.toString( 16 ) );

				info.mLastRecvdSequence = sequence;
			}

	        else
	            qDebug( "Ignoring unexpected retry packet 0x" + sequence.toString( 16 ) );
	        
	    }
	    
	    if( this.mLogSampleData )
	    {
	        qDebug( "Post State: seq=0x" + sequence.toString( 16 ) + ", lastRecvd=0x" + info.mLastRecvdSequence.toString( 16 ) + ", lastProcessed=0x" + info.mLastProcessedSequence.toString( 16 ) );
	        var str = "";
	        for( var i = 0; i < retryCacheSize; i++ )
	            str += " " + info.mRetryCache[i].length;
	        qDebug( "Cache State = " + str );
	    }
	    
	    // If we need to notify the sensor that we have received some packets or that we need some packets re-sent
	    if( timeToAck )
	    {
	        var missingBits = 0;
	        
	        // If we have missing packets, tell the sensor what they are
	        if( info.mLastProcessedSequence != info.mLastRecvdSequence )
	        {
	            var begin = (info.mLastProcessedSequence + 1) & retryCacheSizeMask;
	            var end = info.mLastRecvdSequence & retryCacheSizeMask;
	            
	            // Walk through the cache of packets that have not yet been processed and not where there are
	            // missing packets based on gaps in their sequences so that we can tell the sensor to resend those packets.
	            for( var i = begin; i != end; i = (i + 1) & retryCacheSizeMask)
	                if( !info.mRetryCache[i].length )
	                    missingBits |= (1 << i);
	        }
	        
	        // Tell the device which packets we have received
	        var cmd = new Uint8Array( 5 );
			cmd.set( [missingBits & 0xff, (missingBits >> 8) & 0xff, (missingBits >> 16) & 0xff, (missingBits >> 24) & 0xff, info.mLastRecvdSequence] );
			await this.BleWrite( PascoCharID( sensor + 1, pbc.charAck ), cmd );
	        
	        info.mRecvPktCountForAck = 0;
	    }
	}

	//*************************************************************************
	// Assign a BLE device that will be used to talk to the Pasco wireless sensors
	//*************************************************************************
	this.SetBleOrUsbDevice = async function( device, numSensors )
	{
		// Get the interface ID from the device driver
		if( device.constructor.name == "USBDevice" )
		{
			this.mUsbDevice = device;
			this.mBleDevice = null;
			this.mNumSensors = numSensors;
	
			// Do not await this so that it will run without blocking anything
			this.BleUsbReadLoop( );
		}
		else if( device.constructor.name == "WebBLEDriver" )
		{
			this.mBleDevice = device;
			this.mUsbDevice = null;
			this.mNumSensors = numSensors;

			let success = await device.EnableNotify( PascoCharID( 0, 3 ) );
			for( let i = 0; i < this.mNumSensors; i++ )
			{
				success = success && await device.EnableNotify( PascoCharID( i + 1, 3 ) );
				success = success && await device.EnableNotify( PascoCharID( i + 1, 4 ) );
			}

			if( !success )
				qDebug( "SetBleDevice failed to enable notifications" );

			// Tell the BLE driver to send me all packets for this BLE device
			device.SetNotificationCallback( this.BleNotification.bind( this ) );
		}
		else
		{
			return false;
		}

		return true;
	}

	//*************************************************************************
	// Handles packets recvd from the BLE connection
	//*************************************************************************
	this.BleNotification = function( charID, data )
	{
//		qDebug( "Pasco BLE Request: req=" + obj.request );

		var channel = (charID >> 8) & 0xff;
		var characteristic = charID & 0xff;
		var cmd = data[0];

//			qDebug( "Pasco BLE Notification: cmd=0x" + cmd.toString( 16 ) + ", char=" + characteristic + ", chan=" + channel );
		
		if( this.mLogData )
		{
			qDebug( "Recv " + data.length + " bytes from charID:" + charID.toString(16) );
			qHexDump( data );
		}

		// If this is sample data
		if( characteristic == pbc.charData )
		{
			this.OnSampleData( channel - 1, data );
		}
		// If this is a response or async event
		else if( characteristic == pbc.charRsp )
		{
			// If this is a response to a generic command, complete the command
			if( cmd == pbc.GenericResult )
			{
				// Return the BLE packet to the caller
				clearTimeout( this.mPendingGenericRequest.mTimer );
				this.mPendingGenericRequest.mResolve( {mSuccess : true, mData : data} );
				this.mPendingGenericRequest = null;
			}
			else if( cmd == pbc.BurstDataDone )
			{
				// Allow the burst code to complete or retry for missed packets
				clearTimeout( this.mPendingGenericRequest.mTimer );
				this.mPendingGenericRequest.mResolve( true );
				this.mPendingGenericRequest = null;
			}
			// If we got a block of burst data, add it to the accumulating burst data block
			else if( cmd == pbc.WriteBurstData )
			{
				let index = data[1] & 0x0f;

				// Skip over generic command and index and store data portion of the packet
				this.mBurstPacketMask &= ~(1 << index);
				this.mBurstRecvData[index] = data.slice( 2 );
			}
			// Notification of a pluggable sensor or accessory
			else if( cmd == pbc.EvtSensorID )
			{
				if( this.mOnSensorID )
					for( let i = 1; i < data.length; i += 2 )
						this.mOnSensorID( (i - 1) / 2, data[i] | (data[i+1] << 8) );
			}
			// Notification of battery status
			else if( cmd == pbc.EvtBatteryStatus )
			{
				if( this.mOnBatteryStatus )
				{
					let voltage = data[1] | (data[2] << 8);
					let percent = data[3] | (data[4] << 8);
					let low = (data[5] & 2) != 0;
					let charging = (data[5] & 1) != 0;
					this.mOnBatteryStatus( voltage, percent, low, charging );
				}
			}
			// Synchronization packet
			else if( cmd == pbc.EvtSync )
			{
				if( this.mOnSync )
				{
					let timeStamp = data[1] | (data[2] << 8) | (data[3] << 16) | (data[4] << 24);
					this.mOnSync( timeStamp );
				}
			}
		}
	}

	//*************************************************************************
	// Writes packet to sensor via BLE or the pseudo-BLE USB channel
	//*************************************************************************
	this.BleWrite = async function( charID, data )
	{
		if( this.mLogData )
		{
			qDebug( "Send " + data.length + " bytes to charID:" + charID.toString(16) );
			qHexDump( data );
		}

		if( this.mBleDevice )
		{
			return await this.mBleDevice.Write( charID, data );
		}
		else if( this.mUsbDevice )
		{
			// Prepend the BLE header info to the USB packet so that the sensor can translate the USB request to a BLE request
			let newData = new Uint8Array( data.length + 2 );
			newData.set( data, 2 );
			newData[0] = (charID >> 8) & 0xff;
			newData[1] = charID & 0xff;
			return await this.mUsbDevice.transferOut( 3, newData );
		}
	}

	//*************************************************************************
	// Keeps an async read posted against USB ep2 to receive packets from the sensor
	//*************************************************************************
	this.BleUsbReadLoop = async function( )
	{
		while( true )
		{
			let rsp = await this.mUsbDevice.transferIn( 2, 64 );
			let data = new Uint8Array( rsp.data.buffer );
			if( rsp.status == "ok" && data.length > 2 )
			{
				let newData = new Uint8Array( data.length - 2 );
				for( let i = 0; i < data.length - 2; i++ )
					newData[i] = data[i + 2];
				this.BleNotification( PascoCharID( data[0], data[1] ), newData );
			}
		}
	}
}

function PascoCharID( serviceIndex, charIndex )
{
	return 0x4a000000 + (serviceIndex << 8) + charIndex;
}