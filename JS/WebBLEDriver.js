
'use strict'

function WebBLEDriver( bleDevice )
{
	this.mDevice = bleDevice;
	this.mRSSI = 0;
	this.mName = "";
	this.mCharacteristicMap = new Map( );
	this.mNotifyCallback = null;
	this.mLogData = false;

	//**********************************************************************************
	//
	//**********************************************************************************
	this.Connect = async function( )
	{
		try
		{
			await this.mDevice.gatt.connect( );
			this.mCharacteristicMap = new Map( );

			qDebug( "Device " + this.mDevice.name + " Connected, discovering services..." );

			this.mDevice.addEventListener( 'gattserverdisconnected', this.OnDisconnected.bind( this ) );

		    let services = await this.mDevice.gatt.getPrimaryServices( );
			qDebug( "Services detected:" );
		    for( let service of services )
		    {
//		    	qDebug( "Found service " + service.uuid );
		    	let characteristics = await service.getCharacteristics( );
		    	for( let characteristic of characteristics )
		    	{
//		    		qDebug( "Found characteristic " + characteristic.uuid );
		    		this.mCharacteristicMap.set( ConvertBLEUUIDToInt( characteristic.uuid ), characteristic );
					characteristic.addEventListener( 'characteristicvaluechanged', this.OnCharacteristicValueChanged.bind( this ) );
		    	}
		    }

		    return true;
		}
		catch( err )
		{
			qDebug( "BLE Connect Error: " + err );
			return false;
		}
	}

	//**********************************************************************************
	//
	//**********************************************************************************
	this.EnableNotify = async function( charID )
	{
		let characteristic = this.mCharacteristicMap.get( charID );
		if( !characteristic )
			return false;

		try
		{
			if( this.mLogData )
				qDebug( "Enable Notify: chr=" + charID.toString( 16 ) );
			await characteristic.startNotifications( );
			return true;
		}
		catch( err )
		{
			qDebug( "BLE EnableNotify failed: " + err );
			return false;
		}
	}

	//**********************************************************************************
	//
	//**********************************************************************************
	this.Write = async function( charID, data )
	{
		let characteristic = this.mCharacteristicMap.get( charID );
		if( !characteristic )
			return;

		if( this.mLogData )
		{
			qDebug( "Write BLE Packet: chr=" + charID.toString( 16 ) );
			qHexDump( data );
		}

		try
		{
			await characteristic.writeValue( data );
			return true;
		}
		catch( err )
		{
			qDebug( "BLE Write failed: " + err );
			return false;
		}
	}

	//**********************************************************************************
	//
	//**********************************************************************************
	this.Disconnect = async function( )
	{
		this.mDevice.gatt.disconnect( );
	}

	//**********************************************************************************
	//
	//**********************************************************************************
	this.OnDisconnected = function( event )
	{
		qDebug( "OnDisconnected: " + event.target.name );
	}

	//**********************************************************************************
	//
	//**********************************************************************************
	this.SetNotificationCallback = function( cb )
	{
		this.mNotifyCallback = cb;
	}
	
	//**********************************************************************************
	//
	//**********************************************************************************
	this.OnCharacteristicValueChanged = function( event )
	{
		let characteristic = event.target;
		if( !characteristic )
			return;

		for( let [key, value] of this.mCharacteristicMap )
		{
			if( value === characteristic )
			{
				let data = new Uint8Array( characteristic.value.buffer );
				if( this.mLogData )
				{
					qDebug( "Recv BLE Packet: chr=" + key.toString( 16 ) );
					qHexDump( data );
				}

				this.mNotifyCallback( key, data )
				return;
			}
		}
	}
}

//**********************************************************************************
//
//**********************************************************************************
function ConvertBLEUUIDToInt( uuid )
{
	// If this is a Pasco service or characteristic uuid
	if( uuid.substr( 0, 4 ).toLowerCase( ) == "4a5c" )
	{
		let serviceIndex = uuid.charCodeAt( 7 ) - "0".charCodeAt( 0 );
		let charIndex = uuid.charCodeAt( 12 ) - "0".charCodeAt( 0 );

		return 0x4a000000 + (serviceIndex << 8) + charIndex;
	}
	// If this is a 128-bit representation of a 16-bit uuid (e.g. '00002a37-0000-1000-8000-00805f9b34fb')
	else if( uuid.substr( -12 ).toLowerCase( ) == "00805f9b34fb" )
		return parseInt( uuid.substr( 4, 4 ), 16 );
	// If this is a standard 16-bit uuid
	else if( uuid.length == 4 )
		return parseInt( uuid, 16 );
	else //todo: support non-pasco 128-bit uuids
		return 0;
}
