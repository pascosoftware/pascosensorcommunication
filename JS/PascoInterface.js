
'use strict'

//*******************************************************************
//
//*******************************************************************
function PascoInterface( ds )
{
    //**************************************************************
    this.Init = function( ds )
    {
        this.mDS = ds;
        this.mSensors = [];

        // Create sensor info for each virtual sensor
        for( let i = 0; i < ds.Channels.length; i++ )
        {
            // If this interface channel has a captured sensor, configure it
            if( ds.Channels[i].SensorID )
            {
                let sensorDS = gDatasheets.FindSensor( ds.Channels[i].SensorID );
                this.mSensors.push( new PascoSensor( sensorDS ) );
            }
            else // This channel has a pluggable sensor, so configure it later
                this.mSensors.push( null );
        }
    }

    //**************************************************************
    this.AddSensor = function( channelIndex, sensorID )
    {
        if( channelIndex < this.mSensors.length )
        {
			if( sensorID )
				this.mSensors[channelIndex] = new PascoSensor( gDatasheets.FindSensor( sensorID ) );
			else
				this.mSensors[channelIndex] = null;
        }
    }

    //**************************************************************
    this.StartSampling = function( )
    {
        for( let sensor of this.mSensors )
            if( sensor )
                sensor.StartSampling( );
    }

    this.Init( ds );
}
