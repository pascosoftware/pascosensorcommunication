
'use strict'

function GraphAxis( isXAxis )
{
	//***************************************************************
	this.Init = function( isXAxis )
	{
		this.mIsXAxis = isXAxis;

		this.mX = 0;
		this.mY = 0;
		this.mW = 0;
		this.mH = 0;
		
		this.mDataStart = 0;
		this.mDataRange = 10;
		this.mPixelStart = 0;
		this.mPixelRange = 10;
		this.mScale = 1;		// Number of pixels per data
		this.mOffset = 0;		// Pixel offset from the data origin

		this.mMajorStart = 0;
		this.mMajorStep = 0;
		this.mMinorStep = 0;
		this.mMinorsPerMajor = 5;
		this.mPrecision = 0;

		this.mCtx = null;
	}

	//***************************************************************
	this.SetRect = function( x, y, w, h )
	{
		this.mX = x;
		this.mY = y;
		this.mW = w;
		this.mH = h;
	}

	//***************************************************************
	this.SetDataRange = function( first, range )
	{
		this.mDataStart = first;
		this.mDataRange = range;
		this.mScale = this.mPixelRange / this.mDataRange;
		this.mOffset = this.mPixelStart - this.mScale * this.mDataStart;

		this.GetStepInfo( );
	}
	
	//***************************************************************
	this.SetPixelRange = function( first, range )
	{
		this.mPixelStart = first;
		this.mPixelRange = range;
		this.mScale = this.mPixelRange / this.mDataRange;
		this.mOffset = this.mPixelStart - this.mScale * this.mDataStart;

		this.GetStepInfo( );
	}
	
	//***************************************************************
	this.GetStepInfo = function( )
	{
		var minMajorSpacing = 50; // Smallest allowed major grid spacing in pixels
		var minDataSpacing = minMajorSpacing / Math.abs( this.mScale );
		var l = Math.log10( minDataSpacing ) + 50;
		var li = Math.trunc( l );
		var lf = Math.abs( l - li );

		// Calculate the 1/2/5 log that is >= the current major grid log
		if( lf <= 0.301029995663981 )
		{
			lf = 0.301029995663981;
			this.mMinorsPerMajor = 4;
		}
		else if( lf <= 0.698970004336019 )
		{
			lf = 0.698970004336019;
			this.mMinorsPerMajor = 5;
		}
		else
		{
			lf = 0;
			li++;
			this.mMinorsPerMajor = 5;
		}

		// Calculate the major grid data width
		var step = Math.pow( 10, (li + lf) - 50 );

//		qDebug( "range=" + this.mDataRange + ", maj=" + minMajorSpacing + ", log=" + l + ", int=" + li + ", frac=" + lf + ", step=" + step )

		if( li - 50 < 0 )
			this.mPrecision = 50 - li;
		else
			this.mPrecision = 0;
		this.mMajorStep = step;
		this.mMajorStart = this.mDataStart - this.mDataStart % this.mMajorStep;
		this.mMinorStep = step / this.mMinorsPerMajor;
	}
	
	//***************************************************************
	this.PixelToData = function( pixel )
	{
		return (pixel - this.mOffset) / this.mScale;
	}
	
	//***************************************************************
	this.DataToPixel = function( data )
	{
		return this.mScale * data + this.mOffset;
	}

	//***************************************************************
	this.Draw = function( )
	{
		let ctx = this.mCtx;

		ctx.fillStyle="#e0e0e0";
		ctx.fillRect( this.mX, this.mY, this.mW, this.mH );
		ctx.strokeStyle="#00c0c0";
		ctx.strokeRect( this.mX, this.mY, this.mW, this.mH );
		
		var fontHeight = 12;
		ctx.font = fontHeight + "px Arial";
		ctx.fillStyle = "#000000";

		ctx.save( );
		ctx.rect( this.mX, this.mY, this.mW, this.mH );
		ctx.clip( );

		// Draw the axis label text in the center of the axis
		if( this.mIsXAxis )
		{
			ctx.beginPath( );
			ctx.strokeStyle="#000000";

			// Draw a line across the top of the axis
			ctx.moveTo( this.mX + 4, this.mY + 4 );
			ctx.lineTo( this.mX + this.mW - 4, this.mY + 4 );

			// Draw the major axis
			var safety = 0;
			for( var gx = this.mMajorStart - this.mMajorStep; gx < this.mDataStart + this.mDataRange && safety++ < 40; gx += this.mMajorStep )
			{
				// Draw major ticks
				var majX = this.DataToPixel( gx );
				ctx.moveTo( majX, this.mY + 4 );
				ctx.lineTo( majX, this.mY + 11 );

				// Draw minor ticks
				var gxm = gx + this.mMinorStep;
				for( var i = 1; i < this.mMinorsPerMajor; i++, gxm += this.mMinorStep )
				{
					var minX = this.DataToPixel( gxm );
					ctx.moveTo( minX, this.mY + 4 );
					ctx.lineTo( minX, this.mY + 6 );
				}

				// Draw numbers
				var numText = gx.toFixed( this.mPrecision );
				var textWidth = ctx.measureText( numText ).width;
				var x = majX - textWidth / 2;
				var y = this.mY + this.mH - fontHeight - 5;
				ctx.fillText( numText, x, y );		
			}

			ctx.stroke( );
		}
		else
		{
			ctx.beginPath( );
			ctx.strokeStyle="#000000";

			// Draw a line across the top of the axis
			ctx.moveTo( this.mX + this.mW - 4, this.mY + 4 );
			ctx.lineTo( this.mX + this.mW - 4, this.mY + this.mH - 4 );

			// Draw the major axis
			var safety = 0;
			for( var gy = this.mMajorStart - this.mMajorStep; gy < this.mDataStart + this.mDataRange && safety++ < 40; gy += this.mMajorStep )
			{
				// Draw major ticks
				var majY = this.DataToPixel( gy );
				ctx.moveTo( this.mX + this.mW - 4, majY );
				ctx.lineTo( this.mX + this.mW - 11, majY );

				// Draw minor ticks
				var gym = gy + this.mMinorStep;
				for( var i = 1; i < this.mMinorsPerMajor; i++, gym += this.mMinorStep )
				{
					var minY = this.DataToPixel( gym );
					ctx.moveTo( this.mX + this.mW - 4, minY );
					ctx.lineTo( this.mX + this.mW - 6, minY );
				}

				// Draw numbers
				var numText = gy.toFixed( this.mPrecision );
				var textWidth = ctx.measureText( numText ).width;
				var x = this.mX + this.mW - 14 - textWidth;
				var y = majY + 4;
				ctx.fillText( numText, x, y );		
			}

			ctx.stroke( );
		}

		ctx.restore( );
	}

	this.Init( isXAxis );
}

