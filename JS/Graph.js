
function Graph( canvasID )
{
    this.mCanvas = document.getElementById( canvasID );
    this.mCtx = this.mCanvas.getContext( "2d" );

    this.mX = 0;
    this.mY = 0;
    this.mW = this.mCanvas.offsetWidth;
    this.mH = this.mCanvas.offsetHeight;

    this.mAxisSize = 40;

    this.mXAxis = new GraphAxis( true );
    this.mXAxis.SetPixelRange( 2 * this.mAxisSize, this.mW - 2 * this.mAxisSize );
    this.mXAxis.SetDataRange( -1, 12 );
    this.mXAxis.SetRect( 2 * this.mAxisSize, this.mH - this.mAxisSize, this.mW - 2 * this.mAxisSize, this.mAxisSize );
    this.mXAxis.mCtx = this.mCtx;

    this.mYAxis = new GraphAxis( false );
    this.mYAxis.SetPixelRange( this.mH - this.mAxisSize, this.mAxisSize - this.mH );
    this.mYAxis.SetDataRange( -11, 22 );
    this.mYAxis.SetRect( 0, 0, 2 * this.mAxisSize, this.mH - this.mAxisSize );
    this.mYAxis.mCtx = this.mCtx;

    // This describes the data to be drawn
    this.mXData = [];
    this.mYData = [];
    this.mLastDataIndex = 0;

    //**********************************************************************
    //
    //**********************************************************************
    this.AutoScale = function( force )
    {
        let rescaled = false;

        for( xAxis = 0; xAxis < 2; xAxis++ )
        {
            let min = 100000000;
            let max = -100000000;
            let data = xAxis ? this.mXData : this.mYData;
            let curStart = xAxis ? this.mXAxis.mDataStart : this.mYAxis.mDataStart;
            let curRange = xAxis ? this.mXAxis.mDataRange : this.mYAxis.mDataRange;

            // Make sure there is enough data
            if( data.length < 2 )
                return;

            // Find the min/max for all data
            for( d of data )
            {
                if( d < min )
                    min = d;
                if( d > max )
                    max = d;
            }

            // If the data already fits within the graph, don't rescale unless forced
            if( min < curStart || max > curStart + curRange || force ) 
            {
                rescaled = true;
                if( xAxis )
                    this.mXAxis.SetDataRange( min, max - min );
                else
                    this.mYAxis.SetDataRange( min, max - min );
            }
        }

        if( rescaled )
            this.DrawGrid( );
    }

    //**********************************************************************
    //
    //**********************************************************************
    this.DrawGrid = function( )
    {
        let ctx = this.mCtx;

        ctx.fillStyle="#f0f0f0";
		ctx.fillRect( this.mX, this.mY, this.mW, this.mH );
        ctx.strokeStyle="#ff0000";
		ctx.strokeRect( this.mX, this.mY, this.mW, this.mH );
		ctx.strokeStyle="#c0c0c0";
	
		ctx.save( );
		ctx.rect( this.mX, this.mY, this.mW, this.mH );
		ctx.clip( );

		// Paint the graph background
		ctx.beginPath( );

		let safety = 0;
		for( let gx = this.mXAxis.mMajorStart; gx < this.mXAxis.mDataStart + this.mXAxis.mDataRange && safety++ < 40; gx += this.mXAxis.mMajorStep )
		{
			var x = this.mXAxis.DataToPixel( gx );
			ctx.moveTo( x, this.mYAxis.mPixelStart );
			ctx.lineTo( x, this.mYAxis.mPixelStart + this.mYAxis.mPixelRange );
		}

		safety = 0;
		for( let gy = this.mYAxis.mMajorStart; gy < this.mYAxis.mDataStart + this.mYAxis.mDataRange && safety++ < 40; gy += this.mYAxis.mMajorStep )
		{
			let y = this.mYAxis.DataToPixel( gy );
			ctx.moveTo( this.mXAxis.mPixelStart, y );
			ctx.lineTo( this.mXAxis.mPixelStart + this.mXAxis.mPixelRange, y );
		}

		ctx.stroke( );
		ctx.restore( );
    
        this.mXAxis.Draw( );
        this.mYAxis.Draw( );
    }

    //**********************************************************************
    //
    //**********************************************************************
    this.DrawData = function( redraw )
    {
        let ctx = this.mCtx;

        if( redraw )
            this.mLastDataIndex = 0;

	    // Get the plot area dimensions
        let px = this.mAxisSize;
        let py = 0;
        let pw = this.mW - this.mAxisSize;
        let ph = this.mH - this.mAxisSize;

        ctx.save( );
        ctx.beginPath( );
		ctx.rect( px, py, pw, ph );
		ctx.clip( );
            
        ctx.strokeStyle = "#0000ff";
        ctx.fillStyle = "#0000ff";

		// Draw any new data points
		for( let i = this.mLastDataIndex; i <= this.mYData.length; i++ )
		{
            // Get the pixel position of the data point
            let y = this.mYAxis.DataToPixel( this.mYData[i] );
            let x = this.mXAxis.DataToPixel( this.mXData[i] );

            if( i == this.mLastDataIndex )
                ctx.moveTo( x, y );
            else
            {
                // Draw the data point and the connecting line
                ctx.fillRect( x-1, y-1, 3, 3 );
                ctx.lineTo( x, y );
            }
        }

        ctx.stroke( );
		ctx.restore( );
    }
}

