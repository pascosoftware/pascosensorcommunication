
'use strict'

let STD_HEARTRATE_SERVICE_ID    = 0x0000180d;
let STD_HEARTRATE_CHAR_ID		= 0x00002a37;

//**********************************************************
//
//**********************************************************
function StandardBLEInterface( ifaceID )
{
	PascoInterface.call( this, gDatasheets.FindInterface( ifaceID ) );

	this.mBleDriver = null;
	this.mSampleTimer = 0;
	this.mHeartRate = 0;
	this.mExpended = 0;
	this.mRrInterval = 0;
	
	//*******************************************************
	this.Connect = async function( device )
	{
		this.mBleDriver = device;

		let success = await device.EnableNotify( STD_HEARTRATE_CHAR_ID );
		if( !success )
			qDebug( "StandardBleDevice failed to enable notifications" );

		// Tell the BLE driver to send me all packets for this BLE device
		device.SetNotificationCallback( this.BleNotification.bind( this ) );
		
		return true;
	}

	//*******************************************************
	this.PreStartSampling = async function(  )
	{
		return true;
	}

	//*******************************************************
	this.StartSampling = async function(  )
	{
		// Must have a sensor configured
		if( !this.mSensors.length || !this.mSensors[0] )
			return false;
		
		this.mSampleQueue = [];
		if( !this.mSampleTimer )
			this.mSampleTimer = setInterval( this.PollForData, this.mSensors[0].mSamplePeriod * 1000 );
	}

	//*******************************************************
	this.StopSampling = async function(  )
	{
		if( this.mSampleTimer )
		{
			clearInterval( this.mSampleTimer );
			this.mSampleTimer = 0;
		}
	}

	//*******************************************************
	this.ZeroSensor = async function(  )
	{
	}

	//*******************************************************
	this.RequestBatteryInfo = async function(  )
	{
	}

	//**********************************************************
	this.PollForData = function( )
	{
		// Must have a sensor configured
		if( !this.mSensors.length || !this.mSensors[0] )
			return;

		let data = [this.mHeartRate & 0xff, this.mHeartRate >> 8, this.mRrInterval & 0xff, this.mRrInterval >> 8];
		this.mSensors[0].ProcessData( data );
//		qDebug( "Sample: Rate=" + this.mHeartRate + " bpm, Interval=" + this.mRrInterval +" ms" );
	}.bind( this );

	//*******************************************************
	//
	//*******************************************************
	this.ReadOneSample = async function( sensor )
	{
		this.PollForData( );
	}

	//*******************************************************
	//
	//*******************************************************
	this.BleNotification = function( charID, data )
	{
		if( charID == STD_HEARTRATE_CHAR_ID )
		{
//			qDebug( "Heart rate notification: " + data );

			let rateData16Bit = data[0] & 0x01;
			let contactDetected = (data[0] & 0x06) == 0x06;
			let hasEnergyExpended = (data[0] & 0x08) != 0;
			let hasRRInterval = (data[0] & 0x10) != 0;
			let heartRate = data[1];

			if( rateData16Bit && data.length >= 2 )
				heartRate |= (data[2] << 8);
			if( heartRate )
				this.mHeartRate = heartRate;
			data = data.slice( rateData16Bit ? 3 : 2 );

			if( hasEnergyExpended && data.length >= 2 )
			{
				data = data.slice( 2 );
				this.mExpended = data[0] | (data[1] << 8);
			}

			if( hasRRInterval && data.length >= 2 )
				this.mRrInterval = data[0] | (data[1] << 8);

//			qDebug( "Heart Rate = " + this.mHeartRate + ", expended = " + this.mExpended + ", RR Interval = " + this.mRrInterval );
		}
	}
}