
var gConsole = document.getElementById( "console" );

//******************************************************
async function sleep( ms )
{
	return new Promise(resolve => setTimeout(resolve, ms));
}

//******************************************************
function qDebug( msg )
{
    gConsole.value += msg + "\r\n";
	gConsole.scrollTop = document.getElementById("console").scrollHeight;
}

//*****************************************************************
function ToHex8( num )
{
	num &= 0xff;
	var str = num.toString( 16 );
	if( str.length == 2 )
		return str;
	else
		return "0" + str;

}

//*****************************************************************
function ToHex16( num )
{
	num &= 0xffff;
	var str = num.toString( 16 );
	if( str.length == 4 )
		return str;
	else if( str.length == 3 )
		return "0" + str;
	else if( str.length == 2 )
		return "00" + str;
	else if( str.length == 1 )
		return "000" + str;

}

//***********************************************************************
//
//***********************************************************************
function qHexDump( data )
{
    if( !data.length )
        return;
    
    // for each 16 byte row
    for( var row = 0; row <= ((data.length - 1) >> 4); row++ )
    {
    	var str = ToHex16( row * 0x10 ) + ": ";

        // fill in the hex view of the data
        var col;
        for( col = 0; col < 16; col++ )
        {
			if( row * 16 + col < data.length )
				str += ToHex8( data[row * 16 + col]) + " ";
			else
				str += "   ";
        }

        str += "   ";
        
        // fill in the char view of the data
        for( col = 0; col < 16; col++ )
        {
            if( row * 16 + col < data.length )
            {
                var ch = data[row * 16 + col];
                if( ch < 0x20 || ch >= 0x7f )
                    str += ".";
                else
                    str += String.fromCharCode( ch );
            }
            else
                str += " ";
        }

        qDebug( str );
    }
}
