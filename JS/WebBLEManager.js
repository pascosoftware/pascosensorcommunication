
'use strict'

// Shows all named Pasco devices
var gShowPascoBleDevicesOnly = true;
var bleFilterPasco =
{
	optionalServices:   ['4a5c0000-0000-0000-0000-5c1e741f1c00',
						'4a5c0001-0000-0000-0000-5c1e741f1c00',
						'4a5c0002-0000-0000-0000-5c1e741f1c00',
						'4a5c0003-0000-0000-0000-5c1e741f1c00',
						'4a5c0004-0000-0000-0000-5c1e741f1c00',
						'heart_rate'],
	keepRepeatedDevices : false
};

if( gShowPascoBleDevicesOnly )
{
	// Create filter from advertising names listed in the interface datasheet
	bleFilterPasco.filters = [];
	for( let iface of gDatasheets.mInterfaces )
		bleFilterPasco.filters.push( {namePrefix : iface.AdvertName} );
}
else // Show all BLE devices, even if they are not Pasco devices.
{
	bleFilterPasco.filters =
		[{namePrefix: 'A'},{namePrefix: 'B'},{namePrefix: 'C'},{namePrefix: 'D'},{namePrefix: 'E'},{namePrefix: 'F'},
		{namePrefix: 'G'},{namePrefix: 'H'},{namePrefix: 'I'},{namePrefix: 'J'},{namePrefix: 'K'},
		{namePrefix: 'L'},{namePrefix: 'M'},{namePrefix: 'N'},{namePrefix: 'O'},{namePrefix: 'P'},
		{namePrefix: 'Q'},{namePrefix: 'R'},{namePrefix: 'S'},{namePrefix: 'T'},{namePrefix: 'U'},
		{namePrefix: 'V'},{namePrefix: 'W'},{namePrefix: 'X'},{namePrefix: 'Y'},{namePrefix: 'Z'},
		{namePrefix: 'a'},{namePrefix: 'b'},{namePrefix: 'c'},{namePrefix: 'd'},{namePrefix: 'e'},{namePrefix: 'f'},
		{namePrefix: 'g'},{namePrefix: 'h'},{namePrefix: 'i'},{namePrefix: 'j'},{namePrefix: 'k'},
		{namePrefix: 'l'},{namePrefix: 'm'},{namePrefix: 'n'},{namePrefix: 'o'},{namePrefix: 'p'},
		{namePrefix: 'q'},{namePrefix: 'r'},{namePrefix: 's'},{namePrefix: 't'},{namePrefix: 'u'},
		{namePrefix: 'v'},{namePrefix: 'w'},{namePrefix: 'x'},{namePrefix: 'y'},{namePrefix: 'z'},{namePrefix: '/'}];
}

function WebBLEManager( )
{
	this.mNextDeviceID = 1;
	this.mScanCallback = null;
	this.mDevices = [];

	this.mLogData = true;

	//**********************************************************************************
	//
	//**********************************************************************************
	this.StartScan = async function( )
	{
		// See if we support LE scanning
		this.mScanner = null;
		try
		{
//			this.mScanner = await navigator.bluetooth.requestLEScan( bleFilterAll );
//			navigator.bluetooth.addEventListener( 'advertisementreceived', this.OnAdvert.bind( this ) );
//		    qDebug('BLE Scan started...');
		}
		catch( err )
		{
			qDebug( "BLE Scan Failed:" + err );
		}

		// If LE scanning is not supported, request the device using the WebBluetooth API
		if( !this.mScanner )
		{
			try
			{
				qDebug( "Requesting BLE device..." );
				let device = await navigator.bluetooth.requestDevice( bleFilterPasco );
				qDebug( "Found device " + device.name + ": id=" + device.id );

				// Act as if the selected device was just another advertised devise
				let event = { device : device, name : device.name, rssi : 0 };
				this.OnAdvert( event );
			}
			catch( err )
			{
				qDebug( "BLE Request Failed: " + err );
			}
		}
	}

	//**********************************************************************************
	//
	//**********************************************************************************
	this.StopScan = function( )
	{
		if( this.mScanner )
			this.mScanner.stop( );
	}

	//**********************************************************************************
	// Called when a device is detected via an advertisemet packet or a WinBluetooth selection
	//**********************************************************************************
	this.OnAdvert = function( event )
	{
		// See if the device is already in my device list
		let device = null;
		for( let dev of this.mDevices )
		{
			if( dev.mDevice === event.device )
			{
				device = dev;
				break;
			}
		}

		// If the device is not already in my device list, add it and notify any listener
		if( !device )
		{
			device = new WebBLEDriver( event.device )
			this.mDevices.push( device );
		}

		device.mName = event.device.name;
		device.mRSSI = event.device.rssi;
		ExtractPascoInfoFromBLEName( device );

		// Notify any listeners of a new device being available
		if( this.mScanCallback )
			this.mScanCallback( device );
	}
}

//*******************************************************
// Decodes 6-bit encoding from Pasco advert packet
//*******************************************************
function Decode64( ch )
{
	// 0-9 		-> '0' - '9'
	// 10-25	-> 'K' - 'Z' -- Note: this funkiness is required due to an error in this code that was caught
	// 26-35	-> 'A' - 'J' -- after the wireless sensors went into production, so part of the bug stays
	// 36-61	-> 'a' - 'z' -- since the bug also exists in the matching sensor firmware code
	// 62		-> '*'
	// 63		-> '#'
	if( ch >= 0x30 && ch <= 0x39 ) // 0 to 9
        return ch - 0x30;
    else if( ch >= 0x4b && ch <= 0x5a ) // K to Z
        return ch - 0x41;
    else if( ch >= 0x41 && ch <= 0x4a ) // A to J
        return ch - 0x41 + 26;
    else if( ch >= 0x61 && ch <= 0x7a ) // a to z
        return ch - 0x61 + 36;
    else if( ch == 0x2a ) // *
        return 62;
    else if( ch == 0x23 ) // #
        return 63;
    else
        return -1;
}

//**********************************************************************************
//
//**********************************************************************************
function ExtractPascoInfoFromBLEName( device )
{
	device.mIfaceID = 0;
	device.mHasLoggedData = false;
	device.mBoxID = "";
	device.mRedCart = false;
	device.mDevName = "";

	let name = device.mName;

	// Special case for Polar heart rate sensor
    if( name.indexOf( "Polar " ) >= 0 )
    {
        device.mIfaceID = 24; //ZData::InterfaceType::BLEHeartRate;
        device.mDevName = name;
    }
    // Special case for OHAUS balance
    else if( name.indexOf( "OHBT_" ) >= 0 )
    {
        device.mIfaceID = 25; //ZData::InterfaceType::OhausBalance;
        device.mDevName = name;
    }
    // Otherwise, check for Pasco BLE wireless sensor
	else
	{
    	// First, make sure this is a valid Pasco device name (i.e. "nnnnnn xxx-xxx>ff")
	    var dashIndex = name.indexOf( '-' );
	    var gtIndex = name.indexOf( '>' );
	    
	    // Not a valid Pasco name
	    if( dashIndex == -1 || gtIndex == -1 || name.length < dashIndex + 4 || name.length < gtIndex + 2)
	        return;
	    
		// Flags:0 - true if currently logging
		// Flags:1 - true if there is valid logged data
		// Flags:2 - true for blue Smart Cart, false for red Smart Cart
		// Flags:3 - true if logged data has already been read
	    var flags = Decode64( name.charCodeAt( gtIndex + 2 ) );
	    device.mHasLoggedData = ((flags & 1) != 0) && ((flags & 8) == 0);
	    device.mRedCart = (flags & 4) == 0;

	    device.mBoxID = name.substr( dashIndex - 3, 7 );
	    device.mIfaceID = 0x400 + Decode64( name.charCodeAt( gtIndex + 1 ) );
	    device.mDevName = name.substr( 0, dashIndex - 4 );
	}
}

var gBLEManager = new WebBLEManager( );
