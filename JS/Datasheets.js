
'use strict'

var gDatasheets = new Datasheets( );

// Sensor IDs that need to be referred to in code
var sids =
{
	BloodPressure			: 2046,
}

// Interface IDs that need to be referred to in code
var iids =
{
	PolarHeartRate			: 24,
	AirLink3				: 1024,
	SmartGate				: 1048,
	GMCounter				: 1064,
	AirQuality				: 1067,
}

function Datasheets( )
{
	//**************************************************************************************
	this.mSensors =
	[
		{
      		ID:130, NameTag:"ForceSensor", DefaultRate:"20Hz", Model:"PS-2104", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawForce", NameTag:"RawForce", Equation:"uint16(0)", Internal:1 },
        		{ ID:"Force", NameTag:"Force", SymbolTag:"F", MeasType:"Force", Equation:"fcal(RawForce,0)", UnitType:"N", Precision:1 }
     		],
			FactoryCals: [[2048,0,4000,50]]
		},
		{
      		ID:2020, NameTag:"WirelessTemperatureSensor", DefaultRate:"1Hz", Model:"PS-3201", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawTemp", NameTag:"RawTemperature", Equation:"uint16(0)", Internal:1 },
        		{ ID:"Temperature", NameTag:"Temperature", SymbolTag:"T", MeasType:"Temperature", Equation:"RawTemp * 0.00268127 - 46.85", UnitType:"DegC", Precision:1 }
			]
		},
		{
      		ID:2021, NameTag:"WirelessPHSensor", DefaultRate:"1Hz", Model:"PS-3210", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawVoltage", NameTag:"RawVoltage", Equation:"uint16(0)", Internal:1 },
        		{ ID:"Voltage", NameTag:"Voltage", MeasType:"Voltage", Equation:"RawVoltage - 2048", UnitType:"mV", Precision:0 },
        		{ ID:"pH", NameTag:"pH", MeasType:"pH", Equation:"Voltage * -0.01671 + 6.958", UnitType:"", Precision:1 },
			]
		},
		{
			ID:2022, NameTag:"WirelessPressureSensor", DefaultRate:"20Hz", Model:"PS-3203", SampleSize:2,
			Measurements:
			[
			  { ID:"RawPress", NameTag:"RawPressure", Equation:"uint16(0)", Internal:1 },
			  { ID:"Pressure", NameTag:"Pressure", MeasType:"Pressure", Equation:"fcal(RawPress,0)", UnitType:"psi", Precision:1 }
		  	],
		  	FactoryCals: [[16016, 14.7, 38688, 34.7]]
		},
		{
      		ID:2023, NameTag:"ForceSensor", DefaultRate:"20Hz", Model:"PS-3202", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawForce", NameTag:"RawForce", Equation:"uint16(0)", Internal:1 },
        		{ ID:"Force", NameTag:"Force", MeasType:"Force", Equation:"fcal(RawForce,0)", UnitType:"N", Precision:1, UserCalIndex:0 }
     		],
			FactoryCals: [[32768,0,5000,50]],
			UserCals: [[0,0,50,50]]
			},
		{
			ID:2024, NameTag:"ForceAccel", DefaultRate:"20Hz", SampleSize:6,
			Measurements:
			[
				{ ID:"RawAccX", NameTag:"RawAccX", Equation:"int16(0)", Internal:1 },
				{ ID:"RawAccY", NameTag:"RawAccY", Equation:"int16(2)", Internal:1 },
				{ ID:"RawAccZ", NameTag:"RawAccZ", Equation:"int16(4)", Internal:1 },
        		{ ID:"AccelX", NameTag:"Accelerationx", Equation:"fcal(RawAccX,0)", UnitType:"ms2" },
				{ ID:"AccelY", NameTag:"Accelerationy", Equation:"fcal(RawAccY,1)", UnitType:"ms2" },
				{ ID:"AccelZ", NameTag:"Accelerationz", Equation:"fcal(RawAccZ,2)", UnitType:"ms2" },
				{ ID:"AccelR", NameTag:"AccelerationRes", Equation:"sqrt(AccelX * AccelX + AccelY * AccelY + AccelZ * AccelZ)", UnitType:"ms2" },
			],
			FactoryCals: [[0,0,1,0.004787],[0,0,1,0.004787],[0,0,1,0.004787]]
		},
		{
			ID:2025, NameTag:"SmartCartForce", DefaultRate:"20Hz", SampleSize:2,
			Measurements:
			[
			  { ID:"RawForce", NameTag:"RawForce", Equation:"uint16(0)", Internal:1 },
			  { ID:"Force", NameTag:"Force", Equation:"fcal(RawForce,0)", UnitType:"psi" }
		  	],
		  	FactoryCals: [[32768, 0, 1000, 100]]
		},
		{
			ID:2026, NameTag:"SmartCartAccel", DefaultRate:"20Hz", SampleSize:6,
			Measurements:
			[
				{ ID:"RawAccX", NameTag:"RawAccX", Equation:"int16(0)", Internal:1 },
				{ ID:"RawAccY", NameTag:"RawAccY", Equation:"int16(2)", Internal:1 },
				{ ID:"RawAccZ", NameTag:"RawAccZ", Equation:"int16(4)", Internal:1 },
        		{ ID:"AccelX", NameTag:"Accelerationx", Equation:"fcal(RawAccX,0)", UnitType:"ms2" },
				{ ID:"AccelY", NameTag:"Accelerationy", Equation:"fcal(RawAccY,1)", UnitType:"ms2" },
				{ ID:"AccelZ", NameTag:"Accelerationz", Equation:"fcal(RawAccZ,2)", UnitType:"ms2" },
			],
			FactoryCals: [[0,0,1,0.004787],[0,0,1,0.004787],[0,0,1,0.004787]]
		},
		{
			ID:2027, NameTag:"SmartCartPosition", DefaultRate:"20Hz", SampleSize:2,
			Measurements:
			[
        		{ ID:"RawPosition", NameTag:"RawPosition", UnitType:"", Equation:"int16(0)" },
        		{ ID:"Position", NameTag:"Position", UnitType:"m", Equation:"sum(RawPosition)*0.00012126" },
				{ ID:"Velocity", NameTag:"Velocity", UnitType:"mps", Equation:"deriv(Position, Position.x)" },
				{ ID:"Accel", NameTag:"Accel", UnitType:"mps2", Equation:"deriv(Velocity, Velocity.x)" }
			],
		},
		{
			ID:2028, NameTag:"ForceGyro", DefaultRate:"20Hz", SampleSize:6,
			Measurements:
			[
				{ ID:"RawGyroX", NameTag:"RawGyroX", Equation:"int16(0)", Internal:1 },
				{ ID:"RawGyroY", NameTag:"RawGyroY", Equation:"int16(2)", Internal:1 },
				{ ID:"RawGyroZ", NameTag:"RawGyroZ", Equation:"int16(4)", Internal:1 },
        		{ ID:"GyroX", NameTag:"AngVelocityX", Equation:"fcal(RawGyroX,0)", UnitType:"dps" },
				{ ID:"GyroY", NameTag:"AngVelocityY", Equation:"fcal(RawGyroY,1)", UnitType:"dps" },
				{ ID:"GyroZ", NameTag:"AngVelocityZ", Equation:"fcal(RawGyroZ,2)", UnitType:"dps" },
			],
			FactoryCals: [[0,0,1,0.07],[0,0,1,0.07],[0,0,1,0.07]]
		},
		{
			ID:2029, NameTag:"SmartCartGyro", DefaultRate:"20Hz", SampleSize:6,
			Measurements:
			[
				{ ID:"RawGyroX", NameTag:"RawGyroX", Equation:"int16(0)", Internal:1 },
				{ ID:"RawGyroY", NameTag:"RawGyroY", Equation:"int16(2)", Internal:1 },
				{ ID:"RawGyroZ", NameTag:"RawGyroZ", Equation:"int16(4)", Internal:1 },
        		{ ID:"GyroX", NameTag:"AngVelocityX", Equation:"fcal(RawGyroX,0)", UnitType:"dps" },
				{ ID:"GyroY", NameTag:"AngVelocityY", Equation:"fcal(RawGyroY,1)", UnitType:"dps" },
				{ ID:"GyroZ", NameTag:"AngVelocityZ", Equation:"fcal(RawGyroZ,2)", UnitType:"dps" },
			],
			FactoryCals: [[0,0,1,0.00875],[0,0,1,0.00875],[0,0,1,0.00875]]
		},
		{
			ID:2030, NameTag:"AmbientLightSensor1", DefaultRate:"1Hz", Model:"PS-3213", SampleSize:14,
			Measurements:
			[
				{ ID:"RawR", NameTag:"RawR", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"RawG", NameTag:"RawG", Equation:"uint16(2)", UnitType:"", Internal:1 },
				{ ID:"RawB", NameTag:"RawB", Equation:"uint16(4)", UnitType:"", Internal:1 },
				{ ID:"RawIR", NameTag:"RawIR", Equation:"uint16(6)", UnitType:"", Internal:1 },
				{ ID:"RawUVA", NameTag:"RawUVA", Equation:"uint16(8)", UnitType:"", Internal:1 },
				{ ID:"RawUVB", NameTag:"RawUVB", Equation:"uint16(10)", UnitType:"", Internal:1 },
				{ ID:"RawUVI", NameTag:"RawUVI", Equation:"uint16(12)", UnitType:"", Internal:1 },
				{ ID:"Illuminance", NameTag:"Illuminance", Equation:"fcal(RawG, 0) * 2.0", UnitType:"lux" },
				{ ID:"UVIndex", NameTag:"UVIndex", Equation:"RawUVI * 0.00076294", UnitType:"" },
				{ ID:"UVA", NameTag:"UVA", Equation:"RawUVA", UnitType:"" },
				{ ID:"UVB", NameTag:"UVB", Equation:"RawUVB", UnitType:"" },
				{ ID:"Irradiance", NameTag:"Irradiance", Equation:"Illuminance * 0.0104", UnitType:"wpm2" },
				{ ID:"PAR", NameTag:"PAR", Equation:"Illuminance * 0.0185", UnitType:"umolpm2ps" },
		  	],
		  	FactoryCals: [[0, 0, 1, 1]]
		},
		{
			ID:2031, NameTag:"VoltageSensor", DefaultRate:"20Hz", Model:"PS-3211", SampleSize:2,
			Measurements:
			[
				{ ID:"RawVoltage", NameTag:"RawVoltage", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"Voltage5V", NameTag:"Voltage5V", Equation:"fcal(RawVoltage, 0)", UnitType:"V", Internal:1 },
				{ ID:"Voltage15V", NameTag:"Voltage15V", Equation:"fcal(RawVoltage, 1)", UnitType:"V", Internal:1 },
				{ ID:"Voltage", NameTag:"Voltage", Equation:"rangesel(Voltage5V, Voltage15V)", UnitType:"V" }
		  	],
			FactoryCals: [[32768,0,65536,5],[32768,0,65536,5]],
			Range:
			{
				ID:"Range", DefaultIndex:1,
				Ranges:[{NameTag:"pm5V", HWIndex:1},{NameTag:"pm15V", HWIndex:2}]
			}
		},
		{
			ID:2032, NameTag:"CurrentSensor", DefaultRate:"20Hz", Model:"PS-3211", SampleSize:2,
			Measurements:
			[
				{ ID:"RawCurrent", NameTag:"RawCurrent", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"Current100mA", NameTag:"Current100mA", Equation:"fcal(RawCurrent, 0)", UnitType:"A", Internal:1 },
				{ ID:"Current1A", NameTag:"Current1A", Equation:"fcal(RawCurrent, 1)", UnitType:"A", Internal:1 },
				{ ID:"Current", NameTag:"Current", Equation:"rangesel(Current100mA, Current1A)", UnitType:"A" }
		  	],
			FactoryCals: [[32768,0,65536,0.1],[32768,0,65536,1]],
			Range:
			{
				ID:"Range", DefaultIndex:1,
				Ranges:[{NameTag:"pm100mA", HWIndex:3},{NameTag:"pm1A", HWIndex:4}]
			}
		},
		{
			ID:2033, NameTag:"ConductivitySensor", DefaultRate:"1Hz", Model:"PS-3204", SampleSize:4,
			Measurements:
			[
				{ ID:"RawConductivity", NameTag:"RawConductivity", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"RawTemperature", NameTag:"RawTemperature", Equation:"uint16(2)", UnitType:"", Internal:1 },
				{ ID:"Conductivity", NameTag:"Conductivity", Equation:"fcal(RawConductivity, 0)", UnitType:"uSpcm" },
				{ ID:"Temperature", NameTag:"Temperature", Equation:"RawTemperature * 0.00268127 - 46.85", UnitType:"DegC" },
				{ ID:"TDS", NameTag:"TotalDissolvedSolids", Equation:"0.65*(Conductivity/(1+0.02*(Temperature-25)))", UnitType:"mgpL" },
		  	],
			FactoryCals: [[0, 0, 1, 1]],
		},
		{
			ID:2034, NameTag:"SpotLightSensor", DefaultRate:"20Hz", SampleSize:8,
			Measurements:
			[
				{ ID:"RawR", NameTag:"RawR", Equation:"uint16(0)", Internal:1 },
				{ ID:"RawG", NameTag:"RawG", Equation:"uint16(2)", Internal:1 },
				{ ID:"RawB", NameTag:"RawB", Equation:"uint16(4)", Internal:1 },
				{ ID:"RawW", NameTag:"RawW", Equation:"uint16(6)", Internal:1 },
				{ ID:"White", NameTag:"White", Equation:"(RawR+(RawG*1.27)+(RawB*1.49))/3.76", UnitType:"" },
        		{ ID:"Red", NameTag:"Red", Equation:"(RawR/White)*26.6", UnitType:"percent" },
				{ ID:"Green", NameTag:"Green", Equation:"((RawG*1.27)/White)*26.6", UnitType:"percent" },
				{ ID:"Blue", NameTag:"Blue", Equation:"((RawB*1.49)/White)*26.6", UnitType:"percent" }
			],
		},
		{
			ID:2035, NameTag:"WirelessCO2Sensor", DefaultRate:"1Hz", Model:"PS-3208", SampleSize:2,
			Measurements:
			[
				{ ID:"Concentration", NameTag:"CO2Concentration", Equation:"uint16(0)*2", UnitType:"ppm" }
		  	],
		},
		{
			ID:2037, NameTag:"WeatherSensor", DefaultRate:"1Hz", MaxRate:"1Hz", SampleSize:8,
			Measurements:
			[
				{ ID:"RawTemperature", NameTag:"RawTemperature", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"RawHumidity", NameTag:"RawHumidity", Equation:"uint16(2)", UnitType:"", Internal:1 },
				{ ID:"RawBaroPressure", NameTag:"RawBaro", Equation:"uint16(4)", UnitType:"", Internal:1 },
				{ ID:"RawWindSpeed", NameTag:"RawWindSpeed", Equation:"uint16(6)", UnitType:"", Internal:1 },
				{ ID:"Temperature", NameTag:"Temperature", Equation:"RawTemperature* 0.0025177 - 40.0", UnitType:"DegC" },
				{ ID:"RelHumidity", NameTag:"RelHumidity", Equation:"RawHumidity * 0.0015258", UnitType:"percent" },
				{ ID:"AbsHumidity", NameTag:"AbsHumidity", Equation:"((13.24*RelHumidity)/(Temperature+273.15)) * Math.pow(2.71828, ((17.42*Temperature)/(Temperature+239.7)))", UnitType:"gpm3" },
				{ ID:"BaroPressure", NameTag:"BaroPressure", Equation:"RawBaroPressure * 0.002", UnitType:"kPa" },
				{ ID:"WindSpeed", NameTag:"WindSpeed", Equation:"RawWindSpeed * 0.0009144", UnitType:"mps" },
				{ ID:"DewPoint", NameTag:"DewPoint", Equation:"dewpoint(Temperature, RelHumidity)", UnitType:"DegC" },
				{ ID:"WindChill", NameTag:"WindChill", Equation:"windchill(Temperature, WindSpeed)", UnitType:"DegC" },
				{ ID:"Humidex", NameTag:"Humidex", Equation:"heatindex(Temperature, RelHumidity)", UnitType:"DegC" }
		  	],
		},
		{
			ID:2038, NameTag:"GPSSensor", DefaultRate:"1Hz", MaxRate:"1Hz", SampleSize:13,
			Measurements:
			[
				{ ID:"RawLatitude", NameTag:"RawLatitude", Equation:"uint32(0)", UnitType:"", Internal:1 },
				{ ID:"RawLongitude", NameTag:"RawLongitude", Equation:"uint32(4)", UnitType:"", Internal:1 },
				{ ID:"RawAltitude", NameTag:"RawAltitude", Equation:"uint16(8)", UnitType:"", Internal:1 },
				{ ID:"RawSpeed", NameTag:"RawSpeed", Equation:"uint16(10)", UnitType:"", Internal:1 },
				{ ID:"SatCount", NameTag:"SatCount", Equation:"uint8(12)", UnitType:""}
		  	],
		},
		{
			ID:2039, NameTag:"LightSensor", DefaultRate:"1Hz", MaxRate:"1Hz", SampleSize:4,
			Measurements:
			[
				{ ID:"RawUVI", NameTag:"RawUVI", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"RawG", NameTag:"RawGreen", Equation:"uint16(2)", UnitType:"", Internal:1 },
				{ ID:"Illuminance", NameTag:"Illuminance", Equation:"fcal(RawG, 0) * 2.0", UnitType:"lux" },
				{ ID:"UVIndex", NameTag:"UVIndex", Equation:"RawUVI * 0.01", UnitType:"" },
				{ ID:"Irradiance", NameTag:"Irradiance", Equation:"Illuminance * 0.0104", UnitType:"wpm2" },
				{ ID:"PAR", NameTag:"PAR", Equation:"Illuminance * 0.0185", UnitType:"umolpm2ps" },
		  	],
		  	FactoryCals: [[0, 0, 1, 1]]
		},
		{
			ID:2040, NameTag:"HandGripHeartRate", DefaultRate:"1Hz", MaxRate:"1Hz", SampleSize:4,
			Measurements:
			[
				{ ID:"HeartRate", NameTag:"HeartRate", Equation:"uint16(0)", UnitType:"bpm" },
				{ ID:"PPInterval", NameTag:"PPInterval", Equation:"uint16(2)", UnitType:"ms" },
		  	],
		},
		{
			ID:2041, NameTag:"WirelessColorimeter", DefaultRate:"1Hz", MaxRate:"1Hz", SampleSize:12,
			Measurements:
			[
				{ ID:"RawRed", NameTag:"RawRed650nm", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"RawOrange", NameTag:"RawOrange600nm", Equation:"uint16(2)", UnitType:"", Internal:1 },
				{ ID:"RawYellow", NameTag:"RawYellow570nm", Equation:"uint16(4)", UnitType:"", Internal:1 },
				{ ID:"RawGreen", NameTag:"RawGreen550nm", Equation:"uint16(6)", UnitType:"", Internal:1 },
				{ ID:"RawBlue", NameTag:"RawBlue500nm", Equation:"uint16(8)", UnitType:"", Internal:1 },
				{ ID:"RawViolet", NameTag:"RawViolet450nm", Equation:"uint16(10)", UnitType:"", Internal:1 },
				{ ID:"VioletTransmittance", NameTag:"Violet450nmTransmittance", Equation:"RawViolet", UnitType:"percent", UserCalIndex:5, Limit:[0, 100] },
				{ ID:"BlueTransmittance", NameTag:"Blue500nmTransmittance", Equation:"RawBlue", UnitType:"percent", UserCalIndex:4, Limit:[0, 100] },
				{ ID:"GreenTransmittance", NameTag:"Green550nmTransmittance", Equation:"RawGreen", UnitType:"percent", UserCalIndex:3, Limit:[0, 100] },
				{ ID:"YellowTransmittance", NameTag:"Yellow570nmTransmittance", Equation:"RawYellow", UnitType:"percent", UserCalIndex:2, Limit:[0, 100] },
				{ ID:"OrangeTransmittance", NameTag:"Orange600nmTransmittance", Equation:"RawOrange", UnitType:"percent", UserCalIndex:1, Limit:[0, 100] },
				{ ID:"RedTransmittance", NameTag:"Red650nmTransmittance", Equation:"RawViolet", UnitType:"percent", UserCalIndex:0, Limit:[0, 100] },
				{ ID:"VioletAbsorbance", NameTag:"Violet450nmAbsorbance", Equation:"2.0-log10(limit(VioletTransmittance, 0.1, 100))", UnitType:"" },
				{ ID:"BlueAbsorbance", NameTag:"Blue500nmAbsorbance", Equation:"2.0-log10(limit(BlueTransmittance, 0.1, 100))", UnitType:"" },
				{ ID:"GreenAbsorbance", NameTag:"Green550nmAbsorbance", Equation:"2.0-log10(limit(GreenTransmittance, 0.1, 100))", UnitType:"" },
				{ ID:"YellowAbsorbance", NameTag:"Yellow570nmAbsorbance", Equation:"2.0-log10(limit(YellowTransmittance, 0.1, 100))", UnitType:"" },
				{ ID:"OrangeAbsorbance", NameTag:"Orange600nmAbsorbance", Equation:"2.0-log10(limit(OrangeTransmittance, 0.1, 100))", UnitType:"" },
				{ ID:"RedAbsorbance", NameTag:"Red650nmAbsorbance", Equation:"2.0-log10(limit(RedTransmittance, 0.1, 100))", UnitType:"" },
				{ ID:"Ammonia", NameTag:"Ammonia", Equation:"table(OrangeAbsorbance,[0,-0.117,0.617,1.56,2,8.13])", UnitType:"mgpL" },
				{ ID:"Iron", NameTag:"Iron", Equation:"table(GreenAbsorbance,[0,0.0641,0.833,5.85,2,101.6])", UnitType:"mgpL" },
				{ ID:"Phosphate", NameTag:"Phosphate", Equation:"table(RedAbsorbance,[0,-0.017,0.647,5.33,2,42.4])", UnitType:"mgpL" },
				{ ID:"Nitrate", NameTag:"Nitrate", Equation:"table(VioletAbsorbance,[0,-0.196,1.93,7.04,3,30.5])", UnitType:"mgpL" },
		  	],
			UserCals:[[0,0,10000,100],[0,0,10000,100],[0,0,10000,100],[0,0,10000,100],[0,0,10000,100],[0,0,10000,100]],
		},
		{
			ID:2042, NameTag:"WirelessTurbidimeter", DefaultRate:"1Hz", MaxRate:"1Hz", SampleSize:2,
			Measurements:
			[
				{ ID:"RawTurbidity", NameTag:"RawTurbidity", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"Turbidity", NameTag:"Turbidity", Equation:"RawTurbidity", UnitType:"NTU", UserCalIndex:0 },
		  	],
			UserCals:[[0,0,10000,100]],
		},
		{
			ID:2043, NameTag:"Force", DefaultRate:"20Hz", MaxRate:"2000Hz", SampleSize:2,
			Measurements:
			[
				{ ID:"RawForce", NameTag:"RawForce", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"Force", NameTag:"Force", Equation:"fcal(RawForce, 0)", UnitType:"N" },
		  	],
			FactoryCals:[[32768,0,5000,50]],
		},
		{
			ID:2044, NameTag:"Acceleration", DefaultRate:"20Hz", MaxRate:"500Hz", SampleSize:6,
			Measurements:
			[
				{ ID:"RawAccX", NameTag:"RawAccX", Equation:"int16(0)", Internal:1 },
				{ ID:"RawAccY", NameTag:"RawAccY", Equation:"int16(2)", Internal:1 },
				{ ID:"RawAccZ", NameTag:"RawAccZ", Equation:"int16(4)", Internal:1 },
        		{ ID:"AccelX", NameTag:"Accelerationx", Equation:"fcal(RawAccX,0)", UnitType:"ms2" },
				{ ID:"AccelY", NameTag:"Accelerationy", Equation:"fcal(RawAccY,1)", UnitType:"ms2" },
				{ ID:"AccelZ", NameTag:"Accelerationz", Equation:"fcal(RawAccZ,2)", UnitType:"ms2" },
				{ ID:"AccelR", NameTag:"AccelerationRes", Equation:"sqrt(AccelX * AccelX + AccelY * AccelY + AccelZ * AccelZ)", UnitType:"ms2" },
			],
			FactoryCals: [[0,0,1,0.004787],[0,0,1,0.004787],[0,0,1,0.004787]]
		},
		{
			ID:2045, NameTag:"Compass", DefaultRate:"1Hz", MaxRate:"1Hz", SampleSize:2,
			Measurements:
			[
				{ ID:"RawHeading", NameTag:"RawHeading", Equation:"int16(0)", UnitType:"", Internal:1 },
				{ ID:"MagneticHeading", NameTag:"MagneticHeading", Equation:"RawHeading * 0.1", UnitType:"deg" },
				{ ID:"TrueHeading", NameTag:"TrueHeading", Equation:"MagneticHeading % 360", UnitType:"deg" },
				{ ID:"WindDirection", NameTag:"WindDirection", Equation:"(TrueHeading + 180) % 360", UnitType:"deg" },
		  	],
		},
		{
			ID:2046, NameTag:"BloodPressure", DefaultRate:"100Hz", SampleSize:2,
			Measurements:
			[
				{ ID:"RawAdc", NameTag:"RawAdc", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"CuffPressure", NameTag:"CuffPressure", Equation:"fcal(RawAdc, 0)", UnitType:"mmHg" },
				{ ID:"PulsePressure", NameTag:"PulsePressure", Equation:"bloodpressure(CuffPressure)", UnitType:"mmHg" },
				{ ID:"PulsePeaks", NameTag:"PulsePeaks", Equation:"bppeak()", UnitType:"mmHg" },
				{ ID:"MAP", NameTag:"MeanArterialPressure", Equation:"bpmap()", UnitType:"mmHg" },
				{ ID:"Systolic", NameTag:"SystolicPressure", Equation:"systolic()", UnitType:"mmHg" },
				{ ID:"Diastolic", NameTag:"DiastolicPressure", Equation:"diastolic()", UnitType:"mmHg" },
				{ ID:"PulseRate", NameTag:"PulseRate", Equation:"bppulse()", UnitType:"bpm" },
		  	],
		  	FactoryCals: [[3000, 0, 38688, 181]]
		},
		{
			ID:2047, NameTag:"WirelessRotaryMotion", DefaultRate:"20Hz", SampleSize:2,
			Measurements:
			[
        		{ ID:"RawAngle", NameTag:"RawAngle", UnitType:"", Equation:"int16(0)" },
        		{ ID:"Angle", NameTag:"Angle", UnitType:"rad", Equation:"sum(RawAngle)*0.00314159" },
				{ ID:"AngleVel", NameTag:"AngularVelocity", UnitType:"radps", Equation:"deriv(Angle, Angle.x)" },
				{ ID:"AngleAccel", NameTag:"AngularAcceleration", UnitType:"radps2", Equation:"deriv(AngleVel, AngleVel.x)" }
			],
		},
	  	{
			ID:2048, NameTag:"WirelessMotionSensor", DefaultRate:"10Hz", Model:"PS-3219", SampleSize:2,
			Measurements:
			[
		  		{ ID:"EchoTime", NameTag:"EchoTime", Equation:"uint16(0)", UnitType:"us" },
		  		{ ID:"Position", NameTag:"Position", SymbolTag:"P", MeasType:"Position", Equation:"EchoTime * 344.0 / 2000000.0", UnitType:"psi" },
				{ ID:"Velocity", NameTag:"Velocity", SymbolTag:"V", MeasType:"Velocity", Equation:"deriv(Position, Position.x)", UnitType:"mps" },
				{ ID:"Acceleration", NameTag:"Acceleration", SymbolTag:"a", MeasType:"Acceleration", Equation:"deriv(Velocity, Velocity.x)", UnitType:"ms2" }
			],
			Range:
			{
				ID:"Range", DefaultIndex:1,
				Ranges:[{NameTag:"ShortRange", HWIndex:1},{NameTag:"LongRange", HWIndex:2}]
			}
  		},
		{
      		ID:2049, NameTag:"WirelessOpticalDO", DefaultRate:"1Hz", SampleSize:6,
      		Measurements:
      		[
        		{ ID:"RawSat", NameTag:"RawSaturation", Equation:"uint16(0)", Internal:1 },
        		{ ID:"RawConc", NameTag:"RawConcentration", Equation:"uint16(2)", Internal:1 },
        		{ ID:"RawTemp", NameTag:"RawTemperature", Equation:"uint16(4)", Internal:1 },
        		{ ID:"DO2Saturation", NameTag:"DO2Saturation", Equation:"RawSat * 0.0045776", UnitType:"percent" },
        		{ ID:"DO2Concentration", NameTag:"DO2Concentration", Equation:"RawConc * 0.00030518", UnitType:"mgpL" },
        		{ ID:"O2Concentration", NameTag:"O2GasConcentration", Equation:"DO2Saturation * 0.209", UnitType:"percent" },
        		{ ID:"Temperature", NameTag:"Temperature", Equation:"RawTemp * 0.0045776 - 50.0", UnitType:"DegC" }
			]
		},
		{
			ID:2051, NameTag:"MagField", DefaultRate:"10Hz", MaxRate:"100Hz", SampleSize:6,
			Measurements:
			[
				{ ID:"RawMagX", NameTag:"RawMagX", Equation:"int16(0)", UnitType:"", Internal:1 },
				{ ID:"RawMagY", NameTag:"RawMagY", Equation:"int16(2)", UnitType:"", Internal:1 },
				{ ID:"RawMagZ", NameTag:"RawMagZ", Equation:"int16(4)", UnitType:"", Internal:1 },
				{ ID:"MagX50G", NameTag:"MagX50G", Equation:"RawMagX * 0.15", UnitType:"utesla", Internal:1 },
				{ ID:"MagY50G", NameTag:"MagY50G", Equation:"RawMagY * 0.15", UnitType:"utesla", Internal:1 },
				{ ID:"MagZ50G", NameTag:"MagZ50G", Equation:"RawMagZ * 0.15", UnitType:"utesla", Internal:1 },
				{ ID:"MagX1300G", NameTag:"MagX1300G", Equation:"RawMagX * 6.125", UnitType:"utesla", Internal:1 },
				{ ID:"MagY1300G", NameTag:"MagY1300G", Equation:"RawMagY * 6.125", UnitType:"utesla", Internal:1 },
				{ ID:"MagZ1300G", NameTag:"MagZ1300G", Equation:"RawMagZ * 6.125", UnitType:"utesla", Internal:1 },
				{ ID:"MagFieldX", NameTag:"MagFieldX", Equation:"rangesel(MagX50G,MagX1300G)", UnitType:"utesla", OffsetZero:1 },
				{ ID:"MagFieldY", NameTag:"MagFieldY", Equation:"rangesel(MagY50G,MagY1300G)", UnitType:"utesla", OffsetZero:1 },
				{ ID:"MagFieldZ", NameTag:"MagFieldZ", Equation:"rangesel(MagZ50G,MagZ1300G)", UnitType:"utesla", OffsetZero:1 },
				{ ID:"MagResultant", NameTag:"MagResultant", Equation:"sqrt(MagFieldX * MagFieldX + MagFieldY * MagFieldY + MagFieldZ * MagFieldZ )", UnitType:"utesla" },
		  	],
			Range:
			{
				ID:"Range", DefaultIndex:0,
				Ranges:[{NameTag:"pm50G", HWIndex:1},{NameTag:"pm1300G", HWIndex:2}]
			}
		},
		{
      		ID:2052, NameTag:"WirelessTempLink", DefaultRate:"2Hz", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawTemp", NameTag:"RawTemperature", Equation:"uint16(0)", Internal:1 },
        		{ ID:"Temperature", NameTag:"Temperature", Equation:"RawTemp / 172.463 - 40.0", UnitType:"DegC" }
			]
		},
		{
			ID:2053, NameTag:"Accelerometer", DefaultRate:"20Hz", MaxRate:"5000Hz", SampleSize:6,
			Measurements:
			[
				{ ID:"RawX", NameTag:"RawX", Equation:"int16(0)", UnitType:"", Internal:1 },
				{ ID:"RawY", NameTag:"RawY", Equation:"int16(2)", UnitType:"", Internal:1 },
				{ ID:"RawZ", NameTag:"RawZ", Equation:"int16(4)", UnitType:"", Internal:1 },
				{ ID:"X16", NameTag:"X16", Equation:"fcal(RawX, 0)", UnitType:"ms2", Internal:1 },
				{ ID:"Y16", NameTag:"Y16", Equation:"fcal(RawY, 1)", UnitType:"ms2", Internal:1 },
				{ ID:"Z16", NameTag:"Z16", Equation:"fcal(RawZ, 2)", UnitType:"ms2", Internal:1 },
				{ ID:"X100", NameTag:"X100", Equation:"fcal(RawX, 3)", UnitType:"ms2", Internal:1 },
				{ ID:"Y100", NameTag:"Y100", Equation:"fcal(RawY, 4)", UnitType:"ms2", Internal:1 },
				{ ID:"Z100", NameTag:"Z100", Equation:"fcal(RawZ, 5)", UnitType:"ms2", Internal:1 },
				{ ID:"X200", NameTag:"X200", Equation:"fcal(RawX, 6)", UnitType:"ms2", Internal:1 },
				{ ID:"Y200", NameTag:"Y200", Equation:"fcal(RawY, 7)", UnitType:"ms2", Internal:1 },
				{ ID:"Z200", NameTag:"Z200", Equation:"fcal(RawZ, 8)", UnitType:"ms2", Internal:1 },
				{ ID:"X400", NameTag:"X400", Equation:"fcal(RawX, 9)", UnitType:"ms2", Internal:1 },
				{ ID:"Y400", NameTag:"Y400", Equation:"fcal(RawY, 10)", UnitType:"ms2", Internal:1 },
				{ ID:"Z400", NameTag:"Z400", Equation:"fcal(RawZ, 11)", UnitType:"ms2", Internal:1 },
				{ ID:"AccelX", NameTag:"AccelX", Equation:"rangesel(X16,X100,X200,X400)", UnitType:"ms2", OffsetZero:1 },
				{ ID:"AccelY", NameTag:"AccelY", Equation:"rangesel(Y16,Y100,Y200,Y400)", UnitType:"ms2", OffsetZero:1 },
				{ ID:"AccelZ", NameTag:"AccelZ", Equation:"rangesel(Z16,Z100,Z200,Z400)", UnitType:"ms2", OffsetZero:1 },
				{ ID:"AccelRes", NameTag:"AccelResultant", Equation:"sqrt(AccelX * AccelX + AccelY * AccelY + AccelZ * AccelZ )", UnitType:"ms2" },
		  	],
			FactoryCals: [[0, 0, 1, 0.004787],[0, 0, 1, 0.004787],[0, 0, 1, 0.004787],[0, 0, 1, 0.03],[0, 0, 1, 0.03],[0, 0, 1, 0.03],
						  [0, 0, 1, 0.06],[0, 0, 1, 0.06],[0, 0, 1, 0.06],[0, 0, 1, 0.12],[0, 0, 1, 0.12],[0, 0, 1, 0.12]],
			Range:
			{
				ID:"Range", DefaultIndex:0,
				Ranges:[{NameTag:"pm16g", HWIndex:1},{NameTag:"pm100g", HWIndex:2},{NameTag:"pm200g", HWIndex:3},{NameTag:"pm400g", HWIndex:4}]
			}
		},
		{
			ID:2054, NameTag:"Altimeter", DefaultRate:"20Hz", MaxRate:"200Hz", SampleSize:4,
			Measurements:
			[
				{ ID:"RawAlt", NameTag:"RawAltitude", Equation:"uint32(0)", UnitType:"", Internal:1 },
				{ ID:"Altitude", NameTag:"Altitude", Equation:"RawAlt / 1000", UnitType:"m" },
		  	],
			Range:
			{
				ID:"Range", DefaultIndex:1,
				Ranges:[{NameTag:"FilterLow", HWIndex:1},{NameTag:"FilterMed", HWIndex:2},{NameTag:"FilterHigh", HWIndex:3}]
			}
		},
		{
			ID:2056, NameTag:"Gyro", DefaultRate:"20Hz", MaxRate:"1000Hz", SampleSize:6,
			Measurements:
			[
				{ ID:"RawX", NameTag:"RawX", Equation:"int16(0)", UnitType:"", Internal:1 },
				{ ID:"RawY", NameTag:"RawY", Equation:"int16(2)", UnitType:"", Internal:1 },
				{ ID:"RawZ", NameTag:"RawZ", Equation:"int16(4)", UnitType:"", Internal:1 },
				{ ID:"AngVelX", NameTag:"AngularVelocityX", Equation:"RawX * 0.07", UnitType:"degps", OffsetZero:1 },
				{ ID:"AngVelY", NameTag:"AngularVelocityY", Equation:"RawY * 0.07", UnitType:"degps", OffsetZero:1 },
				{ ID:"AngVelZ", NameTag:"AngularVelocityZ", Equation:"RawZ * 0.07", UnitType:"degps", OffsetZero:1 },
		  	],
		},
	  	{
			ID:2055, NameTag:"WirelessSmartGate", DefaultRate:"10Hz", Model:"PS-3225", SampleSize:3,
			Measurements:
			[
//		  		{ ID:"RawP1", NameTag:"P1", Equation:"wsgtime(0)", UnitType:"s", internal:1 },
//		  		{ ID:"RawP2", NameTag:"P2", Equation:"wsgtime(1)", UnitType:"s", internal:1 },
//				{ ID:"RawAux", NameTag:"Aux", Equation:"wsgtime(2)", UnitType:"s", internal:1 },
//				{ ID:"RawLaser", NameTag:"Laser", Equation:"wsgtime(3)", UnitType:"s", internal:1 },
				{ ID:"P1P2Spacing", NameTag:"P1P2Spacing", Value:0.015, UnitType:"m" },
//				{ ID:"PendulumWidth", NameTag:"PendulumWidth", Value:0.016, UnitType:"m" },
//        		{ ID:"Period", NameTag:"Period", Equation:"pendulum(RawP1)", UnitType:"s" },
//				{ ID:"Speed", NameTag:"Velocity", Equation:"PendulumWidth / tig(RawP1)", UnitType:"mps" },
			],
  		},
		{
			ID:2057, NameTag:"OxygenGasSensor", DefaultRate:"1Hz", Model:"PS-3217", SampleSize:6,
			Measurements:
			[
				{ ID:"RawOxygen", NameTag:"RawOxygen", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"RawTemperature", NameTag:"RawTemperature", Equation:"uint16(2)", UnitType:"", Internal:1 },
				{ ID:"RawHumidity", NameTag:"RawHumidity", Equation:"uint16(4)", UnitType:"", Internal:1 },
				{ ID:"O2Concentration", NameTag:"O2Concentration", Equation:"RawOxygen * 0.0015258", UnitType:"pph" },
				{ ID:"Temperature", NameTag:"Temperature", Equation:"RawTemperature* 0.0025177 - 40.0", UnitType:"DegC" },
				{ ID:"RelHumidity", NameTag:"RelHumidity", Equation:"RawHumidity * 0.0015258", UnitType:"percent" },
				{ ID:"AbsHumidity", NameTag:"AbsHumidity", Equation:"((13.24*RelHumidity)/(Temperature+273.15)) * Math.pow(2.71828, ((17.42*Temperature)/(Temperature+239.7)))", UnitType:"gpm3" }
		  	],
		},
		{
			ID:2061, NameTag:"AmbientLightSensor2", DefaultRate:"1Hz", Model:"PS-3213", SampleSize:8,
			Measurements:
			[
				{ ID:"RawR", NameTag:"RawR", Equation:"uint16(0)", UnitType:"", Internal:1 },
				{ ID:"RawG", NameTag:"RawG", Equation:"uint16(2)", UnitType:"", Internal:1 },
				{ ID:"RawB", NameTag:"RawB", Equation:"uint16(4)", UnitType:"", Internal:1 },
				{ ID:"RawIR", NameTag:"RawIR", Equation:"uint16(6)", UnitType:"", Internal:1 },
				{ ID:"Illuminance", NameTag:"Illuminance", Equation:"fcal(RawG, 1) * 2.0", UnitType:"lux" },
				{ ID:"UVIndex", NameTag:"UVIndex", Equation:"fcal(RawB, 0)", UnitType:"" },
				{ ID:"Irradiance", NameTag:"Irradiance", Equation:"Illuminance * 0.0104", UnitType:"wpm2" },
				{ ID:"PAR", NameTag:"PAR", Equation:"Illuminance * 0.0185", UnitType:"umolpm2ps" },
		  	],
		  	FactoryCals: [[0, 0, 28128, 7], [0, 0, 1, 1]]
		},
		{
      		ID:2062, NameTag:"WirelessSoundWaveSensor", DefaultRate:"0Hz", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawSound", NameTag:"RawSound", Equation:"uint16(0)", Internal:1 },
        		{ ID:"Sound", NameTag:"SoundWave", Equation:"RawSound * 0.00305 - 93", UnitType:"percent" }
			]
		},
		{
      		ID:2063, NameTag:"WirelessSoundLevelSensor", DefaultRate:"5Hz", SampleSize:4,
      		Measurements:
      		[
        		{ ID:"RawSoundA", NameTag:"RawSoundA", Equation:"uint16(0)", Internal:1 },
        		{ ID:"RawSoundC", NameTag:"RawSoundC", Equation:"uint16(2)", Internal:1 },
        		{ ID:"SoundA", NameTag:"SoundLevelA", Equation:"RawSoundA * 0.1", UnitType:"dBA" },
        		{ ID:"SoundC", NameTag:"SoundLevelC", Equation:"RawSoundC * 0.1", UnitType:"dBC" }
			]
		},
		{
      		ID:2065, NameTag:"SoilMoisture", DefaultRate:"1Hz", MaxRate:"1Hz", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawMoisture", NameTag:"RawMoisture", Equation:"uint16(0)", Internal:1 },
        		{ ID:"VWCLoam", NameTag:"VWCLoam", Equation:"table(RawMoisture, [7122,45,14100,20,17245,15,51725,0])", UnitType:"percent" },
        		{ ID:"VWCSand", NameTag:"VWCSand", Equation:"table(RawMoisture, [6344,35,11964,15,50689,0])", UnitType:"percent" },
        		{ ID:"VWCClay", NameTag:"VWCClay", Equation:"table(RawMoisture, [6499,45,11234,25,19999,15,52875,0])", UnitType:"percent" },
			]
		},
		{
      		ID:2066, NameTag:"CodeNodeTempLightSound", DefaultRate:"10Hz", MaxRate:"50Hz", Model:"PS-3231", SampleSize:6,
      		Measurements:
      		[
        		{ ID:"RawTemp", NameTag:"RawTemperature", Equation:"int16(0)", Internal:1 },
        		{ ID:"RawLight", NameTag:"RawLight", Equation:"uint16(2)", Internal:1 },
        		{ ID:"RawSound", NameTag:"RawSoundLevel", Equation:"uint16(4)", Internal:1 },
        		{ ID:"Temperature", NameTag:"Temperature", SymbolTag:"T", MeasType:"Temperature", Equation:"RawTemp*0.01", UnitType:"DegC", Precision:1 },
				{ ID:"Light", NameTag:"Brightness", SymbolTag:"brns", MeasType:"LightIntensity", Equation:"sqrt(RawLight)*0.3906", UnitType:"percent", Precision:0 },
				{ ID:"Loudness", NameTag:"Loudness", SymbolTag:"ldns", MeasType:"SoundIntensity", Equation:"RawSound*0.001526", UnitType:"percent", Precision:0 },
     		]
		},
		{
			ID:2067, NameTag:"CodeNodeMagneticField", DefaultRate:"10Hz", MaxRate:"50Hz", Model:"PS-3231", SampleSize:6,
			Measurements:
			[
				{ ID:"RawMagX", NameTag:"RawMagX", Equation:"int16(0)", Internal:1 },
				{ ID:"RawMagY", NameTag:"RawMagY", Equation:"int16(2)", Internal:1 },
				{ ID:"RawMagZ", NameTag:"RawMagZ", Equation:"int16(4)", Internal:1 },
				{ ID:"MagY", NameTag:"MagneticFieldStrength", SymbolTag:"B", MeasType:"MagneticField",
					Equation:"RawMagY*0.15", UnitType:"utesla", DefaultUnit:"gauss", Minimum:-1000, Maximum:1000, Precision:1 },
			]
		},
		{
			ID:2068, NameTag:"CodeNodeMotion", DefaultRate:"10Hz", MaxRate:"50Hz", Model:"PS-3231", SampleSize:6,
			Measurements:
			[
				{ ID:"RawAccX", NameTag:"RawAccX", Equation:"int16(0)", Internal:1 },
				{ ID:"RawAccY", NameTag:"RawAccY", Equation:"int16(2)", Internal:1 },
				{ ID:"RawAccZ", NameTag:"RawAccZ", Equation:"int16(4)", Internal:1 },
        		{ ID:"AccelX", NameTag:"Accelerationx", SymbolTag:"ax", MeasType:"Acceleration", Equation:"fcal(RawAccX,0)", UnitType:"ms2", Precision:1 },
				{ ID:"AccelY", NameTag:"Accelerationy", SymbolTag:"ay", MeasType:"Acceleration", Equation:"fcal(RawAccY,1)", UnitType:"ms2", Precision:1 },
				{ ID:"AccelZ", NameTag:"Accelerationz", SymbolTag:"az", MeasType:"Acceleration", Equation:"fcal(RawAccZ,2)", UnitType:"ms2", Precision:1, Internal:1 },
				{ ID:"TiltX", NameTag:"TiltAngleX", SymbolTag:"thetaX", MeasType:"Direction", Equation:"atan2(AccelX,sqrt((AccelY*AccelY)+(AccelZ*AccelZ)))*180/3.1416", UnitType:"deg", Precision:1 },
				{ ID:"TiltY", NameTag:"TiltAngleY", SymbolTag:"thetaY", MeasType:"Direction", Equation:"atan2(AccelY,sqrt((AccelX*AccelX)+(AccelZ*AccelZ)))*180/3.1416", UnitType:"deg", Precision:1 },
			],
			FactoryCals: [[0,0,1,0.002394],[0,0,1,0.002394],[0,0,1,0.002394]]
		},
		{
			ID:2069, NameTag:"CodeNodeButtons", DefaultRate:"10Hz", MaxRate:"50Hz", Model:"PS-3231", SampleSize:2,
			Measurements:
			[
				{ ID:"Button1", NameTag:"Button1", SymbolTag:"btn1", Equation:"uint8(0)", UnitType:"Unitless", Precision:0 },
				{ ID:"Button2", NameTag:"Button2", SymbolTag:"btn2", Equation:"uint8(1)", UnitType:"Unitless", Precision:0 },
			]
		},
		{
			ID:2071, NameTag:"Spirometer", DefaultRate:"50Hz", MaxRate:"1000Hz", SampleSize:2,
			Measurements:
			[
				{ ID:"RawFlowRate", NameTag:"RawFlowRate", Equation:"int16(0)", Internal:1 },
				{ ID:"FlowRate", NameTag:"FlowRate", Equation:"RawFlowRate / 1000.0", UnitType:"Lps" },
				{ ID:"FlowVolume", NameTag:"FlowVolume", Equation:"integral(FlowRate)", UnitType:"L" },
        		{ ID:"MaxFlowRate", NameTag:"MaxFlowRate", Equation:"max(FlowRate)", UnitType:"Lps" },
				{ ID:"PerBreathVolume", NameTag:"PerBreathVolume", Equation:"perbreathvolume(FlowRate, FlowVolume, 3)", UnitType:"L" },
				{ ID:"LungVolume", NameTag:"LungVolume", Equation:"2.5-FlowVolume", UnitType:"L" },
			],
		},
		{
      		ID:2074, NameTag:"EKGVoltage", DefaultRate:"200Hz", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawVoltage", NameTag:"RawVoltage", Equation:"uint16(0)", Internal:1 },
        		{ ID:"Voltage", NameTag:"Voltage", Equation:"(RawVoltage - 28500) * 0.000065", UnitType:"mV" }
			]
		},
		{
      		ID:2075, NameTag:"EKGHeartRate", DefaultRate:"1Hz", SampleSize:2,
      		Measurements:
      		[
        		{ ID:"RawHeartRate", NameTag:"RawHeartRate", Equation:"uint16(0)", Internal:1 },
        		{ ID:"HeartRate", NameTag:"HeartRate", Equation:"ekgbeat(timestamp(RawHeartRate,0.0001),0)", UnitType:"bpm" }
			]
		},
		{
			ID:2076, NameTag:"ForcePlatform", DefaultRate:"10Hz", MaxRate:"50Hz", Model:"PS-3229", SampleSize:8,
			Measurements:
			[
				{ ID:"RawForce1", NameTag:"RawForce1", Equation:"int16(0)", Internal:1 },
				{ ID:"RawForce2", NameTag:"RawForce2", Equation:"int16(2)", Internal:1 },
				{ ID:"RawForce3", NameTag:"RawForce3", Equation:"int16(4)", Internal:1 },
				{ ID:"RawForce4", NameTag:"RawForce4", Equation:"int16(6)", Internal:1 },
        		{ ID:"Force1", NameTag:"Force1", MeasType:"Force", Equation:"fcal(RawForce1,0)", UnitType:"N", OffsetZero:1 },
        		{ ID:"Force2", NameTag:"Force2", MeasType:"Force", Equation:"fcal(RawForce2,1)", UnitType:"N", OffsetZero:1 },
        		{ ID:"Force3", NameTag:"Force3", MeasType:"Force", Equation:"fcal(RawForce3,2)", UnitType:"N", OffsetZero:1 },
        		{ ID:"Force4", NameTag:"Force4", MeasType:"Force", Equation:"fcal(RawForce4,3)", UnitType:"N", OffsetZero:1 },
				{ ID:"NormalForce", NameTag:"NormalForce", MeasType:"Force", Equation:"Force1+Force2+Force3+Force4", UnitType:"N" },
			],
			FactoryCals: [[32768,0,1000,100],[32768,0,1000,100],[32768,0,1000,100],[32768,0,1000,100]]
		},
		{
			ID:2077, NameTag:"ForcePlatform2D", DefaultRate:"10Hz", MaxRate:"50Hz", Model:"PS-3230", SampleSize:10,
			Measurements:
			[
				{ ID:"RawForce1", NameTag:"RawForce1", Equation:"int16(0)", Internal:1 },
				{ ID:"RawForce2", NameTag:"RawForce2", Equation:"int16(2)", Internal:1 },
				{ ID:"RawForce3", NameTag:"RawForce3", Equation:"int16(4)", Internal:1 },
				{ ID:"RawForce4", NameTag:"RawForce4", Equation:"int16(6)", Internal:1 },
				{ ID:"RawForce5", NameTag:"RawForce4", Equation:"int16(8)", Internal:1 },
        		{ ID:"Force1", NameTag:"Force1", MeasType:"Force", Equation:"fcal(RawForce1,0)", UnitType:"N", OffsetZero:1 },
        		{ ID:"Force2", NameTag:"Force2", MeasType:"Force", Equation:"fcal(RawForce2,1)", UnitType:"N", OffsetZero:1 },
        		{ ID:"Force3", NameTag:"Force3", MeasType:"Force", Equation:"fcal(RawForce3,2)", UnitType:"N", OffsetZero:1 },
        		{ ID:"Force4", NameTag:"Force4", MeasType:"Force", Equation:"fcal(RawForce4,3)", UnitType:"N", OffsetZero:1 },
        		{ ID:"ForceP", NameTag:"Force4", MeasType:"Force", Equation:"fcal(RawForce5,3)", UnitType:"N", OffsetZero:1 },
				{ ID:"NormalForce", NameTag:"NormalForce", MeasType:"Force", Equation:"Force1+Force2+Force3+Force4", UnitType:"N" },
			],
			FactoryCals: [[32768,0,1000,100],[32768,0,1000,100],[32768,0,1000,100],[32768,0,1000,100]]
		},
		{
			ID:2079, NameTag:"WirelessGM", DefaultRate:"5s", MaxRate:"10Hz", MinRate:"3600s", Model:"PS-3238", SampleSize:4,
			Measurements:
			[
				{ ID:"CountRate", NameTag:"CountRate", Equation:"uint16(0)", UnitType:"Count/Sample" },
				{ ID:"TubeVoltage", NameTag:"TubeVoltage", Equation:"uint16(2)", UnitType:"V" },
			],
		},
		{
			ID:2082, NameTag:"ControlNodeInternal", DefaultRate:"20Hz", MaxRate:"50Hz", Model:"PS-3232", SampleSize:8,
			Measurements:
			[
				{ ID:"RawAccX", NameTag:"RawAccX", Equation:"int16(0)", Internal:1 },
				{ ID:"RawAccY", NameTag:"RawAccY", Equation:"int16(2)", Internal:1 },
				{ ID:"RawAccZ", NameTag:"RawAccZ", Equation:"int16(4)", Internal:1 },
        		{ ID:"AccelX", NameTag:"Accelerationx", SymbolTag:"ax", MeasType:"Acceleration", Equation:"fcal(RawAccX,0)", UnitType:"ms2", Precision:1 },
				{ ID:"AccelY", NameTag:"Accelerationy", SymbolTag:"ay", MeasType:"Acceleration", Equation:"fcal(RawAccY,1)", UnitType:"ms2", Precision:1 },
				{ ID:"AccelZ", NameTag:"Accelerationz", SymbolTag:"az", MeasType:"Acceleration", Equation:"fcal(RawAccZ,2)", UnitType:"ms2", Precision:1 },
				{ ID:"ServoLoad1", NameTag:"ServoLoad1", SymbolTag:"Ls1", MeasType:"Load", Equation:"uint8(6)*12.5", UnitType:"percent", Precision:1 },
				{ ID:"ServoLoad2", NameTag:"ServoLoad2", SymbolTag:"Ls2", MeasType:"Load", Equation:"uint8(7)*12.5", UnitType:"percent", Precision:1 },
			],
			FactoryCals: [[0,0,1,0.002394],[0,0,1,0.002394],[0,0,1,0.002394]]
		},
		{
			ID: 2084, NameTag: "WirelessAirQuality", DefaultRate: "1Hz", MaxRate: "1Hz", Model: "PS-3226", SampleSize: 14,
			Measurements:
			[
				{ ID: "RawTemp", NameTag: "RawTemperature", Equation: "uint16(0)", Internal: 1 },
				{ ID: "RawHumidity", NameTag: "RawHumidity", Equation: "uint16(2)", Internal: 1 },
				{ ID: "RawOzone", NameTag: "RawOzone", Equation: "uint16(4)", Internal: 1 },
				{ ID: "RawVOC", NameTag: "RawVOC", Equation: "uint16(6)", Internal: 1 },
				{ ID: "RawPM1_0", NameTag: "RawPM1_0", Equation: "uint16(8)", Internal: 1 },
				{ ID: "RawPM2_5", NameTag: "RawPM2_5", Equation: "uint16(10)", Internal: 1 },
				{ ID: "RawPM10_0", NameTag: "RawPM10_0", Equation: "uint16(12)", Internal: 1 },
				{ ID: "Temperature", NameTag: "Temperature", MeasType: "Temperature", Equation: "(RawTemp/65536)*165-40", UnitType: "DegC", Precision: 1 },
				{ ID: "RelHumidity", NameTag: "RelativeHumidity", MeasType: "Humidity", Equation: "(RawHumidity/65536)*100", UnitType: "%", Precision: 1 },
				{ ID: "OzoneNOx", NameTag: "OzoneAndNOx", MeasType: "Concentration", Equation: "RawOzone", UnitType: "ppb", Precision: 1, OffsetZero: 1 },
				{ ID: "VOC", NameTag: "VolOrgCompounds", MeasType: "Concentration", Equation: "RawVOC", UnitType: "", Precision: 1 },
				{ ID: "PM1_0", NameTag: "ParticMatter1", MeasType: "Concentration", Equation: "RawPM1_0 * 0.015259", UnitType: "ug/m3", Precision: 1 },
				{ ID: "PM2_5", NameTag: "ParticMatter2", MeasType: "Concentration", Equation: "RawPM2_5 * 0.015259", UnitType: "ug/m3", Precision: 1 },
				{ ID: "PM10_0", NameTag: "ParticMatter10", MeasType: "Concentration", Equation: "RawPM10_0 * 0.015259", UnitType: "ug/m3", Precision: 1 },
			],
		},

		{
			ID:2086, NameTag:"WirelessGM2", DefaultRate:"10Hz", MaxRate:"10Hz", MinRate:"10Hz", Model:"PS-3238", SampleSize:2,
			Measurements:
			[
				{ ID:"RawCount", NameTag:"RawCount", Equation:"uint16(0)", Internal:1 },
				{ ID:"RunningCount", NameTag:"RunningCount", Equation:"sum(RawCount)", UnitType:"Counts" },
			],
		},
		{
			ID:2501, NameTag:"HighSpeedStepper", DefaultRate:"20Hz", MaxRate:"50Hz", Model:"PS-2976", SampleSize:2,
			Measurements:
			[
        		{ ID:"Angle", NameTag:"Angle", UnitType:"rad", MeasType:"Angle", Equation:"sum(int16(0))*0.006545", Precision:1 },
				{ ID:"AngularVelocity", NameTag:"Velocity", SymbolTag:"V", MeasType:"Velocity", Equation:"deriv(Angle, Angle.x)", UnitType:"mps" }
			],
		},
		{
			ID:21132, NameTag:"SmartGateTimer", DefaultRate:"10Hz", SampleSize:3,
			Measurements:
			[
				{ ID:"P1P2Spacing", NameTag:"PhotogateSpacing", Value:0.015, UnitType:"m" },
				{ ID:"RawP1", NameTag:"P1", Equation:"wsgtime(0)", UnitType:"s", Internal:1 },
				{ ID:"RawP2", NameTag:"P2", Equation:"wsgtime(1)", UnitType:"s", Internal:1 },
				{ ID:"Speed", NameTag:"SpeedBetweenGates", Equation:"P1P2Spacing / tbg(RawP1,RawP2)", UnitType:"mps" },
			],
		},
		{
			ID:23001, NameTag:"PicketFence", DefaultRate:"10Hz", SampleSize:3,
			Measurements:
			[
				{ ID:"FlagSpacing", NameTag:"FlagSpacing", Value:0.05, UnitType:"m" },
		  		{ ID:"RawP1", NameTag:"P1", Equation:"wsgtime(0)", UnitType:"s", Internal:1 },
				{ ID:"Position", NameTag:"Position", Equation:"blockcount(RawP1) * FlagSpacing", UnitType:"m" },
				{ ID:"Velocity", NameTag:"Velocity", Equation:"deriv(Position, Position.x)", UnitType:"mps" },
				{ ID:"Acceleration", NameTag:"Acceleration", Equation:"deriv(Velocity, Velocity.x)", UnitType:"mps2" },
			],
		},
		{
			ID:23005, NameTag:"PhotogateAndPendulum", DefaultRate:"10Hz", SampleSize:3,
			Measurements:
			[
				{ ID:"PendulumWidth", NameTag:"PendulumWidth", Value:0.016, UnitType:"m" },
		  		{ ID:"RawP1", NameTag:"P1", Equation:"wsgtime(0)", UnitType:"s", Internal:1 },
				{ ID:"Period", NameTag:"Period", Equation:"pendulum(RawP1)", UnitType:"s" },
				{ ID:"Speed", NameTag:"Velocity", Equation:"PendulumWidth / tig(RawP1)", UnitType:"mps" },
			],
		},
		{
			ID:23006, NameTag:"TimeOfFlight", DefaultRate:"10Hz", SampleSize:3,
			Measurements:
			[
				{ ID:"P1P2Spacing", NameTag:"P1P2Spacing", Value:0.015, UnitType:"m" },
				{ ID:"RawP1", NameTag:"P1", Equation:"wsgtime(0)", UnitType:"s", Internal:1 },
				{ ID:"RawP2", NameTag:"P2", Equation:"wsgtime(1)", UnitType:"s", Internal:1 },
				{ ID:"RawAux", NameTag:"Aux", Equation:"wsgtime(2)", UnitType:"s", Internal:1 },
				{ ID:"Speed", NameTag:"InitialSpeed", Equation:"P1P2Spacing / tbg(RawP1, RawP2)", UnitType:"mps" },
				{ ID:"TimeOfFlight", NameTag:"TimeOfFlight", Equation:"tbg(RawP1, RawAux)", UnitType:"s" },
			],
		},
		{
			ID:23019, NameTag:"PhotogateTiming2", DefaultRate:"10Hz", SampleSize:3,
			Measurements:
			[
				{ ID:"FlagLength", NameTag:"FlagLength", Value:0.05, UnitType:"m" },
				{ ID:"PhotogateSpacing", NameTag:"PhotogateSpacing", Value:0.1, UnitType:"m" },
				{ ID:"RawP1", NameTag:"P1", Equation:"wsgtime(0)", UnitType:"s", Internal:1 },
				{ ID:"RawAux", NameTag:"Aux", Equation:"wsgtime(2)", UnitType:"s", Internal:1 },
				{ ID:"TimeInGate1", NameTag:"TimeInGate1", Equation:"tig(RawP1)", UnitType:"s" },
				{ ID:"SpeedInGate1", NameTag:"SpeedInGate1", Equation:"FlagLength/TimeInGate1", UnitType:"mps" },
				{ ID:"TimeInGate2", NameTag:"TimeInGate2", Equation:"tig(RawAux)", UnitType:"s" },
				{ ID:"SpeedInGate2", NameTag:"SpeedInGate2", Equation:"FlagLength/TimeInGate2", UnitType:"mps" },
				{ ID:"TimeBetweenGates", NameTag:"TimeBetweenGates", Equation:"tbg(RawP1, RawAux)", UnitType:"s" },
				{ ID:"VelocityBetweenGates", NameTag:"VelocityBetweenGates", Equation:"PhotogateSpacing / TimeBetweenGates", UnitType:"mps" },
				{ ID:"AccelerationBetweenGates", NameTag:"AccelerationBetweenGates", Equation:"deriv(VelocityBetweenGates,VelocityBetweenGates.x)", UnitType:"mps2" },
			],
		}
	];
//**************************************************************************************
	this.mInterfaces = 
	[
		{
			ID:24, NameTag:"WirelessHeartRate", AdvertName:"Polar ",
			Channels:
			[
				{ ID:0, SensorID:2040 }
			]
		},
		{
			ID:1024, NameTag:"AirLink", AdvertName:"AirLink",
			Channels:
			[
				{ ID:0, SensorID:0 }
			]
		},
		{
			ID:1025, NameTag:"WirelessTemperature", AdvertName:"Temperature",
			Channels:
			[
				{ ID:0, SensorID:2020 }
			]
		},
		{
			ID:1026, NameTag:"WirelessPH", AdvertName:"pH",
			Channels:
			[
				{ ID:0, SensorID:2021 }
			]
		},
		{
			ID:1027, NameTag:"WirelessPressure", AdvertName:"Pressure",
			Channels:
			[
				{ ID:0, SensorID:2022 }
			]
		},
		{
			ID:1028, NameTag:"WirelessForceAccel", AdvertName:"Force Accel",
			Channels:
			[
				{ ID:0, SensorID:2023 },
				{ ID:1, SensorID:2024 },
				{ ID:2, SensorID:2028 }
			]
		},
		{
			ID:1029, NameTag:"SmartCart", AdvertName:"Smart Cart",
			Channels:
			[
				{ ID:0, SensorID:2025 },
				{ ID:1, SensorID:2026 },
				{ ID:2, SensorID:2027 },
				{ ID:3, SensorID:2029 }
			]
		},
		{
			ID:1030, NameTag:"WirelessLight", AdvertName:"Light",
			Channels:
			[
				{ ID:0, SensorID:2030 },
				{ ID:1, SensorID:2034 }
			]
		},
		{
			ID:1031, NameTag:"WirelessVoltage", AdvertName:"Voltage",
			Channels:
			[
				{ ID:0, SensorID:2031 }
			]
		},
		{
			ID:1032, NameTag:"WirelessCurrent", AdvertName:"Current",
			Channels:
			[
				{ ID:0, SensorID:2032 }
			]
		},
		{
			ID:1033, NameTag:"WirelessConductivity", AdvertName:"Conductivity",
			Channels:
			[
				{ ID:0, SensorID:2033 }
			]
		},
		{
			ID:1034, NameTag:"WirelessCO2", AdvertName:"CO2",
			Channels:
			[
				{ ID:0, SensorID:2035 }
			]
		},
		{
			ID:1036, NameTag:"WirelessWeather", AdvertName:"Weather",
			Channels:
			[
				{ ID:0, SensorID:2037 },
				{ ID:1, SensorID:2038 },
				{ ID:2, SensorID:2039 },
				{ ID:3, SensorID:2045 }
			]
		},
		{
			ID:1038, NameTag:"WirelessColorimeter", AdvertName:"Colorimeter",
			Channels:
			[
				{ ID:0, SensorID:2041 },
				{ ID:1, SensorID:2042 }
			]
		},
		{
			ID:1039, NameTag:"WirelessLoadCell", AdvertName:"Load Cell",
			Channels:
			[
				{ ID:0, SensorID:2043 },
				{ ID:1, SensorID:2044 }
			]
		},
		{
			ID:1040, NameTag:"WirelessBloodPressure", AdvertName:"BP",
			Channels:
			[
				{ ID:0, SensorID:2046 }
			]
		},
	  	{
			ID:1041, NameTag:"WirelessRotaryMotion", AdvertName:"Rotary Motion",
			Channels:
			[
				{ ID:0, SensorID:2047 }
			]
		},
	  	{
			ID:1042, NameTag:"WirelessMotion", AdvertName:"Motion",
			Channels:
			[
				{ ID:0, SensorID:2048 }
			]
		},
	  	{
			ID:1044, NameTag:"WirelessMagField", AdvertName:"Mag Field",
			Channels:
			[
				{ ID:0, SensorID:2051 }
			]
		},
	  	{
			ID:1045, NameTag:"WirelessODO", AdvertName:"Optical DO",
			Channels:
			[
				{ ID:0, SensorID:2049 }
			]
		},
	  	{
			ID:1046, NameTag:"WirelessTempLink", AdvertName:"Temperature",
			Channels:
			[
				{ ID:0, SensorID:2052 }
			]
		},
	  	{
			ID:1047, NameTag:"WirelessAccelAltimeter", AdvertName:"Accel Alt",
			Channels:
			[
				{ ID:0, SensorID:2053 },
				{ ID:1, SensorID:2056 },
				{ ID:2, SensorID:2054 }
			]
		},
		{
			ID:1048, NameTag:"WirelessSmartGate", AdvertName:"Smart Gate",
			Channels:
			[
				{ ID:0, SensorID:21132 }
			]
		},
		{
			ID:1049, NameTag:"WirelessO2", AdvertName:"O2",
			Channels:
			[
				{ ID:0, SensorID:2057 }
			]
		},
	  	{
			ID:1052, NameTag:"WirelessSound", AdvertName:"Sound",
			Channels:
			[
				{ ID:0, SensorID:2062 },
				{ ID:1, SensorID:2063 }
			]
		},
		{
			ID:1053, NameTag:"WirelessLightColor", AdvertName:"Light",
			Channels:
			[
				{ ID:0, SensorID:2061 },
				{ ID:1, SensorID:2034 }
			]
		},
		{
			ID:1055, NameTag:"WirelessSoilMoisture", AdvertName:"Moisture",
			Channels:
			[
				{ ID:0, SensorID:2065 }
			]
		},
		{
			ID:1056, NameTag:"CodeNode", AdvertName:"//code.Node",
			Channels:
			[
				{ ID:0, SensorID:2066 },
				{ ID:1, SensorID:2067 },
				{ ID:2, SensorID:2068 },
				{ ID:3, SensorID:2069 }
			]
		},
		{
			ID:1057, NameTag:"ControlNode", AdvertName:"//control.Node",
			Channels:
			[
				{ ID:0, SensorID:0 },
				{ ID:1, SensorID:0 },
				{ ID:2, SensorID:0 },
				{ ID:3, SensorID:2082 }
			]
		},
		{
			ID:1058, NameTag:"WirelessSpirometer", AdvertName:"Spirometer",
			Channels:
			[
				{ ID:0, SensorID:2071 }
			]
		},
		{
			ID:1059, NameTag:"WirelessForcePlatform", AdvertName:"Force Platform",
			Channels:
			[
				{ ID:0, SensorID:2076 }
			]
		},
		{
			ID:1060, NameTag:"WirelessForcePlatform2D", AdvertName:"Force Plat. 2D",
			Channels:
			[
				{ ID:0, SensorID:2077 }
			]
		},
		{
			ID:1063, NameTag:"WirelessEKG", AdvertName:"EKG",
			Channels:
			[
				{ ID:0, SensorID:2074 },
				{ ID:1, SensorID:2075 }
			]
		},
		{
			ID:1064, NameTag:"WirelessGM", AdvertName:"GM",
			Channels:
			[
				{ ID:0, SensorID:2079 },
				{ ID:1, SensorID:2086 }
			]
		},
		{
			ID:1067, NameTag:"WirelessAirQuality", AdvertName:"Air Quality",
			Channels:
			[
				{ ID:0, SensorID:2084 }
			]
		},
	];

	this.SmartGateModes = [21132, 23001, 23005, 23006, 23019];

	//**************************************************************************************
	this.FindInterface = function( id )
	{
		for( let iface of this.mInterfaces )
		{
			if( iface.ID == id )
				return iface;
		}

		return null;
	}

	//**************************************************************************************
	this.FindSensor = function( id )
	{
		for( let sensor of this.mSensors )
		{
			if( sensor.ID == id )
				return sensor;
		}

		return null;
	}
}
