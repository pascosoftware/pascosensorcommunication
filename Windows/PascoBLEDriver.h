//************************************************************************
// Copyright (c) 2015 PASCO Scientific. All rights reserved.
///
/// \file   CrossPlatform\PascoBLEDriver.h
/// \author Scott Wild
/// \date   08/06/2015
/// \brief  Defines Cross-platform Interfaces and Commands used to communicate with Pasco BLE devices
///
/// This class is instantiated and owned by the BLE interface, and in turn owns
/// a platform-specific BLE Device (IBLEDevice), all communication is forwarded to
/// or received from the actual device via the IBLEDevice instance (mDriver)
///
/// This class also implements burst data transfers which improve the
/// speed of BLE devices by grouping multiple data samples into a packet
///
/// PASCO BLE SERVICES AND CHARACTERISTICS
/// --------------------------------------
///
/// Characteristics are described by A 32 Bit Value, Defined as follows:
///
///     4a 00 01 03
///     |   |  |  |
///     |	|  |   `---> Command To Send, e.g. TYPE_GENERIC_RSP_CMD = 0x03
///     |	|   `------> 0=Interface,  1-N specifies the embedded sensor #
///     | 	 `---------> Unused?
///     `-------------> PASCO_BLE_ID_HDR = 0x4a >> 24
///
///
///     **** SAMPlE: Services and Characteristics for PasTemp Sensor ****
///     ****            SUBJECT TO CHANGE                            ****
///
///     Device: name= "PasTemp: 438087: 438-087"
///
///     mServices:
///     [0]    Service: uuid=00000a18			Service Discovery? (180a), see below
///     [1]    Service: uuid=4a000000			Interface Services
///     [2]    Service: uuid=4a000100			Sensor 01 Services
///
///     mCharacteristics:
///     [0]    Characteristic: uuid=4a000001, props=0002		0x02=Read										IFace 	TYPE_IFACE_ID
///     [1]    Characteristic: uuid=4a000002, props=000c		0x08(Write) + (0x04)WriteWithoutResponse		IFace	TYPE_GENERIC_REQ_CMD
///     [2]    Characteristic: uuid=4a000003, props=0038		0x20(Indicate) + 0x10(Notify) + 0x8(Write)		IFace	TYPE_GENERIC_RSP_CMD
///     [3]    Characteristic: uuid=4a000102, props=000c		Write/WriteWithoutResponse						Sensor	TYPE_GENERIC_REQ_CMD
///     [4]    Characteristic: uuid=4a000103, props=0038		Write/Indicate/Notify							Sensor	TYPE_GENERIC_RSP_CMD
///     [5]    Characteristic: uuid=4a000104, props=0012		0x20 (Notify) + 0x02 (Read)						Sensor	TYPE_SAMPLE_DATA
///     [6]    Characteristic: uuid=4a000105, props=000c		Write/WriteWithoutResponse						Sensor	TYPE_SAMPLE_ACK
///
///
//************************************************************************

#pragma once

#include "PascoBLEDriver.h"
#include "WinBLEDevice.h"
#include <stdio.h>
#include <vector>
#include <string>

#define PASCO_BLE_ID_HDR		0x4a

#define RETRY_FLAG              0x80

// Last byte of Characteristic, e.g. the XX of characteristic 4A0000XX
#define TYPE_IFACE_ID           0x01
#define TYPE_GENERIC_REQ_CMD    0x02
#define TYPE_GENERIC_RSP_CMD    0x03
#define TYPE_SAMPLE_DATA        0x04
#define TYPE_SAMPLE_ACK         0x05

// Generic events (Byte 0 of Data Buffer Received via Generic Response)
#define GEVT_SYNC               0x81
#define GEVT_SENSOR_ID          0x82
#define GEVT_SAMPLE_ERROR		0x83
#define GEVT_BUTTON_PRESS		0x84
#define GEVT_BATTERY_INFO		0x85
#define GEVT_BURST_BEGIN		0x86
#define GEVT_BURST_END			0x87
#define GEVT_ACCESSORY_DETECT	0x88
#define GEVT_OVERCURRENT		0x89

// Generic commands (Byte 0 of Data Buffer Sent via Generic Command)
#define GCMD_NOP				0x00
#define GCMD_SET_SAMPLE_PERIOD  0x01
#define GCMD_SET_SAMPLE_SIZE    0x02
#define GCMD_WRITE_DEVICE_NAME  0x03
#define GCMD_READ_DEVICE_NAME   0x04
#define GCMD_READ_ONE_SAMPLE    0x05
#define GCMD_START_SAMPLING     0x06
#define GCMD_STOP_SAMPLING      0x07
#define GCMD_REQUEST_SENSOR_ID  0x08

#define GCMD_START_BURST_XFER	0x09	// Prepare to read/write data from/to temporary RAM storage
#define GCMD_WRITE_BURST_DATA	0x0a	// Send the read/write data, 1st byte is a sequence count
#define GCMD_BURST_DATA_DONE	0x0b	// Ask for or indicate completion of a burst
#define GCMD_RESEND_BURST_DATA	0x0c	// Ask to re-send specific packets
#define GCMD_ERASE_FLASH		0x0d
#define GCMD_XFER_BURST_RAM		0x0e	// Copy burst RAM to/from flash,sensor,nvr
#define GCMD_XFER_PAS_SPI_DATA	0x0f	// Copy 1-16 bytes of data to/from SPI sensor

#define GCMD_DO_FIRMWARE_UPGRADE	0x10
#define GCMD_DISCONNECT				0x11	// Disconnect from the client
#define GCMD_POWER_OFF				0x12	// Power down and disconnect from client
#define GCMD_IS_SPI_SENSOR_DETECTED	0x13	// Returns whether a sensor is plugged in
#define GCMD_SET_LEDS				0x14	// Forces LEDs on or off
#define GCMD_GET_FIRMWARE_VERSION	0x15	// Get x.y version and build date
#define GCMD_TEST_WATCHDOG			0x16	// Force a hard loop that should trigger a watchdog reset
#define GCMD_SET_IDLE_TIMEOUTS		0x17	// Currently not implemented
#define GCMD_REQUEST_BATTERY_INFO	0x18	// Force a battery info event

#define GCMD_LOG_START				0x19	// Start logging sample data
#define GCMD_LOG_STOP				0x20	// Stop logging sample data
#define GCMD_LOG_DELETE				0x21	// Delete logged sample data
#define GCMD_LOG_STATUS				0x22	// Get logging status

#define GCMD_SYNC					0x23	// Send sync cmd

#define GCMD_WRITE_NVR				0x24	// Write an entry to the NVR (up to 16 bytes)
#define GCMD_READ_NVR				0x25	// Read an entry from the NVR (up to 16 bytes)
#define GCMD_DELETE_NVR				0x26	// Delete an entry from the NVR
#define GCMD_GET_USB_BLE_INFO		0x27	// Allows USB channel to ask for BLE info while connected via BLE
#define GCMD_SET_RANGE				0x28	// Allows setting of range/gain for sensors that require it
#define GCMD_SET_TRIGGER_MODE		0x29	// Sets burst/trigger mode for burst sampling
#define GCMD_XFER_ACCESSORY_CMD		0x2a	// Send/recv a command to the Smart Cart accessory port
#define GCMD_SET_ACCESSORY_PORT		0x2b	// Write/Read the pins of the accessory port
#define GCMD_LOAD_FLASH_CAL_DATA	0x2c	// Load cal data from flash that the firmware uses
#define GCMD_ENABLE_RAW_DATA		0x2d	// Enable individual raw data measurements
#define GCMD_START_SYNC_SCAN		0x2e	// Start listening for sync advertising packets
#define GCMD_STOP_SYNC_SCAN			0x2f	// Stop listening for sync advertising packets
#define GCMD_SEND_SYNC_EVTS			0x30	// Send a burst of sync event notifications
#define GCMD_SEND_SYNC_ADVERT		0x31	// Send burst of sync advertising packets
#define GCMD_READ_SYS_TSTAMP		0x32	// Read system timestamp
#define GCMD_READ_SYNC_TSTAMPS0		0x33	// Get timestamps of recvd sync advertising packets
#define GCMD_READ_SYNC_TSTAMPS1		0x34	// Get timestamps of recvd sync advertising packets
#define GCMD_RW_CO2_RAW_CMD			0x35	// Send WinSen CO2 probe command
#define GCMD_LIGHT_SOURCE_CMD		0x36	// Send light source command
#define GCMD_COLORIMETER_CMD		0x37	// Send Colorimeter command
#define GCMD_MOTION_CMD				0x37	// Send motion sensor command
#define GCMD_DROP_COUNTER_CMD		0x37	// Send drop counter command
#define GCMD_SMART_GATE_CMD			0x37	// Send smart gate command
#define GCMD_ROTARY_MOTION_CMD		0x37	// Send rotary motion sensor command
#define GCMD_SMART_CART_CMD			0x37	// Send smart cart command
#define GCMD_FUNC_GEN_CMD			0x37	// Send function generator command
#define GCMD_WEATHER_CMD			0x37	// Send weather sensor command
#define GCMD_STEM_CMD				0x37	// Send STEM Controller command
#define GCMD_CODENODE_CMD			0x37	// Send Code Node command
#define GCMD_CONTROL_NODE_CMD		0x37	// Send Code Node command
#define GCMD_CUSTOM_CMD				0x37	// Send sensor-specific command (generic version of the previous sensor-specific commands)
#define GCMD_LOCAL_CAL_CMD			0x38	// Initiate/Stop/Save a local calibration (e.g. mag field cal)
#define GCMD_GPS_CMD				0x39	// GPS information command
#define GCMD_SET_COMPRESSION		0x3a	// Set data compression parameters
#define GCMD_LOCATE					0x3b	// Blink the device's LEDs to locate it
#define GCMD_LOG_ERASE_MEM			0x3c	// Pre-erase log flash to allow faster logging
#define GCMD_EXECUTE_PASCODE		0x3d	// Run the downloaded Pascode
#define GCMD_READ_PASCODE_STATUS	0x3e	// Get run/error status of the currently running Pascode
#define GCMD_XFER_EXT_ACCESSORY_CMD	0x3f	// Send/recv an extended command to the Control Node accessory port

#define CO2_CMD_WRITE_UART				0	// Write raw serial data to the Winsen CO2 probe
#define CO2_CMD_READ_UART				1	// Read raw serial data from the Winsen CO2 probe
#define CO2_CMD_FLUSH_UART				2	// Empty any data from the UART recv queue
#define CO2_CMD_BAUD_RATE_9600 			3	// Set the baud rate to 9600 for the Winsen CO2 probe
#define CO2_CMD_BAUD_RATE_115200		4	// Set the baud rate to 115200 for the Winsen CO2 probe
#define CO2_CMD_POWER_OFF_PROBE			5	// Turn off the power to the Winsen CO2 probe
#define CO2_CMD_POWER_ON_PROBE			6	// Turn on the power to the Winsen CO2 probe
#define CO2_CMD_WRITE_RAM				7	// Write data to RAM buffer in the CO2 sensor
#define CO2_CMD_XFER_RAM_TO_UART		8	// Send RAM buffer to UART
#define ODO_CMD_GET_PRESSURE			9	// Read barometric pressure that was present when the sensor was turned on

// Generic responses
#define GRSP_RESULT				0xc0	// Optional response to last generic cmd: |err|chan|gcmd|data[]

// Flags for transferring data to/from burst RAM
#define GCMD_XFER_READ_FLAG			0x80
#define GCMD_XFER_LOC_FLASH			0x01
#define GCMD_XFER_LOC_SPI_SENSOR	0x02
#define GCMD_XFER_LOC_NVR			0x03
#define GCMD_XFER_LOC_LOG			0x04
#define GCMD_XFER_LOC_FLASH2		0x05

#define SAMPLE_ERROR_SENSOR_TIMEOUT 	0x01
#define SAMPLE_ERROR_ACK_TIMEOUT 		0x02
#define SAMPLE_ERROR_OVERFLOW 			0x03

// Sensor non-volatile storage tags
#define NVSTORAGE_NAME_TAG 				1
#define NVSTORAGE_FACTORY_CAL_TAG(x)	(2+x) // x=sensor index (0 to 7)
#define NVSTORAGE_CART_COLOR_TAG		10
#define NVSTORAGE_UVI_CAL_TAG			11		// Wireless Light Sensor UVIndex calculation coefficients
#define NVSTORAGE_USER_CAL_TAG(x)		(12+x) // x=sensor index (0 to 7)
#define NVSTORAGE_CUR_RANGE_TAG(x)		(20+x) // x=sensor index (0 to 7)
#define NVSTORAGE_MEASUREMENT_CONSTANT  28     // this is specific for the Conductivity sensor
#define NVSTORAGE_ISE_PROBE_MEAS_ID		29	   // Which ISE probe is being used
#define NVSTORAGE_MAG_FIELD_OFFSETS		30	   // Mag field sensor calibration offsets
#define NVSTORAGE_NEW_LOG_DATA			31	   // Presence of this indicates that there is new logged data to be downloaded
#define NVSTORAGE_ACDC_DAC_OFFSETS		32	   // Centering output voltages for AC/DC module sine and square waves

// Force LED bits
#define FORCE_LED_ON			0x80
#define FORCE_LED_CONN_RED		0x01
#define FORCE_LED_CONN_GREEN	0x02
#define FORCE_LED_CONN_AMBER	0x04
#define FORCE_LED_BATT_RED		0x08
#define FORCE_LED_BATT_GREEN	0x10
#define FORCE_LED_BATT_AMBER	0x20
#define FORCE_LED_MISC_1		0x100
#define FORCE_LED_MISC_2		0x200
#define FORCE_LED_MISC_3		0x400

// Logged data tags
#define LOG_TAG_VERSION				0x01
#define LOG_TAG_START_TIME 			0x02
#define LOG_TAG_STOP				0x03
#define LOG_TAG_SAMPLE_RATE			0x04
#define LOG_TAG_START_OFFSET		0x05
#define LOG_TAG_TRIGGER_INFO		0x06
#define LOG_TAG_INVALID				0xff
#define LOG_TAG_SAMPLE_DATA(index)	(0x40 + index)

// Accessory port bits
#define ACCESSORY_PORT_SCK		1
#define ACCESSORY_PORT_MOSI		2
#define ACCESSORY_PORT_MISO		4
#define ACCESSORY_PORT_SENSEn	0x80

// Accessory commands
#define ACCESSORY_CMD_GET_INFO 	0xa5
#define ACCESSORY_CMD_SET_SPEED	0xa6
#define ACCESSORY_CMD_UPDATE	0xa7
#define ACCESSORY_CMD_SET_ADDR	0xc0
#define ACCESSORY_CMD_ERASE		0xc1
#define ACCESSORY_CMD_WRITE		0xc2
#define ACCESSORY_CMD_READ		0xc3
#define ACCESSORY_CMD_RESET		0xc4
#define ACCESSORY_CMD_BOOT_INFO	0xc5
#define ACCESSORY_END_OF_COMMAND	0xa5

// Light source commands
#define LIGHT_CMD_SET_RED_PWM			1
#define LIGHT_CMD_SET_GREEN_PWM			2
#define LIGHT_CMD_SET_BLUE_PWM			3
#define LIGHT_CMD_SET_RED_CURRENT		4
#define LIGHT_CMD_SET_GREEN_CURRENT		5
#define LIGHT_CMD_SET_BLUE_CURRENT		6
#define LIGHT_CMD_SET_RED_PERIOD		7
#define LIGHT_CMD_SET_GREEN_PERIOD		8
#define LIGHT_CMD_SET_BLUE_PERIOD		9
#define LIGHT_CMD_SET_RED_DATA			10
#define LIGHT_CMD_SET_GREEN_DATA		11
#define LIGHT_CMD_SET_BLUE_DATA			12
#define LIGHT_CMD_SET_PWM_AND_CURRENT	13
#define LIGHT_CMD_START_DATA			14
#define LIGHT_CMD_SET_YELLOW			15
#define LIGHT_CMD_SET_GROUP				16

// Colorimeter commands
#define COLORIMETER_CMD_SET_LEDS			1
#define COLORIMETER_CMD_SET_INT_TIME		8
#define COLORIMETER_CMD_SET_GAIN			9
#define COLORIMETER_CMD_PROG_FLASH			10

// Motion sensor commands
#define MOTION_GET_TEMPERATURE		1

// Drop counter commands
#define DROP_COUNTER_CAL			1

// Smart Gate commands
#define WSG_GET_STATUS				1	// Get Smart Gate blocked states and whether aux port has something plugged in
#define WSG_SET_MODE				2	// Enables/Disables Ch1/2 LEDs and Laser Switch

// Smart Cart commands
#define CMD_CONFIG_BALLISTIC_CART	2
#define SC_CMD_SET_ENABLE_ACC_CHECK	3	// Allow disabling of the accessory polling loop so it doesn't interfere with accessory firmware upgrades
#define BC_TRIG_NONE			0
#define BC_TRIG_ZERO			1		// Trigger when cart returns to the same position it was in when requested
#define BC_TRIG_DISTANCE		2		// Trigger when cart moves a specified distance
#define BC_TRIG_TIME			3		// Trigger after a specified amount of time
#define BC_TRIG_START_SAMPLING	0x80	// If true, start trigger when sampling is started.  If false, start trigger immediately.
#define CMD_CONFIG_VISUAL_VECTOR	4	// Visual vector display configuration
#define VV_MODE_FORCE			0
#define VV_MODE_ACCELEROMETER	1
#define VV_MODE_WHEEL_ACCEL		2
#define VV_MODE_VELOCITY		3

// Code Node commands
#define CODENODE_CMD_SET_LED		2
#define CODENODE_CMD_SET_LEDS		3
#define CODENODE_CMD_SET_SOUND_FREQ	4

// Control Node commands
#define CTRLNODE_CMD_DETECT_DEVICES		2	// Detects which devices are attached
#define CTRLNODE_CMD_SET_SERVO 			3	// Enables PWM to servo
#define CTRLNODE_CMD_SET_STEPPER		4	// Enables stepper motor
#define CTRLNODE_CMD_SET_SIGNALS		5	// Sets motor control signals directly
#define CTRLNODE_CMD_XFER_ACCESSORY		6	// Write I2C accessory and read response
#define CTRLNODE_CMD_READ_LINE_FOLLOWER	7	// Read the line follower accessory
#define CTRLNODE_CMD_GET_STEPPER_INFO   8   // Get stepper remaining distance and remaining angular velocity to accelerate to
#define CTRLNODE_CMD_STOP_ACCESSORIES   9   // Turn off all steppers, servos, and accessories
#define CTRLNODE_CMD_SET_BEEPER			10  // Set beeper frequency
#define CTRLNODE_CMD_SET_ENABLE_ACC_CHECK 11// Allow disabling of the accessory polling loop so it doesn't interfere with accessory firmware upgrades

// Control Node accessory IDs
#define CN_ACC_ID_NONE             0
#define CN_ACC_ID_MOTOR_BASE       2500
#define CN_ACC_ID_STEPPER_480      2501
#define CN_ACC_ID_STEPPER_4800     2502
#define CN_ACC_ID_MOTOR_3          2503
#define CN_ACC_ID_LINE_FOLLOWER    2504
#define CN_ACC_ID_RANGE_SENSOR     2505

// Rotary motion sensor commands
#define RMS_SET_LED_OFF_WHEN_NOT_SAMPLING	1 // Conserves power by turning off RMS LED while not sampling

// Wireless Func Gen measurement sources
#define WFG_SRC_OUTPUT				0
#define WFG_SRC_REFERENCE			1
#define WFG_SRC_PRE_WAVE			2

// Wireless Func Gen waveform
#define WFG_WAVEFORM_SQUARE			1
#define WFG_WAVEFORM_SINE			2
#define WFG_WAVEFORM_TRIANGLE		3
#define WFG_WAVEFORM_DC				4

// Wireless Func Gen commands
#define WFG_CMD_SET_WAVEFORM		1
#define WFG_CMD_SET_FREQUENCY		2
#define WFG_CMD_SET_VOLTAGE			3
#define WFG_CMD_SET_OUTPUT_ENABLE	4
#define WFG_CMD_SET_OFFSET_DAC		5
#define WFG_CMD_SET_CUR_LIMIT_DAC	6
#define WFG_CMD_SET_MDAC			7
#define WFG_CMD_SET_MEAS_SRC		8

// Wireless GM commands
#define GM_CMD_SET_VOLTAGE			1

// Weather sensor requests
#define WEATHER_GET_HARDWARE_INFO	1
#define WEATHER_MAG_UNKNOWN			0
#define WEATHER_MAG_MAG3110			1
#define WEATHER_MAG_LIS2MDL			2
#define WEATHER_MAG_BMM150			3

// Local calibration
#define LOCAL_CAL_STOP				0
#define LOCAL_CAL_START				1
#define LOCAL_CAL_SAVE				2

// GPS requests
#define GPS_GET_SAT_COUNT			0
#define GPS_GET_IS_COMMUNICATING	1 // Returns true if NMEA strings are being processed, even if no fix has occurred.  Used to validate uart connection

// Soil Moisture sensor requests
#define SM_CMD_GET_PROBE_ID				1

// STEM Controller requests
#define STEM_CMD_XFER_MOTOR_SPI		2
#define STEM_MOTOR_CONTROL			0xb2 // Command that tells the motor controller to set the motor's speed

// Force Platform requests
#define FP_CMD_FACTORY_ZERO_BEAMS	0xa3
#define FP_CMD_SET_TRIGGER_TARE_OFFSETS	2

// Burst sampling
#define BLE_TRIGGER_MODE_NONE		0
#define BLE_TRIGGER_MODE_IMMEDIATE	1
#define BLE_TRIGGER_MODE_POS_EDGE	2
#define BLE_TRIGGER_MODE_NEG_EDGE	3

// Range settings
#define SET_RANGE_NONE				0
#define SET_RANGE_1					1
#define SET_RANGE_2					2
#define SET_RANGE_3					3
#define SET_RANGE_4					4
#define SET_RANGE_5V				1
#define SET_RANGE_15V				2
#define SET_RANGE_100MA				3
#define SET_RANGE_1A				4

#define MAX_BLE_PACKET_SIZE     144

class UsbDriver;
class WinBLEDevice;

class BleConnectionInfo
{
public:
	bool mIsConnected;		// Is the device connected via BLE
	int mUniqueID;			// The xxx-xxx unique identifier for the sensor
};

#define MAX_RETRY_CACHE_SIZE 0x20

//****************************************************************
//
//****************************************************************
class PascoBLESensorInfo
{
public:
    PascoBLESensorInfo( )
    {
        Reset( );
    }
    
    void Reset( )
    {
		mBurstMode = false;
		mBurstDone = false;
		mBurstTriggerTime = -1;
        mRecvPktCountForAck = 0;
        mRetryCacheSize = MAX_RETRY_CACHE_SIZE; //todo: have sensor give us this value - must be a power of 2
        mRetryCacheSizeMask = mRetryCacheSize - 1;
        mLastProcessedSequence = mRetryCacheSize -1;
        mLastRecvdSequence = mRetryCacheSize - 1;
        WaitForSingleObject( mMutex, INFINITE );
        mRetryCache.clear( );
        mRetryCache.resize( mRetryCacheSize );
        mData.clear( );
        ReleaseMutex( mMutex );
    }
    
	bool mBurstMode;
	bool mBurstDone;
    HANDLE mMutex;
    std::vector<unsigned char> mData;
	int mBurstTriggerTime;				// Holds trigger time, in samples, of the last burst sample
    int mLastProcessedSequence;         // Last recvd packet before we determined that we missed some packet(s)
    int mLastRecvdSequence;             // Expected sequence ID of the next valid packet
    int mRecvPktCountForAck;
    int mRetryCacheSize;
    int mRetryCacheSizeMask;
    std::vector<std::vector<unsigned char> > mRetryCache;    // Holds recvd packets while waiting for retried packets
};

//************************************************************************
//
//************************************************************************
class IPascoBLEDriverEvents
{
public:
	virtual void OnRecvBatteryInfo( int mV, int percent, bool low, bool charging, bool full ) { };
	virtual void OnRecvSensorID( int channel, int sensorID ) { };
	virtual void OnSyncDone( ) { }
	virtual void OnInterfaceConnected( ) { };
	virtual void OnInterfaceDisconnected( int error ) { };
	virtual void OnInterfaceRemoved( ) { };
	virtual void OnAccessoryDetected( bool detected ) { };
	virtual void OnError( int charID, int error ) { };
	virtual void OnError( int errorCode ) {};
	virtual void OnBurstBegin( int channel, int triggerTime ) { }
	virtual void OnBurstEnd( int channel ) { }
};

//************************************************************************
//
//************************************************************************
class PascoBLEDriver : public IBLEDeviceEvents
{
public:
	PascoBLEDriver( );
    ~PascoBLEDriver( );

	bool SendNop( );
	bool SetSampleConfig( int channel, unsigned int periodUS, int sampleSize, bool resetOnStart );
	bool StartSampling( unsigned short *startFrames = NULL, int numFrames = 0 );
	bool StopSampling( );
	bool RequestInterfaceID( );
	bool AckSampleData( int channel, int lastPacket, unsigned int missingMask );
    
	bool DoBurstWriteTransfer( const void *data, int count );		// Do a blocking multi-packet data transfer to a RAM buffer
	bool DoBurstReadTransfer( void *data, int count );				// Do a blocking multi-packet data transfer from the RAM buffer
	bool StartBurstXfer( bool read, int total, int packetSize );	// Tell the device we want to do a burst transfer to/from the burst data buffer
	bool WriteBurstData( int sequence, void *data, int count );		// Write a packet to the burst data buffer
	bool BurstXferDone( int missingPktMask = 0 );					// Indicate that we're done writing the burst data
	bool XferBurstRam( bool read, int location, int addr, int count, int *actual = NULL ); // Copy data to/from RAM buffer from/to flash, SPI sensor, or non volatile RAM
	bool ReadOneSample( int channel, void *data, int count );		// Read one sample's worth of raw data
	bool GetFirmwareVersion( int &major, int &minor );
	bool WriteNVR( int tag, const void *data, int count );
	bool ReadNVR( int tag, void *data, int count, int &actual );
	bool GetBleConnectionInfo( BleConnectionInfo &info );
	bool SetTriggerMode( int channel, int mode, int level = 0, int pre = 0, int post = 0, int src = 0 );
	bool ForcePlatformSetTriggerOffsets( int offset1, int offset2, int offset3, int offset4 );

	void SetBLEDriver( WinBLEDevice *driver );
    void SetEventHandler( IPascoBLEDriverEvents *handler );

	bool SendData( int channel, int cmd, const void *data, int size, bool needResponse );
	bool SendGenericData( int channel, int genericCmd, const void *data, int size, bool needResponse, int timeout = 0 );
	bool Connect( WinBLEDevice* driver );
	bool Disconnect( ); // { return mDriver ? mDriver->Disconnect( ) : false; }

	// IBLEDeviceEvents implementations ( called by mDriver )
    virtual void OnRecvData( int chanID, const void *data, int size );

	void OnRecvBurstBegin( int channel, unsigned int tstamp );
	void OnRecvBurstEnd( int channel );
    void OnRecvSampleData( int channel, const void *data, int size );
    void ReadSampleData( int channel, void *data, int count, int alignCount, int &actual );
    int GetSampleDataCount( int channel );
	void DumpPacket( int charID, const void *data, int len );

public:
	int mInterfaceID;
	WinBLEDevice *mDriver;	// Low level BLE access driver
	IPascoBLEDriverEvents *mEventHandler;
    std::vector<PascoBLESensorInfo *> mSensorInfo;
	int mMaxBleSendSize;	// Max number of bytes we can send to the device in a single ble packet
	int mMaxBleRecvSize;	// Max number of bytes we can recv from the device in a single ble packet
    
	unsigned int mBurstPacketMask;	// Bitmap of packets recvd or sent: 1=needs to send OR waiting to recv, 0=sent OR recvd
	void *mBurstRecvData;			// Recvd data packet data gets placed here
	int mBurstRecvCount;			// Total expected bytes to receive
	int mBurstPktDataSize;			// Number of burst data bytes per packet
	bool mBurstDoneRecvd;			// true if we got a burst done msg from the device
	int mBurstChecksum;				// Checksum reported by the sensor after burst has completed
	int mRequest;					// What request are we waiting for a response for
	int mResponseResult;			// Generic response error code
	int mResponseCount;
	unsigned char mResponseData[MAX_BLE_PACKET_SIZE];// Generic response data goes here

	// Making sure commands are not called concurrently and that we wait for responses where required
	HANDLE mMutex;
	HANDLE mWaitForResponse;
	HANDLE mWaitForResponseMutex;

	bool mGotResponse;

	bool mLogConnect = false;
	bool mLogRequests = true;
	bool mLogRawData = false;
	bool mLogSampleData = false;
};
