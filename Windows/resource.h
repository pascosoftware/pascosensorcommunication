//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PascoBLESensor.rc
//
#define IDD_PASCOBLESENSOR_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDC_BTN_CONNECT                 1000
#define IDC_EDIT_CONSOLE                1001
#define IDC_BTN_SCAN                    1002
#define IDC_BTN_STOP_SAMPLING           1003
#define IDC_BTN_START_SAMPLING          1004
#define IDC_CHK_LOG_PACKETS             1005
#define IDC_BTN_STOP_SAMPLING2          1006
#define IDC_BTN_ONE_SHOT_READ           1006
#define IDC_BUTTON1                     1007
#define IDC_DEVICE_LIST                 1008
#define IDC_LIST1                       1009

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1010
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
