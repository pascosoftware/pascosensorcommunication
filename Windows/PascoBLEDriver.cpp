
#include "pch.h"
#include "PascoBLEDriver.h"
#include "WinBLEDevice.h"
#include "math.h"

#define MAX_SENSOR_CHANNELS 4
typedef unsigned char UC;

int conPrintf( const char *format, ... );
bool WaitForEvent( bool& evt, int timeout );
void HexDump( const void *data, int count );

#define CharID(channel,cmd)     (cmd | ((channel) << 8) | (PASCO_BLE_ID_HDR << 24))

//*******************************************************************
//
//*******************************************************************
PascoBLEDriver::PascoBLEDriver( )
{
	mDriver = NULL;
	mEventHandler = NULL;
	mMaxBleSendSize = 20;
	mMaxBleRecvSize = 20;
	mLogConnect = false;
	mLogRawData = false;
	mLogRequests = false;
	mLogSampleData = false;
	mInterfaceID = 0;

	for( int i = 0; i < MAX_SENSOR_CHANNELS; i++ )
		mSensorInfo.push_back( new PascoBLESensorInfo( ) );
}

//*******************************************************************
//
//*******************************************************************
PascoBLEDriver::~PascoBLEDriver( )
{
	if (mDriver)
		mDriver->mEventHandler = NULL;
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::Connect( WinBLEDevice *driver )
{
	// Note that if we are called with a null portName the device is disconnected...
	mDriver = driver;
    
	// Try connecting to the BLE device
	if( !mDriver->Connect( 5000 ) )
	{
		conPrintf( "Failed to connect\n" );
		return false;
	}

	mDriver->mEventHandler = this;
    
    // Allow the appropriate characteristics to send us data.  Since we don't know how many
    // sensor services we have, just keep enabling the next one until we get an error.
    // Each device should have at least one sensor service.
    bool success = mDriver->EnableNotifications( CharID(0, TYPE_GENERIC_RSP_CMD), true );

    for( int i = 0; i < MAX_SENSOR_CHANNELS; i++ )
    {
        success = success && mDriver->EnableNotifications( CharID(i+1, TYPE_SAMPLE_DATA), true );
        success = success && mDriver->EnableNotifications( CharID(i+1, TYPE_GENERIC_RSP_CMD), true );
        
        if( !success )
        {
            if( i == 0 )
                conPrintf( "PascoBLEDriver could not enable notifications\n" );
            break;
        }
    }

	mMaxBleSendSize = 20;
	mMaxBleRecvSize = 20;
	mInterfaceID = mDriver->mInterfaceID;

	return true;
}

bool PascoBLEDriver::Disconnect( )
{
	return mDriver ? mDriver->Disconnect( ) : false;
}

//*******************************************************************
// This just pings the sensor and does nothing else
//*******************************************************************
bool PascoBLEDriver::SendNop( )
{
	return SendGenericData( 0, GCMD_NOP, NULL, 0, false );
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::GetFirmwareVersion( int& major, int& minor )
{
	mResponseResult = -1;
	mResponseCount = 0;
	memset( mResponseData, 0, sizeof( mResponseData ) );
	if( !SendGenericData( 0, GCMD_GET_FIRMWARE_VERSION, NULL, 0, true, 1000 ) )
		return false;

	if( mResponseResult || mResponseCount < 2 )
		conPrintf( "Error %d while getting firmware version", mResponseResult );
	else
	{
		major = mResponseData[0];
		minor = mResponseData[1];
	}

	// Return true if no error
	return mResponseResult == 0 && mResponseCount >= 2;
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::SetSampleConfig( int channel, unsigned int periodUS, int sampleSize, bool resetOnStart )
{
	unsigned char cmd[6] = { (unsigned char)(periodUS & 0xff),
							 (unsigned char)((periodUS >> 8) & 0xff),
							 (unsigned char)((periodUS >> 16) & 0xff),
							 (unsigned char)((periodUS >> 24) & 0xff),
							 (unsigned char)(sampleSize & 0xff),
							 (unsigned char)(resetOnStart ? 1 : 0) };

	return SendGenericData( channel + 1, GCMD_SET_SAMPLE_PERIOD, cmd, sizeof( cmd ), false );
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::StartSampling( unsigned short *startFrames, int numFrames )
{
	if( numFrames > 8 )
		numFrames = 8;
	return SendGenericData( 0, GCMD_START_SAMPLING, startFrames, numFrames * 2, true );
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::StopSampling( )
{
	return SendGenericData( 0, GCMD_STOP_SAMPLING, NULL, 0, true );
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::ReadNVR( int tag, void *data, int count, int &actual )
{
	unsigned char cmd[2];
	cmd[0] = tag;
	cmd[1] = count;
	if( !SendGenericData( 0, GCMD_READ_NVR, cmd, sizeof( cmd ), true, 1000 ) )
		return false;

	if( mResponseResult )
		; // conPrintf( "Error %d while reading NVR", mResponseResult );
	else
	{
		actual = mResponseCount;
		if( actual > count )
			actual = count;
		memcpy( data, mResponseData, actual );
	}

	// Return the boolean response data value
	return mResponseResult == 0;
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::SetTriggerMode( int channel, int mode, int level, int pre, int post, int src )
{
	if( channel < (int)mSensorInfo.size( ) )
		mSensorInfo[channel]->mBurstMode = mode != BLE_TRIGGER_MODE_NONE;

	unsigned char cmd[9] = {(unsigned char)(mode & 0xff),
							(unsigned char)((mode >> 8) & 0xff),
							(unsigned char)(level & 0xff),
							(unsigned char)((level >> 8) & 0xff),
							(unsigned char)(pre & 0xff),
							(unsigned char)((pre >> 8) & 0xff),
							(unsigned char)(post & 0xff),
							(unsigned char)((post >> 8) & 0xff),
							(unsigned char)src };

	return SendGenericData( channel + 1, GCMD_SET_TRIGGER_MODE, cmd, sizeof( cmd ), true );
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::ReadOneSample( int channel, void *data, int count )
{
	if( count > 16 )
		count = 16;

	mResponseResult = -1;
	mResponseCount = 0;
	if( !SendGenericData( channel + 1, GCMD_READ_ONE_SAMPLE, &count, 1, true, 1000 ) )
		return false;

	if( mResponseResult )
		conPrintf( "Error %d while reading a sample\n", mResponseResult );
	else
		memcpy( data, mResponseData, count );

	// Return the boolean response data value
	return mResponseResult == 0;
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::AckSampleData( int channel, int lastPacketSequence, unsigned int missingMask )
{
    unsigned char data[5];
    *(unsigned int *)data = missingMask;
    data[4] = lastPacketSequence;

	return SendData( channel + 1, TYPE_SAMPLE_ACK, data, 5, false );
}

//*******************************************************************
// Read a block of data from sensor RAM.  Data comes in as a set of
// 16-byte packets, each with a sequence number so that we can detect missing packets
//
// Packet sequence:
//
// -> StartBurstXfer(read, totalCount)
// Repeat the following sequence until no missing packets are reported
// <- WriteBurstData(seq)
// <-    ...
// <- WriteBurstData(seq)
// <- BurstXferDone(checksum)
// -> BurstXferDone(missingPktMask)
//
// Success when no missing packets and checksum is correct
//*******************************************************************
bool PascoBLEDriver::DoBurstReadTransfer( void *data, int count )
{
	// Max xfer size is 512 bytes
	if( !data || !count || count > 0x400 )
		return false;

	// If firmware doesn't support extended packets, use 16 bytes per packet for backward compatibility with older firmware.
	// Otherwise, limit max packet size to 128 bytes of data.  Also allow for two header bytes per packet.
	if( mMaxBleRecvSize <= 20 ) mBurstPktDataSize = 16;
	else if( mMaxBleRecvSize > 130 ) mBurstPktDataSize = 128;
	else mBurstPktDataSize = mMaxBleRecvSize - 2;

	int numPackets = (count + mBurstPktDataSize - 1) / mBurstPktDataSize;
	int lastPacketSize = (count % mBurstPktDataSize) ? (count % mBurstPktDataSize) : mBurstPktDataSize;
	int pass;

	// Place data where the event handler can get to them
	mBurstPacketMask = (1 << numPackets) - 1; // Set a bit for each packet to be recvd
	mBurstRecvData = data;
	mBurstRecvCount = count;

	mBurstDoneRecvd = false;
	if( !StartBurstXfer( true, count, mBurstPktDataSize ) )
	{
		conPrintf( "Failed to start burst read transfer\n" );
		return false;
	}

	// Try up to five times in case all of the packets didn't make it
	#define MAX_PASSES 5
	for( pass = 0; pass < MAX_PASSES; pass++ )
	{
		#define FAIL_TIMEOUT_COUNT 3
		int failCount = 0;

		// Wait up to three seconds for the device to give us its packets
		mBurstDoneRecvd = WaitForEvent( mBurstDoneRecvd, 3000 );
//		WaitForSingleObject( mWaitForResponseMutex, INFINITE );
//		if( !mBurstDoneRecvd )
//			WaitForSingleObject( mWaitForResponse, 1000 );
//		ReleaseMutex( mWaitForResponseMutex );

		// If we didn't get a completion response from the device, something bad has happened
		if( !mBurstDoneRecvd )
		{
			conPrintf( "Failed read burst transfer due to DONE request not being received\n" );
			return false;
		}

		// If we got all of our packets, we're done.
		if( mBurstPacketMask == 0 )
			break;
		else // Otherwise...
		{
			conPrintf( "Not all burst packets made it from the device.  Retrying: 0x%08x/%08x\n", mBurstPacketMask, (1 << numPackets) - 1 );

			mBurstDoneRecvd = false;
			if( !BurstXferDone(  ) )
			{
				conPrintf( "Failed to request resending of missing packets for burst read transfer\n" );
				return false;
			}
		}
	}

//	conPrintf( "Burst read %d bytes:", count );
//	HexDump( data, count );

	return true;
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::StartBurstXfer( bool read, int total, int packetSize )
{
	unsigned char data[4] = { (unsigned char)read, (unsigned char)(total & 0xff), (unsigned char)((total >> 8) & 0xff), (unsigned char)packetSize };
	return SendGenericData( 0, GCMD_START_BURST_XFER, data, sizeof( data ), true );
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::WriteBurstData( int sequence, void *dataIn, int count )
{
	if( count > MAX_BLE_PACKET_SIZE )
		return false;

	unsigned char data[MAX_BLE_PACKET_SIZE];
	data[0] = sequence;
	memcpy( &data[1], dataIn, count );
	return SendGenericData( 0, GCMD_WRITE_BURST_DATA, data, count + 1, false );
}

//*******************************************************************
// Tell the device we're done sending burst data packets to it
// OR that the device needs to re-send some missing packets
//*******************************************************************
bool PascoBLEDriver::BurstXferDone( int missingPktMask )
{
	return SendGenericData( 0, GCMD_BURST_DATA_DONE, &missingPktMask, 4, true );
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::XferBurstRam( bool read, int location, int addr, int count, int *actual )
{
	unsigned char cmd[7];
	cmd[0] = (read ? GCMD_XFER_READ_FLAG : 0) | location;
	memcpy( &cmd[1], &addr, 4 );
	memcpy( &cmd[5], &count, 2 );
	mResponseResult = -1;
	if( !SendGenericData( 0, GCMD_XFER_BURST_RAM, cmd, 7, true, 1000 ) )
		return false;

	if( actual )
		*actual = (mResponseResult == 0 && mResponseCount >= 2) ? *(unsigned short *)mResponseData : 0;

	if( mResponseResult )
		conPrintf( "Error %d reading flash into RAM\n", mResponseResult );

	return mResponseResult == 0;
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::ForcePlatformSetTriggerOffsets( int offset1, int offset2, int offset3, int offset4 )
{
	unsigned char cmd[9] = { (unsigned char)FP_CMD_SET_TRIGGER_TARE_OFFSETS,
					(unsigned char)(offset1 & 0xff), (unsigned char)(offset1 >> 8),
					(unsigned char)(offset2 & 0xff), (unsigned char)(offset2 >> 8),
					(unsigned char)(offset3 & 0xff), (unsigned char)(offset3 >> 8),
					(unsigned char)(offset4 & 0xff), (unsigned char)(offset4 >> 8) };
	return SendGenericData( 0, GCMD_CUSTOM_CMD, cmd, sizeof( cmd ), false, 1000 );
}
//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::SendData( int channel, int cmd, const void *data, int size, bool needResponse )
{
	if( !mDriver )
		return false;

	DumpPacket( CharID( channel, cmd ), data, size );
    
	bool success = mDriver->SendData( CharID( channel, cmd ), data, size, false );

	if( !success )
		conPrintf( "Failed to send data: ch=%d, cmd=%d, size=%d, needResp=%d\n", channel, cmd, size, needResponse );

	return success;
}

//*******************************************************************
//
//*******************************************************************
bool PascoBLEDriver::SendGenericData( int channel, int genericCmd, const void *data, int size, bool needResponse, int timeout )
{
	if( !mDriver )
		return false;

	if( size > mMaxBleSendSize - 1 )
	    size = mMaxBleSendSize - 1;
    
	char buf[MAX_BLE_PACKET_SIZE];
	buf[0] = genericCmd;
	if( size )
		memcpy( &buf[1], data, size );

	int charID = TYPE_GENERIC_REQ_CMD | (channel << 8) | (PASCO_BLE_ID_HDR << 24);
	DumpPacket( charID, buf, size + 1 );
	mResponseResult = -1;
	mResponseCount = 0;
	mRequest = genericCmd;
	mGotResponse = false;
	bool success = mDriver->SendData( charID, buf, size + 1, false );

	if( !success )
		conPrintf( "Failed to send generic data: ch=%d, gcmd=%d, size=%d, needResp=%d\n", channel, genericCmd, size, needResponse );

	// Wait for a generic response
	if( success && timeout )
	{
		// Wait for the device to respond
		WaitForEvent( mGotResponse, timeout );
//		WaitForSingleObject( mWaitForResponseMutex, INFINITE );
//		if( !mBurstDoneRecvd )
//			WaitForSingleObject( mWaitForResponse, timeout );
//		ReleaseMutex( mWaitForResponseMutex );

		// 
		if( mResponseResult )
		{
			conPrintf( "Error %d response to generic BLE cmd 0x%02x\n", mResponseResult, genericCmd );
			success = false;
		}
	}

	return success;
}

//*******************************************************************
//
//*******************************************************************
int PascoBLEDriver::GetSampleDataCount( int channel )
{
    if( channel < 0 || channel >= (int)mSensorInfo.size( ) || !mSensorInfo[channel] )
        return 0;

	return mSensorInfo[channel]->mData.size( );
}

//*******************************************************************
//
//*******************************************************************
void PascoBLEDriver::ReadSampleData( int channel, void *data, int count, int alignCount, int &actual )
{
    actual = 0;

	// A little sanity check
    if( channel < 0 || channel >= (int)mSensorInfo.size( ) || !mSensorInfo[channel] )
        return;
    
	PascoBLESensorInfo *info = mSensorInfo[channel];

	// If incomplete burst mode transfer, we're done
	if( info->mBurstMode && !info->mBurstDone )
		return;

	// Only return as much as our buffer can hold
    int dataCount = info->mData.size( );
    if( dataCount > count )
        dataCount = count;
    
    // Make sure data is an even multiple of bytes per sample
    if( alignCount && dataCount >= alignCount )
    {
		dataCount -= dataCount % alignCount;
        actual = dataCount;

		WaitForSingleObject( mMutex, INFINITE );
		if( data )
			memcpy( data, info->mData.data( ), dataCount );
		info->mData.erase( info->mData.begin( ), info->mData.begin( ) + dataCount );
		ReleaseMutex( mMutex );
    }
}

//*******************************************************************
//
//*******************************************************************
void PascoBLEDriver::OnRecvSampleData( int channel, const void *data, int size )
{
    // Add sensor info if a new sensor is encountered
    if( channel >= (int)mSensorInfo.size( ) )
        return;
    
    PascoBLESensorInfo *info = mSensorInfo[channel];
    if( !info )
    {
        info = new PascoBLESensorInfo( );
        mSensorInfo[channel] = info;
    }
    
    int sequence = *(unsigned char *)data;
    bool timeToAck = false;

    // If we are just being asked to return the ack status
    if( size == 1 )
    {
        timeToAck = true;
    }
    
    else // We got a sample data packet
	{
		bool isRetry = (sequence & RETRY_FLAG) != 0;
        sequence &= info->mRetryCacheSizeMask;

		// Determine if this packet is an expected missing packet
        bool isMissing;
        if( info->mLastRecvdSequence >= info->mLastProcessedSequence )
            isMissing = sequence > info->mLastProcessedSequence && sequence < info->mLastRecvdSequence;
        else
            isMissing = sequence > info->mLastProcessedSequence || sequence < info->mLastRecvdSequence;

        // If this packet falls between the last processed and last received packets, add it to the cache.
        if( isMissing )
		{
	        if( mLogSampleData )
				conPrintf( "Got %s packet %02x\n", isRetry ? "retried" : "missing", sequence & info->mRetryCacheSizeMask );

			WaitForSingleObject( info->mMutex, INFINITE );
            info->mRetryCache[sequence] = std::vector<unsigned char>( size, *(const unsigned char *)data );
			ReleaseMutex( info->mMutex );
			timeToAck = true;

			// Check the cache to see if the re-sent packet and any following packets can now be processed
			int begin = (info->mLastProcessedSequence + 1) & info->mRetryCacheSizeMask;
			int end = (info->mLastRecvdSequence + 1) & info->mRetryCacheSizeMask;
			int i;
			for( i = begin; i != end; i = (i + 1) & info->mRetryCacheSizeMask )
			{
	            std::vector<unsigned char> &cache = info->mRetryCache[i];
				if( cache.size( ) )
				{
	                WaitForSingleObject( info->mMutex, INFINITE );
					info->mData.insert( info->mData.end( ), cache.data( ) + 1, cache.data( ) + cache.size( ) );
					info->mLastProcessedSequence = cache.data( )[0] & info->mRetryCacheSizeMask;
					cache.clear( );
					ReleaseMutex( info->mMutex );

					if( mLogSampleData )
						conPrintf( "Processing cached packet %02x\n", info->mLastProcessedSequence );
				}
				else
	                break;
			}
		}
        
        // This is not a missing packet in the sequence AND it's not a rogue retry packet
        else if( !isRetry )
		{
			bool caching = info->mLastRecvdSequence != info->mLastProcessedSequence;

			// How far off is the new sequence from the last recvd sequence
			int seqOffset = sequence - info->mLastRecvdSequence;
			if( seqOffset < 0 )
				seqOffset += info->mRetryCacheSize;

			// If we are not caching data waiting for a missing packet AND this is the next expected packet, process it immediately
			if( !caching && seqOffset == 1 )
			{
				// Every four or eight recvd packets will result in an ack packet back to the sensor
				if( ++info->mRecvPktCountForAck == (info->mRetryCacheSize == 8 ? 4 : 8))
					timeToAck = true;
            
	            WaitForSingleObject( info->mMutex, INFINITE );
				info->mData.insert( info->mData.end( ), (const unsigned char*)data + 1, (const unsigned char*)data + size );
				info->mLastProcessedSequence = sequence;
				ReleaseMutex( info->mMutex );
			}

			// If we are already caching data waiting for a missing packet OR we detected a missed packet,
			// tell the device to resend the missing packet(s) and cache this packet until the missing packets are received
			else if( caching || (seqOffset > 1 && seqOffset < (info->mRetryCacheSize - 3)) )
			{
				if( mLogSampleData )
					conPrintf( "Queue packet %02x until retries are complete\n", sequence );
	            WaitForSingleObject( info->mMutex, INFINITE );
				info->mRetryCache[sequence & info->mRetryCacheSizeMask] = std::vector<unsigned char>( size, *(const unsigned char *)data );
				timeToAck = true;
				ReleaseMutex( info->mMutex );
			}
        
			// Any packet that is not an expected missing packet AND has a sequence that preceeds the last
			// received sequence is a duplicate packet and should be ignored
			else
				conPrintf( "Ignoring duplicate packet %02x\n", sequence );

			info->mLastRecvdSequence = sequence;
		}

        else
			conPrintf( "Ignoring unexpected retry packet %02x\n", sequence );
        
    }
    
    if( mLogSampleData )
    {
		conPrintf( "Post State: seq=%02x, lastRecvd=%02x, lastProcessed=%02x\n", sequence, info->mLastRecvdSequence, info->mLastProcessedSequence );
        std::string str;
//        for( int i = 0; i < info->mRetryCacheSize; i++ )
//            str += " " + QString::number( info->mRetryCache[i].size( ) );
        conPrintf( "Cache State = %s\n", str.c_str( ) );
    }
    
    // If we need to notify the sensor that we have received some packets or that we need some packets re-sent
    if( timeToAck )
    {
        unsigned int missingBits = 0;
        
        // If we have missing packets, tell the sensor what they are
        if( info->mLastProcessedSequence != info->mLastRecvdSequence )
        {
            int begin = (info->mLastProcessedSequence + 1) & info->mRetryCacheSizeMask;
            int end = info->mLastRecvdSequence & info->mRetryCacheSizeMask;
            
            // Walk through the cache of packets that have not yet been processed and not where there are
            // missing packets based on gaps in their sequences so that we can tell the sensor to resend those packets.
            for( int i = begin; i != end; i = (i + 1) & info->mRetryCacheSizeMask)
                if( !info->mRetryCache[i].size( ) )
                    missingBits |= (1 << i);
        }
        
        // Tell the device which packets we have received
        AckSampleData( channel, info->mLastRecvdSequence, missingBits );
        
        info->mRecvPktCountForAck = 0;
    }
}

//*******************************************************************
//
//*******************************************************************
void PascoBLEDriver::OnRecvBurstBegin( int channel, unsigned int tstamp )
{
	if( channel < 0 || channel >= (int)mSensorInfo.size( ) )
		return;

	PascoBLESensorInfo *info = mSensorInfo[channel];

	info->mBurstTriggerTime = tstamp;
	info->mData.clear( );

	if ( mEventHandler )
		mEventHandler->OnBurstBegin( channel, tstamp );
}

//*******************************************************************
//
//*******************************************************************
void PascoBLEDriver::OnRecvBurstEnd( int channel )
{
	if( channel < 0 || channel >= (int)mSensorInfo.size( ) )
		return;

	PascoBLESensorInfo *info = mSensorInfo[channel];

	info->mBurstDone = true;

	if ( mEventHandler )
		mEventHandler->OnBurstEnd( channel );
}

//*******************************************************************
//
//*******************************************************************
void PascoBLEDriver::OnRecvData( int charID, const void *vdata, int size )
{
	if( !vdata || !size || (charID >> 24) != PASCO_BLE_ID_HDR )
		return;

	DumpPacket( charID, vdata, size );
    
	const unsigned char *data = (const unsigned char *)vdata;
	const unsigned char *gdata = &data[1];
	int channel = (charID >> 8) & 0xff;
	int cmd = charID & 0xff;
	int gcmd = *data;

	// If we recvd a burst data read packet, place it in the appropriate location in the burst recv buffer
	if( cmd == TYPE_GENERIC_RSP_CMD && gcmd == GCMD_WRITE_BURST_DATA )
	{
		int index = gdata[0] & 0x1f;

		// Skip over generic command and index
		size -= 2;
		gdata++;

		// Copy packet data (up to 16 bytes) to recv buf amd mark it as received
		if( mBurstRecvCount && mBurstRecvData && size >= 1 && size <= mBurstPktDataSize )
		{
			// Get packet index
			int lastExpectedPacket = (mBurstRecvCount - 1) / mBurstPktDataSize;
			int lastPacketSize = (mBurstRecvCount % mBurstPktDataSize) ? (mBurstRecvCount % mBurstPktDataSize) : mBurstPktDataSize;
			mBurstPacketMask &= ~(1 << index);

			// If the packet will fit in the recv data buffer, place it there
			if( (index < lastExpectedPacket) || (index == lastExpectedPacket && size <= lastPacketSize) )
				memcpy( (void *)((char*)mBurstRecvData + (index * mBurstPktDataSize)), gdata, size );
		}
	}

	// If we got a burst transfer completion, let the requester know
	else if( cmd == TYPE_GENERIC_RSP_CMD && gcmd == GCMD_BURST_DATA_DONE )
	{
		// If sending burst data to the device, the DONE cmd will also tell us
		// if the device got all of the packets we sent to it
		if( !mBurstRecvData )
			mBurstPacketMask = *(int *)gdata;
		mBurstChecksum = *(unsigned short *)&gdata[4];

//		WaitForSingleObject( mWaitForResponseMutex, INFINITE );
		mBurstDoneRecvd = true;
//		ReleaseMutex( mWaitForResponseMutex );
//		SetEvent( mWaitForResponse );
	}

	// If we got a result code from a generic request
	else if( cmd == TYPE_GENERIC_RSP_CMD && gcmd == GRSP_RESULT )
	{
		// Ignore any response that was not meant for the current request
		if( gdata[1] == mRequest )
		{
			WaitForSingleObject( mWaitForResponseMutex, INFINITE );
			mResponseResult = gdata[0];
			mResponseCount = size > 3 && size < MAX_BLE_PACKET_SIZE ? size - 3 : 0;
			memcpy( mResponseData, &gdata[2], mResponseCount );
			ReleaseMutex( mWaitForResponseMutex );
			mGotResponse = true;
//			SetEvent( mWaitForResponse );
		}
	}

     
    // If we got sample data from the device
    else if( channel > 0 && channel < 5 && cmd == TYPE_SAMPLE_DATA )
        OnRecvSampleData( channel - 1, data, size );
	else if( channel > 0 && channel < 5 && size >= 5 && cmd == TYPE_GENERIC_RSP_CMD && gcmd == GEVT_BURST_BEGIN )
		OnRecvBurstBegin( channel - 1, *(int *)gdata );
	else if( channel > 0 && channel < 5 && size >= 1 && cmd == TYPE_GENERIC_RSP_CMD && gcmd == GEVT_BURST_END )
		OnRecvBurstEnd( channel - 1 );

	// Send all other events up to the parent's handler
	else if( mEventHandler )
	{
		if( size >= 3 && channel > 0 && cmd == TYPE_GENERIC_RSP_CMD && gcmd == GEVT_SENSOR_ID )
		{
			for( int ch = 0; ch < (size - 1) / 2; ch++ )
				mEventHandler->OnRecvSensorID( ch, ((unsigned short*)gdata)[ch] );
		}
        else if( size >= 3 && channel == 0 && cmd == TYPE_GENERIC_RSP_CMD && gcmd == GEVT_BATTERY_INFO )
            mEventHandler->OnRecvBatteryInfo( *(unsigned short *)gdata, *(unsigned short *)&gdata[2], (gdata[4] & 2) != 0, (gdata[4] & 1) != 0, (gdata[4] & 4) != 0  );
        else if( size >= 2 && cmd == TYPE_GENERIC_RSP_CMD && gcmd == GEVT_ACCESSORY_DETECT )
			mEventHandler->OnAccessoryDetected( *gdata != 0 );
        else if( size >= 2 && cmd == TYPE_GENERIC_RSP_CMD && gcmd == GEVT_SAMPLE_ERROR )
			mEventHandler->OnError( 0, *gdata );
	}
}
//*******************************************************************
//
//*******************************************************************
void PascoBLEDriver::DumpPacket( int charID, const void *vdata, int lenIN )
{
	if( !mLogRequests )
		return;
	int len = lenIN;
	int cmd = charID & 0xff;
	int channel = (charID >> 8) & 0xff;
	unsigned char *data = (unsigned char *)vdata;

	conPrintf( " " );

	if( mLogRawData )
	{
		bool recvd = (cmd == TYPE_SAMPLE_DATA || cmd == TYPE_GENERIC_RSP_CMD);
		conPrintf( "%s %d bytes of packet data on char 0x%08x\n", recvd ? "Recvd" : "Sent", len, charID );
		HexDump( vdata, len );
	}

	// If we recvd sample data
	if( cmd == TYPE_SAMPLE_DATA )
	{
		conPrintf( "Recv %d bytes of sample data on channel %d: seq=%02x\n", len - 1, channel, data[0] );
		HexDump( data + 1, len - 1 );
		return;
	}

	// If we acked sample data
	else if( cmd == TYPE_SAMPLE_ACK )
	{
		conPrintf( "Ack Sample Data: ch=%d, lastPkt=%02x, missing=0x%08x\n", channel, data[4], *(int *)data );
		return;
	}

	// If this is a generic recvd packet
	else if( cmd == TYPE_GENERIC_RSP_CMD && len )
	{
		// Get the generic cmd
		cmd = *data++;
		len--;

//		if( cmd == GEVT_SYNC )
//			conPrintf("Sync: seq=%d, tstamp=%d, hosttime=%d, offset = %d", data[0], *(unsigned short *)&data[1], ZSystemUtility::GetTickCountMilliseconds() & 0xffff, mSyncOffset & 0xffff);
		if( cmd == GEVT_SENSOR_ID )
			conPrintf( "Sensor ID: 0x%04x\n", *(unsigned short *)data );
		else if( cmd == GEVT_BATTERY_INFO )
			conPrintf( "Battery Info: voltage=%dmV, percent=%d, charging=%d, low=%d, full=%d\n",
				*(unsigned short *)data, *(unsigned short *)&data[2], (data[4] & 1) != 0, (data[4] & 2) != 0, (data[4] & 4) != 0 );
		else if( cmd == GEVT_BURST_BEGIN )
			conPrintf( "Burst Begin: tstamp=%d\n", *(unsigned int *)data );
		else if( cmd == GEVT_BURST_END )
			conPrintf( "Burst End\n" );
		else if( cmd == GEVT_ACCESSORY_DETECT )
			conPrintf( "Accessory Detect %d\n", *data );
		else if( cmd == GCMD_WRITE_BURST_DATA )
		{
			conPrintf( "Recv burst data: index=%d, size=%d\n", data[0], len - 1 );
			if( len )
				HexDump( &data[1], len - 1 );
		}
		else if( cmd == GCMD_BURST_DATA_DONE )
			conPrintf( "Burst data done: mask=0x%08x, checksum=0x%04x\n", *(int *)data, *(unsigned short *)&data[4] );
		else if( cmd == GRSP_RESULT )
		{
			cmd = data[1];
			int err = data[0];
			data += 2;
			if( len >= 2 )
				len -= 2;

			if( cmd == GCMD_GET_FIRMWARE_VERSION )
				conPrintf( "Recv Firmware version %d.%d\n", data[0], data[1] );
			else if( cmd == GCMD_GET_USB_BLE_INFO )
				conPrintf( "Recv BLE connection info: connected=%d, uid=%d\n", data[0], data[1] + ((int)data[2] << 8) + ((int)data[3] << 16) );
			else if( cmd == GCMD_READ_SYNC_TSTAMPS0 || cmd == GCMD_READ_SYNC_TSTAMPS1 )
			{
				conPrintf( "Recv sync tstamp data: size=%d, err=%d\n", len, err );
				HexDump(data, len);
			}
			else if( cmd == GCMD_XFER_ACCESSORY_CMD )
			{
				conPrintf( "Response to xfer spi accessory cmd, err=%d\n", err );
			}
			else
			{
				conPrintf( "Generic response to cmd 0x%02x, size=%d, err=%d\n", cmd, len, err );
				HexDump( data, len );
			}
		}
		else
		{
			conPrintf( "Unknown generic response 0x%02x for %d bytes\n", cmd, len );
			HexDump( data, len );
		}
	}

	// If this is a generic sent packet
	else if( cmd == TYPE_GENERIC_REQ_CMD && len )
	{
		// Get the generic cmd
		cmd = *data++;
		len--;

		if( cmd == GCMD_NOP )
			conPrintf( "Sent NO-OP\n" );
		else if( cmd == GCMD_SET_SAMPLE_PERIOD )
			conPrintf( "Sent channel %d Set Sample Period: period=%dms, size=%d, resetOnStart=%d, compressed=%d\n",
				channel, *(int *)data, data[4], data[5] & 1, (data[5] & 2) != 0 );
		else if( cmd == GCMD_SET_SAMPLE_SIZE )
			conPrintf( "Sent channel %d Set Sample Size %d\n", channel, *(short *)data );
		else if( cmd == GCMD_READ_ONE_SAMPLE )
			conPrintf( "Sent channel %d Read One Sample: Size=%d\n", channel, data[0] );
		else if( cmd == GCMD_START_SAMPLING )
			conPrintf( "Sent Start Sampling command\n" );
		else if( cmd == GCMD_STOP_SAMPLING )
			conPrintf( "Sent Stop Sampling command\n" );
		else if( cmd == GCMD_REQUEST_SENSOR_ID )
			conPrintf( "Sent Request Sensor ID command\n" );
		else if( cmd == GCMD_START_BURST_XFER )
			conPrintf( "Sent Start Burst Xfer command: rd=%d, total=%d, pktSize=%d\n", data[0], *(short *)&data[1], data[3] );
		else if( cmd == GCMD_BURST_DATA_DONE )
			conPrintf( "Sent Burst Data Done command: missing=0x%08x\n", *(int *)data );
		else if( cmd == GCMD_WRITE_BURST_DATA )
		{
			conPrintf( "Sent Write Burst Data command: seq=%d, size=%d\n", data[0], len - 1 );
			if( len > 0 )
				HexDump( &data[1], len - 1 );
		}
		else if( cmd == GCMD_ERASE_FLASH )
			conPrintf( "Sent Erase Flash command: addr=0x%05x, count=%d\n", *(int *)data, *(short *)&data[4] );
		else if( cmd == GCMD_XFER_BURST_RAM )
			conPrintf( "Sent Xfer Burst Ram command: loc=%d, rd=%d, addr=0x%05x, count=%d\n", data[0] & 0xf, data[0] >> 7, *(int *)&data[1], *(short *)&data[5] );
		else if( cmd == GCMD_XFER_PAS_SPI_DATA )
		{
			conPrintf( "Sent Xfer Pasport SPI data command: rdCnt=%d, wrCount=%d\n", data[0], len - 1 );
			if( len > 0 )
				HexDump( &data[1], len - 1 );
		}
		else if( cmd == GCMD_XFER_ACCESSORY_CMD )
		{
			conPrintf( "Sent Xfer SPI accessory command\n" );
			HexDump( data, len );
		}
		else if( cmd == GCMD_DO_FIRMWARE_UPGRADE )
			conPrintf( "Sent Do Firmware Upgrade command: addr=0x%05x\n", *(int *)data );
		else if( cmd == GCMD_DISCONNECT )
			conPrintf( "Sent Disconnect request command\n" );
		else if( cmd == GCMD_GET_FIRMWARE_VERSION )
			conPrintf( "Sent Get firmware version command\n" );
		else if( cmd == GCMD_READ_DEVICE_NAME )
			conPrintf( "Sent Read Device Name command\n" );
		else if( cmd == GCMD_START_SYNC_SCAN )
		{
//			QByteArray name = QByteArray( (const char *)data, len );
//			conPrintf( "Sent Sync Start Scan command: name=%s", name.constData( ) );
		}
		else if( cmd == GCMD_STOP_SYNC_SCAN )
			conPrintf( "Sent Sync Stop Scan command\n" );
		else if( cmd == GCMD_SEND_SYNC_EVTS )
			conPrintf( "Sent Sync Events command\n" );
		else if( cmd == GCMD_SEND_SYNC_ADVERT )
			conPrintf( "Sent Start Sync Advert command\n" );
		else if( cmd == GCMD_READ_SYS_TSTAMP )
			conPrintf( "Sent Sync Read Sys Timestamp command\n" );
		else if( cmd == GCMD_READ_SYNC_TSTAMPS0 || cmd == GCMD_READ_SYNC_TSTAMPS1 )
			conPrintf( "Sent Sync Read Scan Timestamps (%d) command\n", cmd - GCMD_READ_SYNC_TSTAMPS0 );
		else if( cmd == GCMD_LOG_START )
			conPrintf( "Sent Log Start command\n" );
		else if( cmd == GCMD_LOG_STOP )
			conPrintf( "Sent Log Stop command\n" );
		else if( cmd == GCMD_LOG_DELETE )
			conPrintf( "Sent Log Delete command\n" );
		else if( cmd == GCMD_LOG_STATUS )
			conPrintf( "Sent Log Status command\n" );
		else if( cmd == GCMD_SYNC )
			conPrintf( "Sent Sync command\n" );
		else if( cmd == GCMD_WRITE_NVR )
			conPrintf( "Sent Write NVR command: tag=0x%02x\n", data[0] );
		else if( cmd == GCMD_READ_NVR )
			conPrintf( "Sent Read NVR command: tag=0x%02x count=%d\n", data[0], data[1] );
		else if( cmd == GCMD_DELETE_NVR )
			conPrintf( "Sent Delete NVR command: tag=0x%02x\n", data[0] );
		else if( cmd == GCMD_GET_USB_BLE_INFO )
			conPrintf( "Get BLE connection info command\n" );
		else if( cmd == GCMD_SET_TRIGGER_MODE )
		{
			const char *strs[] = {"None", "Immediate", "PosEdge", "NegEdge"};
			conPrintf( "Set Trigger Mode command: mode=%s, level=%d, pre=%d, post=%d\n", strs[data[0] & 3],
					*(unsigned short *)&data[2], *(unsigned short *)&data[4], *(unsigned short *)&data[6] );
		}
		else
		{
			conPrintf( "Unknown generic command 0x%02x for %d bytes\n", cmd, len );
			HexDump( data, len );
		}
	}
}
