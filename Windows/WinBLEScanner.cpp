
#include "pch.h"
#include "WinBLEScanner.h"
#include "WinBLEDevice.h"

#include <vector>

#include <functional>
#include <robuffer.h>

#include <windows.storage.streams.h>
#include <windows.foundation.collections.h>
#include <windows.devices.bluetooth.h>	
#include <windows.devices.enumeration.h>	
#include <windows.devices.bluetooth.background.h>
#include <windows.devices.bluetooth.rfcomm.h>

using namespace std;

using namespace Microsoft::WRL;
using namespace Microsoft::WRL::Wrappers;

using namespace ABI::Windows::Storage::Streams;
using namespace Windows::Storage::Streams;

using namespace ABI::Windows::Foundation;
using namespace ABI::Windows::Foundation::Collections;

using namespace ABI::Windows::Devices;
using namespace ABI::Windows::Devices::Enumeration;
using namespace ABI::Windows::Devices::Bluetooth;
using namespace ABI::Windows::Devices::Bluetooth::Advertisement;
using namespace ABI::Windows::Devices::Bluetooth::Rfcomm;

int conPrintf( const char* format, ... );

//***********************************************************************
WinBLEScanner* WinBLEScanner::sInstance = NULL;
//***********************************************************************

//***********************************************************************
//
//***********************************************************************
WinBLEScanner::WinBLEScanner( )
{
	sInstance = this;
	mDeviceMutex = CreateMutex( NULL, false, NULL );
	mScanning = false;
}

//***********************************************************************
//
//***********************************************************************
WinBLEScanner::~WinBLEScanner( )
{
}

//***********************************************************************
//
//***********************************************************************
bool WinBLEScanner::StartScan( )
{
	RoInitializeWrapper initialize(RO_INIT_MULTITHREADED);
	HRESULT hr = S_OK;
	
	conPrintf( "Start BLE Scanning\n" );

	if (!mAdvertisementWatcher)
	{
		hr = RoActivateInstance(HString::MakeReference(RuntimeClass_Windows_Devices_Bluetooth_Advertisement_BluetoothLEAdvertisementWatcher).Get(), &mAdvertisementWatcher);

		mAdvertisementReceived = Callback<ITypedEventHandler<BluetoothLEAdvertisementWatcher*, BluetoothLEAdvertisementReceivedEventArgs*> >
			(std::bind(
				&WinBLEScanner::OnAdvertisementReceived,
				this,
				std::placeholders::_1,
				std::placeholders::_2
			));

		hr = mAdvertisementWatcher->add_Received(mAdvertisementReceived.Get(), &mAdvertisementReceivedEventToken);

		// Make sure we ask for the scan packet since it may contain the device name (e.g. for the Polar heart rate sensor)
		mAdvertisementWatcher->put_ScanningMode( BluetoothLEScanningMode_Active );
	}

	hr = mAdvertisementWatcher->Start();
	
	mScanning = true;
	return true;
}

//***********************************************************************
//
//***********************************************************************
bool WinBLEScanner::StopScan( )
{
	conPrintf( "Stop BLE Scanning\n" );

	mScanning = false;
	if (mAdvertisementWatcher)
	{
		mAdvertisementWatcher->Stop();
	}

    return true;
}

//**************************************************************
//
//**************************************************************
static int Decode64( int ch )
{
	// 0-9 		-> '0' - '9'
	// 10-25	-> 'K' - 'Z' -- Note: this funkiness is required due to an error in this code that was caught
	// 26-35	-> 'A' - 'J' -- after the wireless sensors went into production, so part of the bug stays
	// 36-61	-> 'a' - 'z' -- since the bug also exists in the matching sensor firmware code
	// 62		-> '#'
	// 63		-> '*'
	if( ch >= '0' && ch <= '9' )
		return ch - '0';
	else if( ch >= 'K' && ch <= 'Z' )
		return ch - 'A';
	else if( ch >= 'A' && ch <= 'J' )
		return ch - 'A' + 26;
	else if( ch >= 'a' && ch <= 'z' )
		return ch - 'a' + 36;
	else if( ch == '*' )
		return 62;
	else if( ch == '#' )
		return 63;
	else
		return -1;
}

//***********************************************************************
// I do this weird thing instead of events and mutexes because the BLE responses
// use the main thread message pump and blocking the main thread prevents
// BLE completion responses from being received.
//***********************************************************************
bool WaitForEvent( bool& evt, int timeout )
{
	DWORD endTime = ::GetTickCount( ) + timeout;

	while( !evt && ::GetTickCount( ) < endTime )
		AfxGetApp( )->PumpMessage( );

	evt = false;

	return ::GetTickCount( ) < endTime;
}

//***********************************************************************
//
//***********************************************************************
HRESULT WinBLEScanner::OnAdvertisementReceived(IBluetoothLEAdvertisementWatcher* watcher, IBluetoothLEAdvertisementReceivedEventArgs* args)
{
	INT16 currentRssi;
	args->get_RawSignalStrengthInDBm(&currentRssi);

	ComPtr<IBluetoothLEAdvertisement> advertisement;
	HRESULT hr = args->get_Advertisement(advertisement.GetAddressOf());
	if (!FAILED(hr))
	{
		HString hStringName;
		advertisement->get_LocalName(hStringName.GetAddressOf());
		std::wstring wname = WindowsGetStringRawBuffer(hStringName.Get(), nullptr);

		if (wname.length() == 0)
			return S_OK;

		std::string name;
		for( int i = 0; i < (int)wname.size( ); i++ )
			name.push_back( (char)wname[i] );

		// First, make sure this is a valid Pasco device name
		int dashIndex = name.find( '-' );
		int gtIndex = name.find( '>' );

		// Not a valid Pasco name
		if( dashIndex == -1 || gtIndex == -1 )
			return S_OK;

		// Valid name has three-digit numbers on each side of the dash
		if( !isdigit( name[dashIndex - 3] ) || !isdigit( name[dashIndex + 3] ) )
			return S_OK;

		int ifaceID = 0x400 + Decode64( name[gtIndex + 1] );

		UINT64 bleAddress;
		args->get_BluetoothAddress(&bleAddress);

		for( int i = 0; i < (int)mBLEDevices.size( ); i++ )
			if( mBLEDevices[i]->mDeviceAddress == bleAddress )
				return S_OK;
		
		conPrintf( "Receive Advert Packet: %s\n", name.c_str( ) );

		ComPtr<IBluetoothLEDevice> bleDevice;

		ComPtr<IBluetoothLEDeviceStatics> deviceStatics;
		HRESULT hr = GetActivationFactory(HString::MakeReference(RuntimeClass_Windows_Devices_Bluetooth_BluetoothLEDevice).Get(), &deviceStatics);

		bool evt = false;
		ComPtr<IAsyncOperation<BluetoothLEDevice *>> deviceFromAddressOperation;
		deviceStatics->FromBluetoothAddressAsync(bleAddress, &deviceFromAddressOperation);
		deviceFromAddressOperation->put_Completed(Callback<IAsyncOperationCompletedHandler<BluetoothLEDevice *>>([this, &evt, &bleDevice](IAsyncOperation<BluetoothLEDevice *> *op, AsyncStatus asyncStatus)
		{
			HRESULT hr = op->GetResults(&bleDevice);

			evt = true;

			return S_OK;
		}).Get());

		WaitForEvent( evt, 5000 );

		if( bleDevice )
		{
			BluetoothConnectionStatus status;
			bleDevice->get_ConnectionStatus(&status);

			if (status == BluetoothConnectionStatus_Disconnected)
			{
				WinBLEDevice * winBLEDevice = new WinBLEDevice(bleAddress);
				winBLEDevice->mName = name;
				winBLEDevice->mInterfaceID = ifaceID;

				WaitForSingleObject( mDeviceMutex, INFINITE );
				mBLEDevices.push_back( winBLEDevice );
				ReleaseMutex( mDeviceMutex );
			}
		}
	}

	return S_OK;
}
