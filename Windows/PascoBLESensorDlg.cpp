
// PascoBLESensorDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "PascoBLESensor.h"
#include "PascoBLESensorDlg.h"
#include "afxdialogex.h"
#include "WinBLEScanner.h"
#include "WinBLEDevice.h"
#include "PascoBLEDriver.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define IFACE_ID_TEMPERATURE 1025
#define IFACE_ID_PRESSURE 1027
#define IFACE_ID_FORCE_PLATFORM 1059

CPascoBLESensorDlg* mainDialog = NULL;
WinBLEScanner* gBLEScanner = NULL;
PascoBLEDriver* gBLEDevice = NULL;
int gSampleSize = 1;
double gCalScales[] = { 1, 1, 1, 1 };
double gCalOffsets[] = { 0, 0, 0, 0 };

static DWORD WINAPI BLEThread( LPVOID param ) { ((CPascoBLESensorDlg *)param)->BLEThread( ); return 0; }

int conPrintf( const char* format, ... );
void HexDump( const void* data, int count );

CPascoBLESensorDlg::CPascoBLESensorDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_PASCOBLESENSOR_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	mainDialog = this;
}

void CPascoBLESensorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CPascoBLESensorDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED( IDC_BTN_SCAN, &CPascoBLESensorDlg::OnBnClickedBtnScan )
	ON_BN_CLICKED( IDC_BTN_CONNECT, &CPascoBLESensorDlg::OnBnClickedBtnConnect )
	ON_BN_CLICKED( IDC_BTN_START_SAMPLING, &CPascoBLESensorDlg::OnBnClickedBtnStartSampling )
	ON_BN_CLICKED( IDC_BTN_STOP_SAMPLING, &CPascoBLESensorDlg::OnBnClickedBtnStopSampling )
	ON_BN_CLICKED( IDC_CHK_LOG_PACKETS, &CPascoBLESensorDlg::OnBnClickedChkLogPackets )
	ON_BN_CLICKED( IDC_BTN_ONE_SHOT_READ, &CPascoBLESensorDlg::OnBnClickedBtnOneShotRead )
END_MESSAGE_MAP()


// CPascoBLESensorDlg message handlers

BOOL CPascoBLESensorDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	mConsoleWnd = GetDlgItem( IDC_EDIT_CONSOLE );
//	mBLEThread = CreateThread( NULL, 0, ::BLEThread, this, 0, NULL );
	gBLEScanner = new WinBLEScanner( );
	gBLEDevice = new PascoBLEDriver( );
	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CPascoBLESensorDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CPascoBLESensorDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//**********************************************************************
//
//**********************************************************************
int conPrintf( const char* format, ... )
{
	if( !mainDialog->mConsoleWnd )
		return 0;

	va_list params;
	va_start( params, format );
	int ret = 0;

	if( format )
	{
		CHAR* buf = new CHAR[0x400];

		ret = vsprintf_s( buf, 0x400, format, params );

		int idx = ::GetWindowTextLengthA( mainDialog->mConsoleWnd->m_hWnd );
		::SendMessageA( mainDialog->mConsoleWnd->m_hWnd, EM_SETSEL, (WPARAM)idx, (LPARAM)idx );
		::SendMessageA( mainDialog->mConsoleWnd->m_hWnd, EM_REPLACESEL, 0, (LPARAM)buf );

		delete buf;
	}

	va_end( params );

	return ret;
}

//**********************************************************************
//
//**********************************************************************
void CPascoBLESensorDlg::BLEThread( )
{
	while( true )
	{
		Sleep( 100 );
	}
}

//**********************************************************************
//
//**********************************************************************
void CPascoBLESensorDlg::OnTimer( UINT_PTR nIDEvent )
{
	// Place scanned items in the list box
	if( nIDEvent == 101 )
	{
		CListBox* deviceList = (CListBox*)GetDlgItem( IDC_DEVICE_LIST );
		for( size_t i = deviceList->GetCount( ); i < gBLEScanner->mBLEDevices.size( ); i++ )
			deviceList->InsertString( -1, CString( gBLEScanner->mBLEDevices[i]->mName.c_str( ) ) );
	}

	if( nIDEvent == 100 )
	{
		// Force Platform sample has four 16-bit ints per reading 
		if( gBLEDevice->mInterfaceID == IFACE_ID_FORCE_PLATFORM )
		{
			// Read as many samples that will fit in the buffer
			unsigned short sampleData[0x40];
			int sampleSize = 8;
			int actual = 0;
			gBLEDevice->ReadSampleData( 0, sampleData, sizeof( sampleData ), sampleSize, actual );

			// Convert every four words into a value in Newtons
			for( int i = 0; i < actual / 2; i += sampleSize / 2 )
			{
				double beam1 = gCalScales[0] * sampleData[i + 0] + gCalOffsets[0];
				double beam2 = gCalScales[1] * sampleData[i + 1] + gCalOffsets[1];
				double beam3 = gCalScales[2] * sampleData[i + 2] + gCalOffsets[2];
				double beam4 = gCalScales[3] * sampleData[i + 3] + gCalOffsets[3];
				double total = beam1 + beam2 + beam3 + beam4;

				conPrintf( "%f (N)   %f (lbF)\n", total, total * 0.2248 );
			}
		}
		else if( gBLEDevice->mInterfaceID == IFACE_ID_PRESSURE )
		{
			// Read as many samples that will fit in the buffer
			unsigned short sampleData[0x40];
			int sampleSize = 2;
			int actual = 0;
			gBLEDevice->ReadSampleData( 0, sampleData, sizeof( sampleData ), sampleSize, actual );

			// Convert every 16-bit word into a value in psi
			for( int i = 0; i < actual / 2; i += sampleSize / 2 )
			{
				double pressure = gCalScales[0] * sampleData[i] + gCalOffsets[0];

				conPrintf( "%f (psi)\n", pressure );
			}
		}
		else if( gBLEDevice->mInterfaceID == IFACE_ID_TEMPERATURE )
		{
			// Read as many samples that will fit in the buffer
			unsigned short sampleData[0x40];
			int sampleSize = 2;
			int actual = 0;
			gBLEDevice->ReadSampleData( 0, sampleData, sizeof( sampleData ), sampleSize, actual );

			// Convert every 16-bit word into a value in degrees celsius
			for( int i = 0; i < actual / 2; i += sampleSize / 2 )
			{
				double temperature = 0.00268127 * sampleData[i] - 46.85;

				conPrintf( "%f (degC)\n", temperature );
			}
		}
		else
		{
			// Read as many samples that will fit in the buffer
			unsigned char sampleData[0x40];
			int sampleSize = 2;
			int actual = 0;
			gBLEDevice->ReadSampleData( 0, sampleData, sizeof( sampleData ), sampleSize, actual );

			conPrintf( "Read %d bytes of sample data:\n", actual );
			HexDump( sampleData, actual );
		}
	}

	CDialogEx::OnTimer( nIDEvent );
}

//**********************************************************************
//
//**********************************************************************
void CPascoBLESensorDlg::OnBnClickedBtnScan( )
{
	if( gBLEScanner->mScanning )
	{
		gBLEScanner->StopScan( );
		KillTimer( 101 );
	}
	else
	{
		CListBox* deviceList = (CListBox*)GetDlgItem( IDC_DEVICE_LIST );
		while( deviceList->GetCount( ) )
			deviceList->DeleteString( 0 );
		gBLEScanner->StartScan( );
		SetTimer( 101, 200, NULL );
	}
}


//**********************************************************************
//
//**********************************************************************
void CPascoBLESensorDlg::OnBnClickedBtnConnect( )
{
	gBLEScanner->StopScan( );
	KillTimer( 101 );

	// Get the index of the selected listbox item
	CListBox *devList = (CListBox *)GetDlgItem( IDC_DEVICE_LIST );
	int index = devList->GetCurSel( );
	
	if( index >= 0 )
	{
		gBLEDevice->Connect( gBLEScanner->mBLEDevices[index] );

		int major = 0;
		int minor = 0;
		gBLEDevice->GetFirmwareVersion( major, minor );
		conPrintf( "Firmware version %d.%d\n", major, minor );
	}
	else
	{
		conPrintf( "Please select a device to connect to\n" );
		return;
	}

	// Force Platform has calibration data
	gSampleSize = 4;
	int numCals = 0;
	if( gBLEDevice->mInterfaceID == IFACE_ID_FORCE_PLATFORM )
	{
		gSampleSize = 8;
		numCals = 4;
	}
	else if( gBLEDevice->mInterfaceID == IFACE_ID_PRESSURE )
	{
		gSampleSize = 2;
		numCals = 1;
	}
	else if( gBLEDevice->mInterfaceID == IFACE_ID_TEMPERATURE )
		gSampleSize = 2;

	// Read calibration data
	if( numCals )
	{
		// Transfer calibration data from sensor flash to sensor RAM
		if( !gBLEDevice->XferBurstRam( true, GCMD_XFER_LOC_NVR, NVSTORAGE_FACTORY_CAL_TAG( 0 ), numCals * 16 ) )
			conPrintf( "Failed to transfer calibration data to burst RAM\n" );
		else
		{
			// Read calibration data from sensor RAM
			float calData[0x20];
			if( !gBLEDevice->DoBurstReadTransfer( calData, numCals * 16 ) )
				conPrintf( "Failed to burst transfer calibration data from sensor\n" );
			else
			{
				// Calculate scale/offset from the two-point calibrations
				for( int i = 0; i < numCals; i++ )
				{
					float in1 = calData[i * 4 + 0];
					float out1 = calData[i * 4 + 1];
					float in2 = calData[i * 4 + 2];
					float out2 = calData[i * 4 + 3];

					gCalScales[i] = (out2 - out1) / (in2 - in1);
					gCalOffsets[i] = (out1 + out2 - gCalScales[i] * (in2 + in1)) / 2.0;

					conPrintf( "Cal%d: (%f->%f) (%f->%f) -> Scale=%f, Offset=%f\n", i, in1, out1, in2, out2, gCalScales[i], gCalOffsets[i] );
				}
			}
		}

	}
}


//**********************************************************************
//
//**********************************************************************
void CPascoBLESensorDlg::OnBnClickedBtnStartSampling( )
{
	gBLEDevice->SetSampleConfig( 0, 100000, 0, false );
	gBLEDevice->StartSampling( );
	SetTimer( 100, 200, NULL );
}


//**********************************************************************
//
//**********************************************************************
void CPascoBLESensorDlg::OnBnClickedBtnStopSampling( )
{
	gBLEDevice->StopSampling( );
	KillTimer( 100 );
}

//**********************************************************************
//
//**********************************************************************
void CPascoBLESensorDlg::OnBnClickedBtnOneShotRead( )
{
	if( gBLEDevice->mInterfaceID == IFACE_ID_TEMPERATURE )
	{
		unsigned short data = 0;
		gBLEDevice->ReadOneSample( 0, &data, 2 );

		double temperature = 0.00268127 * data - 46.85;
		conPrintf( "Read One Temperature: %f (degC)\n", temperature );
	}
	else
	{
		unsigned char data[0x10] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
		gBLEDevice->ReadOneSample( 0, data, 0x10 );

		conPrintf( "One-Shot read:\n" );
		HexDump( data, 0x10 );
	}
}

//**********************************************************************
//
//**********************************************************************
void CPascoBLESensorDlg::OnBnClickedChkLogPackets( )
{
	CButton* chkBox = (CButton*)GetDlgItem( IDC_CHK_LOG_PACKETS );
	if( chkBox )
	{
		gBLEDevice->mLogRequests = chkBox->GetCheck( ) == BST_CHECKED;
		gBLEDevice->mLogRawData = chkBox->GetCheck( ) == BST_CHECKED;
	}
}

//**********************************************************************
//
//**********************************************************************
void HexDump( const void* dataIn, int count )
{
	if( !dataIn || !count )
		return;

	char str[0x100];
	const unsigned char* data = (const unsigned char*)dataIn;

	// for each 16 byte row
	for( int row = 0; row <= (count - 1) / 16; row++ )
	{
		char* pstr = str;

		// fill in the hex view of the data
		int col;
		for( col = 0; col < 16; col++ )
		{
			if( row * 16 + col < count )
				pstr += sprintf_s( pstr, 0x10, "%02x ", data[row * 16 + col] );
			else
				pstr += sprintf_s( pstr, 0x10, "   " );
		}

		// fill in the char view of the data
		for( col = 0; col < 16; col++ )
		{
			if( row * 16 + col < count )
			{
				char ch = data[row * 16 + col];
				if( ch < 0x20 || ch >= 0x7f )
					*pstr++ = '.';
				else
					*pstr++ = ch;
			}
			else
				*pstr++ = ' ';
		}
		*pstr = 0;

		conPrintf( "%04x: %s\n", row * 0x10, str );
	}
}
