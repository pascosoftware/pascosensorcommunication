
// PascoBLESensor.cpp : Defines the class behaviors for the application.
//

#include "pch.h"
#include "framework.h"
#include "PascoBLESensor.h"
#include "PascoBLESensorDlg.h"
#include <sddl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CPascoBLESensorApp

BEGIN_MESSAGE_MAP(CPascoBLESensorApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// CPascoBLESensorApp construction

CPascoBLESensorApp::CPascoBLESensorApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only CPascoBLESensorApp object

CPascoBLESensorApp theApp;


// CPascoBLESensorApp initialization

BOOL CPascoBLESensorApp::InitInstance()
{
	CWinApp::InitInstance();

	//**********************************************************************************************
	// This bit of weirdness is required to allow the BLE code to work.
	CoInitialize( NULL );

	PSECURITY_DESCRIPTOR pSDCNV;
	if( !ConvertStringSecurityDescriptorToSecurityDescriptor( L"O:BAG:BAD:(A;;0x7;;;PS)(A;;0x3;;;SY)(A;;0x7;;;BA)(A;;0x3;;;AC)(A;;0x3;;;LS)(A;;0x3;;;NS)", SDDL_REVISION_1, &pSDCNV, NULL ) )
	{
//		qDebug( ) << "Failed";
	}

	SECURITY_DESCRIPTOR SD = { 0 };
	DWORD dwSDSize = sizeof( SECURITY_DESCRIPTOR );
	PACL pDACL = NULL;
	DWORD dwDACLSize = 0;
	PACL pSACL = NULL;
	DWORD dwSACLSize = 0;
	PSID pOwnerSID = NULL;
	DWORD dwOwnerSIDSize = 0;
	PSID pGroupSID = NULL;
	DWORD dwGroupSIDSize = 0;

	if( !MakeAbsoluteSD( pSDCNV, &SD, &dwSDSize, pDACL, &dwDACLSize, pSACL, &dwSACLSize, pOwnerSID, &dwOwnerSIDSize, pGroupSID, &dwGroupSIDSize ) )
	{
		pDACL = (PACL)GlobalAlloc( GPTR, dwDACLSize );
		pSACL = (PACL)GlobalAlloc( GPTR, dwSACLSize );
		pOwnerSID = (PACL)GlobalAlloc( GPTR, dwOwnerSIDSize );
		pGroupSID = (PACL)GlobalAlloc( GPTR, dwGroupSIDSize );

		if( !MakeAbsoluteSD( pSDCNV, &SD, &dwSDSize, pDACL, &dwDACLSize, pSACL, &dwSACLSize, pOwnerSID, &dwOwnerSIDSize, pGroupSID, &dwGroupSIDSize ) )
		{
//			qDebug( ) << "Failed";
		}

		CoInitializeSecurity( &SD, -1, nullptr, nullptr, RPC_C_AUTHN_LEVEL_DEFAULT, RPC_C_IMP_LEVEL_IDENTIFY, NULL, EOAC_NONE, nullptr );

		if( pSDCNV )
			LocalFree( pSDCNV );
		if( pDACL )
			GlobalFree( pDACL );
		if( pSACL )
			GlobalFree( pSACL );
		if( pOwnerSID )
			GlobalFree( pOwnerSID );
		if( pGroupSID )
			GlobalFree( pGroupSID );
	}
	// /weirdness
	//**********************************************************************************************

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Activate "Windows Native" visual manager for enabling themes in MFC controls
	CMFCVisualManager::SetDefaultManager(RUNTIME_CLASS(CMFCVisualManagerWindows));

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	CPascoBLESensorDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	else if (nResponse == -1)
	{
		TRACE(traceAppMsg, 0, "Warning: dialog creation failed, so application is terminating unexpectedly.\n");
		TRACE(traceAppMsg, 0, "Warning: if you are using MFC controls on the dialog, you cannot #define _AFX_NO_MFC_CONTROLS_IN_DIALOGS.\n");
	}

	// Delete the shell manager created above.
	if (pShellManager != nullptr)
	{
		delete pShellManager;
	}

#if !defined(_AFXDLL) && !defined(_AFX_NO_MFC_CONTROLS_IN_DIALOGS)
	ControlBarCleanUp();
#endif

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}

