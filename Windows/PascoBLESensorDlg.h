
// PascoBLESensorDlg.h : header file
//

#pragma once


// CPascoBLESensorDlg dialog
class CPascoBLESensorDlg : public CDialogEx
{
// Construction
public:
	CPascoBLESensorDlg(CWnd* pParent = nullptr);	// standard constructor

	void BLEThread( );

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PASCOBLESENSOR_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnScan( );
	afx_msg void OnBnClickedBtnConnect( );
	afx_msg void OnBnClickedBtnStartSampling( );
	afx_msg void OnBnClickedBtnStopSampling( );
	afx_msg void OnBnClickedChkLogPackets( );
	afx_msg void OnBnClickedBtnOneShotRead( );
	afx_msg void OnTimer( UINT_PTR nIDEvent );

	CWnd* mConsoleWnd;
	HANDLE mBLEThread;
};
