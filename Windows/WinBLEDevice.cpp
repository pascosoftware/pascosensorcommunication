
#include "pch.h"
#include "WinBLEDevice.h"
#include <vector>

#include <atomic>
#include <ppltasks.h>
#include <functional>
#include <sstream>

#include <windows.storage.streams.h>
#include <windows.foundation.collections.h>
#include <windows.Applicationmodel.background.h>
#include <windows.devices.background.h>
#include <windows.devices.enumeration.h>	
#include <windows.devices.bluetooth.h>	
#include <windows.devices.bluetooth.genericattributeprofile.h>
#include <Windows.storage.streams.h>
#include <windows.security.cryptography.h>

//using namespace PascoWRLHelpers;
using namespace Microsoft::WRL;
using namespace Microsoft::WRL::Wrappers;
using namespace ABI::Windows::Foundation;
using namespace ABI::Windows::Foundation::Collections;
using namespace ABI::Windows::ApplicationModel::Background;
using namespace ABI::Windows::Devices;
using namespace ABI::Windows::Devices::Enumeration;
using namespace ABI::Windows::Devices::Bluetooth;
using namespace ABI::Windows::Devices::Bluetooth::Advertisement;
using namespace ABI::Windows::Devices::Bluetooth::GenericAttributeProfile;
using namespace ABI::Windows::Storage::Streams;
using namespace ABI::Windows::Security::Cryptography;

bool logData = false;
bool logDiscovery = false;

int conPrintf( const char* format, ... );
bool WaitForEvent( bool& evt, int timeout );
void HexDump( const void* data, int count );

#define PASCO_BLE_ID_HDR		0x4a
#define CharID(channel,cmd)     (cmd | ((channel) << 8) | (PASCO_BLE_ID_HDR << 24))

//***********************************************************************
//
//***********************************************************************
WinBLEDevice::WinBLEDevice(UINT64 deviceAddress)
{
	mSendMutex = CreateMutex( NULL, false, NULL );
	mCharacteristicMutex = CreateMutex( NULL, false, NULL );
	
	mIsConnected = false;
	mDeviceAddress = deviceAddress;
	mEventHandler = NULL;
	HRESULT hr = S_OK;

	mConnectionChanged = Callback<ITypedEventHandler<BluetoothLEDevice*, IInspectable*> >
		(std::bind(
			&WinBLEDevice::OnConnectionChanged,
			this,
			std::placeholders::_1,
			std::placeholders::_2
		));

	mCharacteristicValueChanged = Callback<ITypedEventHandler<GattCharacteristic*, GattValueChangedEventArgs *> >
		(std::bind(
			&WinBLEDevice::OnCharacteristicValueChanged,
			this,
			std::placeholders::_1,
			std::placeholders::_2
		));

	GetActivationFactory(HString::MakeReference(L"Windows.Security.Cryptography.CryptographicBuffer").Get(), &mBufferStatics);
}

//***********************************************************************
//
//***********************************************************************
WinBLEDevice::~WinBLEDevice( )
{
	mEventHandler = NULL;
	if (mIsConnected)
		Disconnect( );

	conPrintf( "WinBLEDevice::~WinBLEDevice Device %s has been deleted.\n", mName.c_str( ) );
}

//***********************************************************************
//
//***********************************************************************
bool WinBLEDevice::Connect( int timeout )
{
	conPrintf( "Connecting to '%s'\n", mName.c_str( ) );

	mCharacteristics.clear();

	RoInitializeWrapper initialize(RO_INIT_MULTITHREADED);

//	HANDLE connectMutex = CreateMutex( NULL, false, NULL );
//	HANDLE connectEvent = CreateEvent( NULL, false, false, NULL );
	bool evt = false;

	ComPtr<IBluetoothLEDeviceStatics> deviceStatics;
	HRESULT hr = GetActivationFactory(HString::MakeReference(RuntimeClass_Windows_Devices_Bluetooth_BluetoothLEDevice).Get(), &deviceStatics);
	
	ComPtr<IAsyncOperation<BluetoothLEDevice *>> deviceFromAddressOperation;
	deviceStatics->FromBluetoothAddressAsync(mDeviceAddress, &deviceFromAddressOperation);

	deviceFromAddressOperation->put_Completed(Callback<IAsyncOperationCompletedHandler<BluetoothLEDevice *>>([this, &evt](IAsyncOperation<BluetoothLEDevice *> *op, AsyncStatus asyncStatus)
	{
		HRESULT hr = op->GetResults(&mDevice);
		
		if (asyncStatus != Completed)
		{
			conPrintf( "WinBLEDevice::Connect Failed to get Device: %08x\n", hr );
		}

		evt = true;
//		SetEvent( connectEvent );

		return S_OK;
	}).Get());

	WaitForEvent( evt, 5000 );

//	WaitForSingleObject( connectMutex, INFINITE );
//	WaitForSingleObject( connectEvent, INFINITE );
//	ReleaseMutex( connectMutex );

	if (!mDevice)
		return false;

	mDevice->add_ConnectionStatusChanged(mConnectionChanged.Get(), &mConnectionEventToken);

	ComPtr<IBluetoothLEDevice3> device3;
	mDevice.Get()->QueryInterface(IID_IBluetoothLEDevice3, &device3);

	ComPtr<IGattDeviceServicesResult> gattServicesResult;
	ComPtr<IAsyncOperation<GattDeviceServicesResult*>> getGattServicesOp;
	device3->GetGattServicesAsync(&getGattServicesOp);

	getGattServicesOp->put_Completed(Callback<IAsyncOperationCompletedHandler<GattDeviceServicesResult *>>([this, &evt, &gattServicesResult](IAsyncOperation<GattDeviceServicesResult *> *op, AsyncStatus asyncStatus)
	{
		HRESULT hr = op->GetResults(&gattServicesResult);
		
		if (asyncStatus != Completed)
		{
			conPrintf( "WinBLEDevice::Connect Failed to get Gatt Services: %08x\n", hr );
		}

		evt = true;
//		SetEvent( connectEvent );

		return S_OK;
	}).Get());

	WaitForEvent( evt, 5000 );
//	WaitForSingleObject( connectMutex, INFINITE );
//	WaitForSingleObject( connectEvent, INFINITE );
//	ReleaseMutex( connectMutex );

	if (!gattServicesResult)
		return false;
	
	ComPtr<IVectorView<GattDeviceService*>> gattServices;
	hr = gattServicesResult->get_Services(&gattServices);
	
	if (!gattServices)
		return false;
	
	unsigned int gattServicesSize = 0;
	gattServices->get_Size(&gattServicesSize);

	conPrintf( "WinBLEDevice::Connect: Found %d services\n", gattServicesSize );

	for (unsigned int gattServicesCount = 0; gattServicesCount < gattServicesSize; gattServicesCount++)
	{
		ComPtr<IGattDeviceService> gattService;
		gattServices->GetAt(gattServicesCount, &gattService);
			
		GUID gattServiceGuid;
		gattService->get_Uuid(&gattServiceGuid);

		//Filter out the default ones handled by the ble stack
		if (gattServiceGuid.Data1 == 0x1800 || gattServiceGuid.Data1 == 0x1801 || gattServiceGuid.Data1 == 0x180A)
			continue;

		ComPtr<IGattDeviceService3> gattService3;
		gattService.Get()->QueryInterface(IID_IGattDeviceService3, &gattService3);
		
		ComPtr<IGattCharacteristicsResult> gattCharacteristicsResult;
		ComPtr<IAsyncOperation<GattCharacteristicsResult*>> getCharacteristicsOp;
		gattService3->GetCharacteristicsAsync(&getCharacteristicsOp);
		
		getCharacteristicsOp->put_Completed(Callback<IAsyncOperationCompletedHandler<GattCharacteristicsResult *>>([this, gattServiceGuid, &evt, &gattCharacteristicsResult](IAsyncOperation<GattCharacteristicsResult*> *op, AsyncStatus asyncStatus)
		{
			HRESULT hr = op->GetResults(&gattCharacteristicsResult);

			if (asyncStatus != Completed)
			{
				conPrintf( "WinBLEDevice::Connect Failed to get Characteristics for Gatt service id: %08x\n", hr ); // << QUuid( gattServiceGuid ) << hr;
			}

			evt = true;
//			SetEvent( connectEvent );

			return S_OK;
		}).Get());

		WaitForEvent( evt, 5000 );
//		WaitForSingleObject( connectMutex, INFINITE );
//		WaitForSingleObject( connectEvent, INFINITE );
//		ReleaseMutex( connectMutex );

		if (!gattCharacteristicsResult)
			return false;

		ComPtr<IVectorView<GattCharacteristic *>> characteristics;
		gattCharacteristicsResult->get_Characteristics(&characteristics);

		unsigned int characteristicsSize = 0;
		characteristics->get_Size(&characteristicsSize);
		for (unsigned int j = 0; j < characteristicsSize; j++)
		{
			ComPtr<IGattCharacteristic> characteristic;
			characteristics->GetAt(j, &characteristic);

			GUID characteristicGuid;
			characteristic->get_Uuid(&characteristicGuid);

			WinBLECharacteristic winBLECharacteristic;
			winBLECharacteristic.mCharID = CharID( characteristicGuid.Data1 & 7, characteristicGuid.Data2 );
			winBLECharacteristic.mCharacteristic = characteristic;
			mCharacteristics.push_back(winBLECharacteristic);
		}
	}
	
	conPrintf( "WinBLEDevice::Connect: Found %d characteristics\n", mCharacteristics.size() );

	if (mCharacteristics.size() > 0)
	{
		BluetoothConnectionStatus status;
		mDevice->get_ConnectionStatus(&status);
		if(status != BluetoothConnectionStatus_Connected)
			conPrintf( "WinBLEDevice::Connect Connection Status = %d\n", status );
		mIsConnected = (status == BluetoothConnectionStatus_Connected);
	}

	conPrintf( "WinBLEDevice::Connect: %s\n", mIsConnected ? "Success" : "Failed" );

	return mIsConnected;
}

//***********************************************************************
//
//***********************************************************************
ComPtr<IBuffer> WinBLEDevice::CreateIBufferWithData(const void *data, int size)
{
	ComPtr<IBuffer> buffer;

	void * nonconstdata = const_cast<void *>(data);	//Need to pass in a non const pointer :(

	mBufferStatics->CreateFromByteArray(size, reinterpret_cast<BYTE *>(nonconstdata), &buffer);
	return buffer;
}

//***********************************************************************
//
//***********************************************************************
ComPtr<IGattCharacteristic> WinBLEDevice::FindCharacteristic(int charID)
{
	for (size_t i = 0; i < mCharacteristics.size(); i++)
	{
		if (mCharacteristics[i].mCharID == charID)
			return mCharacteristics[i].mCharacteristic;
	}

	return ComPtr<IGattCharacteristic>();
}

//***********************************************************************
//
//***********************************************************************
HRESULT WinBLEDevice::OnConnectionChanged(ABI::Windows::Devices::Bluetooth::IBluetoothLEDevice* device, IInspectable* inspectable)
{
	ULONG size = 0;
	IID *iids;
	if(inspectable)
		inspectable->GetIids(&size, &iids);

	BluetoothConnectionStatus status;
	device->get_ConnectionStatus(&status);

	if (status == BluetoothConnectionStatus_Disconnected && mIsConnected)
	{
		mIsConnected = false;

		mCharacteristics.clear();

		if (mDevice)
		{
			mDevice->remove_ConnectionStatusChanged(mConnectionEventToken);
			mDevice.Reset();
		}

		if (mEventHandler)
			mEventHandler->OnDisconnected(-1);	//Anything but zero is an error. 
	}

	return S_OK;
}

//***********************************************************************
//
//***********************************************************************
HRESULT WinBLEDevice::OnCharacteristicValueChanged(IGattCharacteristic* characteristic, IGattValueChangedEventArgs* args)
{
	WaitForSingleObject( mCharacteristicMutex, INFINITE );

	ComPtr<IBuffer> buffer;
	args->get_CharacteristicValue(&buffer);

	UINT32 bufferSize = 0;
	buffer->get_Length(&bufferSize);

	BYTE *bufferAsByteArray = 0;

	mBufferStatics->CopyToByteArray(buffer.Get(), &bufferSize, &bufferAsByteArray);

	GUID charGuid;
	characteristic->get_Uuid( &charGuid );
		
	int charID = CharID( charGuid.Data1 & 7, charGuid.Data2 );

	if (logData)
	{
		conPrintf( "Characteristic Changed: size=%d, charID=0x%08x\n", bufferSize, charID );
		HexDump( bufferAsByteArray, bufferSize );
	}

	if (mEventHandler)
		mEventHandler->OnRecvData(charID, bufferAsByteArray, bufferSize);
	
	ReleaseMutex( mCharacteristicMutex );
	return S_OK;
}

//***********************************************************************
//
//***********************************************************************
bool WinBLEDevice::Disconnect( )
{
	if (!mIsConnected)
		return true;

	mIsConnected = false;
	
	mCharacteristics.clear();

	if (mDevice)
	{
		mDevice->remove_ConnectionStatusChanged(mConnectionEventToken);
		mDevice.Reset();
	}

	if (mEventHandler)
		mEventHandler->OnDisconnected(0);	//Anything but zero is an error. 

	return true;
}

//***********************************************************************
//
//***********************************************************************
bool WinBLEDevice::IsConnected( )
{
    return mIsConnected;
}

//***********************************************************************
//
//***********************************************************************
bool WinBLEDevice::EnableNotifications( int charID, bool enable )
{
//	HANDLE enableMutex = CreateMutex( NULL, false, NULL );
//	HANDLE enableEvent = CreateEvent( NULL, false, false, NULL );

	int index = -1;

	for ( size_t i = 0; i < mCharacteristics.size(); i++)
	{
		if (mCharacteristics[i].mCharID == charID)
		{
			index = i;
			break;
		}
	}

	bool evt = false;

	if (index >= 0 && index < (int)mCharacteristics.size())
	{
		ComPtr<IAsyncOperation<GattCommunicationStatus>> enableNotificationsOp;
		GattClientCharacteristicConfigurationDescriptorValue descriptorValue = enable ? GattClientCharacteristicConfigurationDescriptorValue_Notify : GattClientCharacteristicConfigurationDescriptorValue_None;

		WinBLECharacteristic characteristic = mCharacteristics[index];
		if (enable)
		{
			characteristic.mCharacteristic->add_ValueChanged(mCharacteristicValueChanged.Get(), &characteristic.mValueChangedEventToken);

			characteristic.mCharacteristic->WriteClientCharacteristicConfigurationDescriptorAsync( GattClientCharacteristicConfigurationDescriptorValue_Notify, &enableNotificationsOp);

			enableNotificationsOp->put_Completed(Callback<IAsyncOperationCompletedHandler<GattCommunicationStatus>>([this, &evt](IAsyncOperation<GattCommunicationStatus> *op, AsyncStatus asyncStatus)
			{
				evt = true;
//				SetEvent( enableEvent );
				return S_OK;
			}).Get());

			WaitForEvent( evt, 5000 );
//			WaitForSingleObject( enableMutex, INFINITE );
//			WaitForSingleObject( enableEvent, INFINITE );
//			ReleaseMutex( enableMutex );
		}
		else
		{
			characteristic.mCharacteristic->remove_ValueChanged(characteristic.mValueChangedEventToken);

			characteristic.mCharacteristic->WriteClientCharacteristicConfigurationDescriptorAsync(GattClientCharacteristicConfigurationDescriptorValue_None, &enableNotificationsOp);
			enableNotificationsOp->put_Completed(Callback<IAsyncOperationCompletedHandler<GattCommunicationStatus>>([this, &evt](IAsyncOperation<GattCommunicationStatus> *op, AsyncStatus asyncStatus)
			{
				evt = true;
//				SetEvent( enableEvent );
				return S_OK;
			}).Get());

//			WaitForSingleObject( enableMutex, INFINITE );
//			WaitForSingleObject( enableEvent, INFINITE );
//			ReleaseMutex( enableMutex );
		}
	}

	return true;
}

//***********************************************************************
//
//***********************************************************************
bool WinBLEDevice::SendData( int charID, const void *data, int size, bool expectsResponse )
{
//	WaitForSingleObject( mSendMutex, INFINITE );

	HANDLE lambdaMutex = CreateMutex( NULL, false, NULL );
//	HANDLE writeEvent = CreateEvent( NULL, false, false, NULL );
	bool evt = false;

	ComPtr<IBuffer> buffer = CreateIBufferWithData(data, size);
	ComPtr<IGattCharacteristic> characteristic = FindCharacteristic(charID);
	
	if( !characteristic )
	{
//		ReleaseMutex( mSendMutex );
		return false;
	}

	if (!mEventHandler)
	{
//		ReleaseMutex( mSendMutex );
		return false;
	}

	ComPtr<IAsyncOperation<GattCommunicationStatus>> writeValueOperation;
	//characteristic->WriteValueWithOptionAsync(buffer.Get(), expectsResponse ? GattWriteOption_WriteWithResponse : GattWriteOption_WriteWithoutResponse, &writeValueOperation);
	characteristic->WriteValueWithOptionAsync(buffer.Get(), GattWriteOption_WriteWithoutResponse, &writeValueOperation);
	if (!writeValueOperation)
		return false;

	bool completed = false;
	
//	WaitForSingleObject( lambdaMutex, INFINITE );
	writeValueOperation->put_Completed(Callback<IAsyncOperationCompletedHandler<GattCommunicationStatus>>([this, &evt, &completed, &lambdaMutex](IAsyncOperation<GattCommunicationStatus> *op, AsyncStatus) {
//		WaitForSingleObject( lambdaMutex, INFINITE );

		GattCommunicationStatus status;
		op->GetResults(&status);

		completed = (status != GattCommunicationStatus_Unreachable);

		if (status == GattCommunicationStatus_Unreachable)
		{
			if (mEventHandler)
				mEventHandler->OnError(0, GattCommunicationStatus_Unreachable);
		}
		evt = true;
//		SetEvent( writeEvent );

//		ReleaseMutex( lambdaMutex );

		return S_OK;
	}).Get());
	
	WaitForEvent( evt, 5000 );
//	WaitForSingleObject( lambdaMutex, INFINITE );
//	WaitForSingleObject( writeEvent, INFINITE );
//	ReleaseMutex( lambdaMutex );

	if (logData)
	{
		conPrintf( "Send Data: charID=0x%08x\n", charID );
	}

	return completed;
}
