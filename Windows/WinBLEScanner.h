#pragma once

#include <string>
#include <vector>
#include <wrl.h>
#include <windows.devices.enumeration.h>
#include <windows.devices.bluetooth.advertisement.h>

class WinBLEDevice;
class WinBLEScanner;

//*********************************************************************
//
//*********************************************************************
class WinBLEScanner
{
public:
	WinBLEScanner( );
	~WinBLEScanner( );
	
    // IBTScanner implementation
	bool StartScan( );
	bool StopScan( );

	std::vector<WinBLEDevice *> mBLEDevices;
	HANDLE mDeviceMutex;
	bool mScanning;

	static WinBLEScanner *sInstance;

private:
	//***********************************************************************
	//
	//***********************************************************************
	HRESULT OnAdvertisementReceived(ABI::Windows::Devices::Bluetooth::Advertisement::IBluetoothLEAdvertisementWatcher*, ABI::Windows::Devices::Bluetooth::Advertisement::IBluetoothLEAdvertisementReceivedEventArgs*);

	Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::Advertisement::IBluetoothLEAdvertisementWatcher> mAdvertisementWatcher;

	EventRegistrationToken mAdvertisementReceivedEventToken;
	Microsoft::WRL::ComPtr<ABI::Windows::Foundation::ITypedEventHandler<ABI::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementWatcher*, ABI::Windows::Devices::Bluetooth::Advertisement::BluetoothLEAdvertisementReceivedEventArgs*> > mAdvertisementReceived;
};
