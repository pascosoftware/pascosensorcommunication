#pragma once

#include <vector>
#include <string>

#include <wrl.h>
#include <windows.devices.bluetooth.h>
#include <windows.devices.enumeration.h>
#include <windows.security.cryptography.h>

class WinBLEScanner;
class WinBLEDevice;

class IBLEDeviceEvents
{
public:
	virtual void OnRecvData( int charID, const void* data, int size ) { };
	virtual void OnError( int charID, int error ) { }
	virtual void OnConnected( ) { }
	virtual void OnDisconnected( int error ) { }
};

//*********************************************************************
class WinBLECharacteristic
{
public:
    int mCharID;
	EventRegistrationToken mValueChangedEventToken;
	Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::IGattCharacteristic> mCharacteristic;
};

//*********************************************************************
class WinBLEGattService
{
public:
    int mServiceID;
    std::vector<WinBLECharacteristic *> mCharacteristics;
	Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::IGattDeviceService> mGattService;
};

//*********************************************************************
class WinBLEDevice
{
public:
    WinBLEDevice( UINT64 deviceAddress );
    ~WinBLEDevice( );
    
    // Establish a connection to the BLE device
    bool Connect( int timeout );
    bool Disconnect( );
    bool IsConnected( );

    bool EnableNotifications( int charID, bool enable );
    
    // Send/recv data synchronously or asynchronously
    bool SendData( int charID, const void *data, int size, bool expectsResponse );
    
	HRESULT OnConnectionChanged(ABI::Windows::Devices::Bluetooth::IBluetoothLEDevice*, IInspectable*);
	HRESULT OnCharacteristicValueChanged(ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::IGattCharacteristic* characteristic, ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::IGattValueChangedEventArgs* args);

	Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::IGattCharacteristic> FindCharacteristic(int charID);

	Microsoft::WRL::ComPtr<ABI::Windows::Storage::Streams::IBuffer> CreateIBufferWithData(const void *data, int size);


	Microsoft::WRL::ComPtr<ABI::Windows::Devices::Bluetooth::IBluetoothLEDevice> mDevice;

	Microsoft::WRL::ComPtr< ABI::Windows::Foundation::ITypedEventHandler<ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::GattCharacteristic*, ABI::Windows::Devices::Bluetooth::GenericAttributeProfile::GattValueChangedEventArgs*>> mCharacteristicValueChanged;

	EventRegistrationToken mConnectionEventToken;
	Microsoft::WRL::ComPtr<ABI::Windows::Foundation::ITypedEventHandler<ABI::Windows::Devices::Bluetooth::BluetoothLEDevice*, IInspectable*>> mConnectionChanged;

	Microsoft::WRL::ComPtr<ABI::Windows::Security::Cryptography::ICryptographicBufferStatics> mBufferStatics;

	std::string mName;
	int mInterfaceID;
	UINT64 mDeviceAddress;
	bool mIsConnected;
	HANDLE mSendMutex;
	HANDLE mCharacteristicMutex;
	std::vector<WinBLECharacteristic> mCharacteristics;
	IBLEDeviceEvents* mEventHandler;
};
